-- SCRIPT IN CARE SE VOR DEZACTIVA (NOTARI/INSCRIERI) CARE NU SE PREIAU DIN CARTEA FUNCIARA
--T2.NOTES NOT LIKE '%nfiin%' and t2.notes not like '%radi%' and t2.notes not like '%intravilan%' and t2.notes not like '%edificat%' and t2.notes not like '%repoz%' and t2.notes not like '%actualizar%' and
--										t2.notes not like '%resping%' 
--------------------------------------------------------------------------------------------------------------

					SELECT  DISTINCT    T7.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

					FROM	REGISTRATIONXENTITY T1
								INNER JOIN GET_LAND T7
								ON T1.LANDID=T7.LANDID
									INNER JOIN GET_REGISTRATION T2
									ON T1.REGISTRATIONID=T2.REGISTRATIONID
										INNER JOIN LANDGEOMETRY T3
										ON T7.LANDID=T3.LANDLOGID
					WHERE	T1.ACTIVE='1'  
					AND T2.NAMEREGISTRATIONTYPE='Notare' 
							AND (T2.NOTES LIKE '%nfiin%' OR t2.notes like '%radi%' OR t2.notes like '%intravilan%' OR t2.notes like '%edificat%' OR t2.notes like '%repoz%' OR t2.notes like '%actualizar%' OR t2.notes like '%modif%' OR t2.notes like '%construc%') AND T2.NOTES NOT LIKE '%CONTRADICTORIU%' and T2.NOTES NOT LIKE '%respingerea%'
							OR t2.notes like '%resping%' 
							AND T2.NOTES LIKE '%17/2014%' and t2.notes like '%oficiu%'
							AND (T2.NOTES <>'' OR T2.COMMENTS<>'')
							--AND T2.COMMENTS LIKE '%B%' COLLATE Latin1_General_CS_AS 
					UNION 

					SELECT  DISTINCT    T8.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

					FROM	REGISTRATIONXENTITY T1
									INNER JOIN GET_BUILDING T8
									ON T1.BUILDINGID=T8.BUILDINGID
										INNER JOIN GET_REGISTRATION T2
										ON T1.REGISTRATIONID=T2.REGISTRATIONID
											INNER JOIN BUILDINGGEOMETRY T3
											ON T8.BUILDINGID=T3.BuildingLogID
					WHERE	T1.ACTIVE='1' 
					AND T2.NAMEREGISTRATIONTYPE='Notare'
							AND (T2.NOTES LIKE '%nfiin%' OR t2.notes like '%radi%' OR t2.notes like '%intravilan%' OR t2.notes like '%edificat%' OR t2.notes like '%repoz%' OR t2.notes like '%actualizar%' OR t2.notes like '%modif%' OR t2.notes like '%construc%') AND T2.NOTES NOT LIKE '%CONTRADICTORIU%' and T2.NOTES NOT LIKE '%respingerea%'
							 OR t2.notes like '%resping%' 
							 AND T2.NOTES LIKE '%17/2014%' and t2.notes like '%oficiu%'
							 AND (T2.NOTES <>'' OR T2.COMMENTS<>'')
							-- AND T2.COMMENTS LIKE '%B%' COLLATE Latin1_General_CS_AS
--------------------------------------------------------------------------------------------------------------

--- versiune MURES-------------

--------------------------------------------------------------------------------------------------------------
--UPDATE  REGISTRATIONXENTITY
--SET		ACTIVE='0'
--WHERE	REGISTRATIONID IN (
	
--							SELECT		DISTINCT T1.REGISTRATIONID,
--										landid,notes
--							FROM		(
--												SELECT  DISTINCT    T7.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

--												FROM	REGISTRATIONXENTITY T1
--															INNER JOIN GET_LAND T7
--															ON T1.LANDID=T7.LANDID
--																INNER JOIN GET_REGISTRATION T2
--																ON T1.REGISTRATIONID=T2.REGISTRATIONID
--																	--INNER JOIN LANDGEOMETRY T3
--																	--ON T7.LANDID=T3.LANDLOGID
--												WHERE	T1.ACTIVE='1' 
--														AND (T2.NOTES LIKE '%nfiin%' OR t2.notes like '%radi%' OR t2.notes like '%intravilan%' OR t2.notes like '%extind%' OR t2.notes like '%repoz%' OR t2.notes like '%localizare%' or t2.notes like '%resping%' )
--														--AND T2.NOTES LIKE '%17/2014%' and t2.notes like '%oficiu%'
--												UNION 

--												SELECT  DISTINCT    T8.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

--												FROM	REGISTRATIONXENTITY T1
--																INNER JOIN GET_BUILDING T8
--																ON T1.BUILDINGID=T8.BUILDINGID
--																	INNER JOIN GET_REGISTRATION T2
--																	ON T1.REGISTRATIONID=T2.REGISTRATIONID
--																		--INNER JOIN BUILDINGGEOMETRY T3
--																		--ON T8.BUILDINGID=T3.BuildingLogID
--												WHERE	T1.ACTIVE='1' 
--														AND (T2.NOTES LIKE '%nfiin%' OR t2.notes like '%radi%' OR t2.notes like '%intravilan%' OR t2.notes like '%extind%' OR t2.notes like '%repoz%' OR t2.notes like '%localizare%' or t2.notes like '%resping%' )
--														 --AND T2.NOTES LIKE '%17/2014%' and t2.notes like '%oficiu%'
--											) AS T1
--											INNER JOIN LANDGEOMETRY T2
--											ON T1.LANDID=T2.LANDLOGID


--								)
--------------------------------------------------------------------------------------------------------------

