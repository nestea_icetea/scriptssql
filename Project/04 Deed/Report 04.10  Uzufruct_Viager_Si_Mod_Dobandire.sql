
-- IMOBILE PE UZUFRUCT VIAGER-------------
--in righttype, righttypeid este la uzufruct viager = 16 si uzufruct = 15

BEGIN TRANSACTION UPDATE REGISTRATION SET RIGHTTYPEID='15'  
--,RIGHTCOMMENT = 'VIAGER, ' + RIGHTCOMMENT -- (se foloseste pentru cele care au completat informatii la rightcomment)
															WHERE REGISTRATIONID IN (
																					SELECT  *
																					FROM	(
																							SELECT  DISTINCT T2.REGISTRATIONID,T2.RIGHTTYPEID,t1.landlogid as landlogid,t2.RightComment
																							FROM	LANDGEOMETRY T1
																										INNER JOIN GET_REGXENTITY T2
																										ON T1.LANDLOGID=T2.LANDID

																							WHERE  T2.RIGHTTYPEID='16' and t2.active='1'

																							UNION

																							SELECT  DISTINCT T2.REGISTRATIONID,T2.RIGHTTYPEID,t1.landlogid as landlogid,t2.RightComment
																							FROM	BUILDINGGEOMETRY T1
																										INNER JOIN GET_REGXENTITY T2
																										ON T1.BUILDINGLOGID=T2.BUILDINGID

																							WHERE   T2.RIGHTTYPEID='16' and t2.active='1' 
																							) AS T1
																					--WHERE RIGHTCOMMENT <>''
																					WHERE RIGHTCOMMENT = ''
																					)

COMMIT










---- IMOBILE FARA MOD DE DOBANDIRE---------------- ----- DE ACTUALIZAT -------
SELECT  COUNT(*)
FROM(
		SELECT  DISTINCT T2.LANDID AS LANDID, T2.REGISTRATIONID,T2.TITLEID
		FROM	LANDGEOMETRY T1
					INNER JOIN GET_REGXENTITY T2
					ON T1.LANDLOGID=T2.LANDID

		WHERE  T2.TITLEID='0' AND T2.NAMEREGISTRATIONTYPE<>'Notare' and t2.namerighTtype='PROPRIETATE'

		UNION

		SELECT  DISTINCT T3.LANDID AS LANDID,T2.REGISTRATIONID,T2.TITLEID
		FROM	BUILDINGGEOMETRY T1
					INNER JOIN GET_REGXENTITY T2
					ON T1.BUILDINGLOGID=T2.BUILDINGID
						INNER JOIN GET_LAND T3
						ON T1.LANDLOGID=T2.LANDID
				
		WHERE  T2.TITLEID='0' AND T2.NAMEREGISTRATIONTYPE<>'Notare' and t2.namerighTtype='PROPRIETATE'
) AS T1





