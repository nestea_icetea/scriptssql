
SELECT  DISTINCT *
FROM	(

					SELECT  T2.LANDID AS LANDID,T3.AUTHORITY,T3.DEEDID
					FROM	LANDGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.LANDLOGID=T2.LANDID
									INNER JOIN DEED T3
									ON T2.DEEDID=T3.DEEDID
					WHERE	LEN(T3.AUTHORITY)>50 AND T3.ACTIVE='1'AND T2.ACTIVE='1'

					UNION

					SELECT  T4.LANDID AS LANDID,T3.AUTHORITY,T3.DEEDID
					FROM	BUILDINGGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.BUILDINGLOGID=T2.BUILDINGID
									INNER JOIN DEED T3
									ON T2.DEEDID=T3.DEEDID
										INNER JOIN GET_LAND T4
										ON T1.LANDLOGID=T4.LANDID

					WHERE	LEN(T3.AUTHORITY)>50 AND T3.ACTIVE='1' AND T2.ACTIVE='1'


		) AS T1