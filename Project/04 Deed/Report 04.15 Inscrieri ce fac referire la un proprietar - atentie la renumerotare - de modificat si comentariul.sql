-- script pentru cautarea inscrierilor care contin comentarii ce fac referire la un anumit proprietar din CF. 
-- comentariul gen sub B1, B10 etc trebuie modificat cu noua pozitie dupa ce se face renumerotarea (la final)


					SELECT  DISTINCT    T7.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

					FROM	REGISTRATIONXENTITY T1
								INNER JOIN GET_LAND T7
								ON T1.LANDID=T7.LANDID
									INNER JOIN GET_REGISTRATION T2
									ON T1.REGISTRATIONID=T2.REGISTRATIONID
										INNER JOIN LANDGEOMETRY T3
										ON T7.LANDID=T3.LANDLOGID
					WHERE	 
							(T2.NOTES LIKE '%nfiin%' OR 
							t2.notes like '%radi%' OR 
							t2.notes like '%intravilan%' OR 
							t2.notes like '%edificat%' OR 
							t2.notes like '%repoz%' OR 
							t2.notes like '%actuali%' or
							t2.notes like '%resping%' or 
							t2.notes like '%adresa%' or
							t2.notes like '%desprind%' or
							t2.notes like '%dezmembr%' or
							t2.notes like '%alipire%' or
							t2.notes like '%apart%' or
							t2.notes like '%condominiu%' or
							t2.notes like '%autoriza%' OR
							t2.notes like '%e deschide%' or
							t2.notes like '%Imobil inscris in cartea funciara%' or
							t2.notes like '%e atribuie numarul%' or
							--
							t2.notes like '%calitatea de bun comun%' or
							t2.notes like '%se inscrie numarul%' or
							t2.notes like '%se reconstituie%' or
							t2.notes like '%eroarea materiala%' or 
							t2.notes like '%dezlipire%' or
							t2.notes like '%schimb%' or
							T2.NOTES LIKE '%e modifica%' or
							t2.notes like '%e rectific%' or
							t2.notes like '%e indreapt%' or
							t2.notes like '%e diminueaz%' or
							t2.notes like '%fara localizare%' or
							t2.notes like '%17/2014%' or
							t2.notes like '%se aduce in aceasta%' or
							t2.notes like '%categoria de folosinta%') 
							AND 
							(T2.NOTES NOT LIKE '%CONTRADICTORIU%' or 
							T2.NOTES NOT LIKE '%respingerea%' or
							--t2.notes not like '%interdic%' and
							t2.notes not like '%posesi%' or
							t2.notes not like '%servitute%' or
							t2.notes not like '%sechestru%' or
							t2.notes not like '%expropriere%' or
							t2.notes not like '%grevare%') and
							T1.ACTIVE='1' 
			
					UNION 

					SELECT  DISTINCT    T8.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

					FROM	REGISTRATIONXENTITY T1
									INNER JOIN GET_BUILDING T8
									ON T1.BUILDINGID=T8.BUILDINGID
										INNER JOIN GET_REGISTRATION T2
										ON T1.REGISTRATIONID=T2.REGISTRATIONID
											INNER JOIN BUILDINGGEOMETRY T3
											ON T8.BUILDINGID=T3.BuildingLogID
					WHERE	 
							(T2.NOTES LIKE '%nfiin%' OR 
							t2.notes like '%radi%' OR 
							t2.notes like '%intravilan%' OR 
							t2.notes like '%edificat%' OR 
							t2.notes like '%repoz%' OR 
							t2.notes like '%actuali%' or
							t2.notes like '%resping%' or 
							t2.notes like '%adresa%' or
							t2.notes like '%desprind%' or
							t2.notes like '%dezmembr%' or
							t2.notes like '%alipire%' or
							t2.notes like '%apart%' or
							t2.notes like '%condominiu%' or
							t2.notes like '%autoriza%' OR
							t2.notes like '%e deschide%' or
							t2.notes like '%Imobil inscris in cartea funciara%' or
							t2.notes like '%e atribuie numarul%' or
							--
							t2.notes like '%calitatea de bun comun%' or
							t2.notes like '%se inscrie numarul%' or
							t2.notes like '%se reconstituie%' or
							t2.notes like '%eroarea materiala%' or 
							t2.notes like '%dezlipire%' or
							t2.notes like '%schimb%' or
							T2.NOTES LIKE '%e modifica%' or
							t2.notes like '%e rectific%' or
							t2.notes like '%e indreapt%' or
							t2.notes like '%e diminueaz%' or
							t2.notes like '%fara localizare%' or
							t2.notes like '%17/2014%' or
							t2.notes like '%se aduce in aceasta%' or
							t2.notes like '%categoria de folosinta%') 
							AND 
							(T2.NOTES NOT LIKE '%CONTRADICTORIU%' or 
							T2.NOTES NOT LIKE '%respingerea%' or
							--t2.notes not like '%interdic%' and
							t2.notes not like '%posesi%' or
							t2.notes not like '%servitute%' or
							t2.notes not like '%sechestru%' or
							t2.notes not like '%expropriere%' or
							t2.notes not like '%grevare%') and
							T1.ACTIVE='1' 

--------------------------------------------------------------------------------------------------------------
--verificare note active
SELECT  DISTINCT    T7.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

					FROM	REGISTRATIONXENTITY T1
								INNER JOIN GET_LAND T7
								ON T1.LANDID=T7.LANDID
									INNER JOIN GET_REGISTRATION T2
									ON T1.REGISTRATIONID=T2.REGISTRATIONID
										INNER JOIN LANDGEOMETRY T3
										ON T7.LANDID=T3.LANDLOGID
					WHERE	 
							
							T1.ACTIVE='1' 
							and t2.notes <>''
							AND (T2.COMMENTS LIKE '%conversi%' or T2.COMMENTS LIKE '%transcris%')
			
					UNION 

					SELECT  DISTINCT    T8.LANDID AS LANDID,T2.LBPARTNO as Partea,T2.POSITION as Pozitia,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPNO as Incheiere,T2.Appdate as DataIncheiere,T2.NAMEREGISTRATIONTYPE,T2.NOTES,T2.COMMENTS

					FROM	REGISTRATIONXENTITY T1
									INNER JOIN GET_BUILDING T8
									ON T1.BUILDINGID=T8.BUILDINGID
										INNER JOIN GET_REGISTRATION T2
										ON T1.REGISTRATIONID=T2.REGISTRATIONID
											INNER JOIN BUILDINGGEOMETRY T3
											ON T8.BUILDINGID=T3.BuildingLogID
					WHERE	 
							
							T1.ACTIVE='1'  
							and t2.notes<>''
							AND (T2.COMMENTS LIKE '%conversi%' or T2.COMMENTS LIKE '%transcris%')
--------------------------------------------------------------------------------------------------------------


