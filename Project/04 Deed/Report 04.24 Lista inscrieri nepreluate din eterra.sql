-- script ce listeaza toate pozitiile nepreluate din eterra
--lista eterrelor valabile pentru comparatie
select  *
into	#eterreValabile
from		
			(select distinct t1.E2identifier
			from	get_land t1
						inner join RegistrationxEntity t2
						on t1.landid = t2.landid	
							inner join registration t3
							on t2.registrationid = t3.registrationid

			where	t3.positionEterra>0) as t1
go
-- listarea pozitiilor active din CG
select  *
into	#pozitiiActiveCG
from		
			(select  concat (t1.e2identifier,'-',t3.lbpartno,'-', ltrim(rtrim(t3.positionEterra))) as pozitieLog, position as pozitieRenumerotare, t3.Lbpartno, t1.E2identifier
			from	get_land t1
						inner join RegistrationxEntity t2
						on t1.landid = t2.landid	
							inner join registration t3
							on t2.registrationid = t3.registrationid

			where	
					t3.positionEterra>0 
					and t1.e2identifier>1 
					

			union

			select  concat (t4.e2identifier,'-',t3.lbpartno,'-', ltrim(rtrim(t3.positionEterra))) as pozitieLog, Position as pozitieRenumerotare, t3.Lbpartno, t4.E2Identifier
			from	get_building t1
						inner join registrationxentity t2
						on t1.buildingid = t2.buildingid	
							inner join registration t3
							on t2.registrationid = t3.registrationid
								inner join get_land t4
								on t1.landid = t4.landid
			where	t3.positionEterra>0 and t4.e2identifier>1) as t1
go
--listarea tuturor pozitiilor 
select  *
into	#pozitiiLogIE
from	(
			select  concat (t1.e2identifier,'-',t3.lbpartno,'-', ltrim(rtrim(t3.position))) as pozitieLog, t1.E2Identifier
			from	get_land_log t1
						inner join registrationxentity_log t2
						on t1.landid = t2.landid
							inner join registration_log t3
							on t2.RegistrationID=t3.RegistrationID

			union

			select  concat (t4.e2identifier,'-',t3.lbpartno,'-', ltrim(rtrim(t3.position))) as pozitieLog,t4.E2Identifier
			from	get_building_log t1
						inner join registrationxentity_log t2
						on t1.buildingid = t2.buildingid
							inner join registration t3
							on t2.RegistrationID = t3.RegistrationID
								inner join get_land_log t4
								on t1.landid = t4.landid
			) as t1

go
--------------------------------------------------
--select * from #pozitiiActiveCG
--select * from #pozitiiLogIE
--select * from #eterreValabile
--drop table #pozitiiActiveCG
--drop table #pozitiiLogIE
--drop table #eterreValabile

-------------------------------------------------
select  t1.E2Identifier,t1.Lbpartno,t1.pozitieRenumerotare,t2.*
from	#pozitiiActiveCG t1
			right outer join #pozitiiLogIE t2
			on t1.pozitieLog = t2.pozitieLog
				inner join #eterreValabile t3
				on t2.E2Identifier = t3.E2Identifier

where	t1.E2Identifier is null
order by t2.pozitieLog asc
go				



--select distinct t1.E2identifier,t3.*
--			from	get_land t1
--						inner join RegistrationxEntity t2
--						on t1.landid = t2.landid	
--							inner join registration t3
--							on t2.registrationid = t3.registrationid

--			where	t3.positionEterra>0 and t1.landid=14875

