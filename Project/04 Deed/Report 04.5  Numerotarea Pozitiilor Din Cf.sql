

-----lista pozitii pentru imobile cu cf 

SELECT  
		--ROW_NUMBER() OVER (Order by CONVERT(INT, PARSENAME(T3.LANDID, 1))) AS RAND, --- LINIE DE COD CE RETURNEAZA POZITIA CURENTA IN TABEL A RANDULUI
		T3.*,T4.INTRAVILAN 
		
		
FROM	(
					SELECT  DISTINCT SUBSTRING(T1.KEY1, 1, CHARINDEX('-', T1.KEY1)-1) AS LANDID,
							T1.LBPARTNO,T1.POSITION,T1.APPDATE,T1.INITIALQUOTA,T1.ACTUALQUOTA,T1.REGISTRATIONID,T1.COMMENTS, T1.NOTES,T1.DEEDDATE
		
					FROM    (


										SELECT  DISTINCT    CONCAT(T7.LANDID,'-',T2.LBPARTNO,'-',T2.POSITION,'-',T2.InitialQuota,'-',T2.Actualquota) AS KEY1,
															T7.LANDID AS LANDID,T2.LBPARTNO,T2.POSITION,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPDATE,T2.APPNO,T2.INITIALQUOTA,T2.ACTUALQUOTA, T2.COMMENTS, T2.NOTES, T3.DEEDDATE

										FROM	REGISTRATIONXENTITY T1
													INNER JOIN GET_LAND T7
													ON T1.LANDID=T7.LANDID
														INNER JOIN GET_REGISTRATION T2
														ON T1.REGISTRATIONID=T2.REGISTRATIONID
															INNER JOIN DEED T3
															ON T2.DEEDID=T3.DEEDID
										WHERE	T1.ACTIVE='1'

										UNION 

										SELECT  DISTINCT    CONCAT(T8.LANDID,'-',T2.LBPARTNO,'-',T2.POSITION,'-',T2.InitialQuota,'-',T2.Actualquota) AS KEY1,
															T8.LANDID AS LANDID,T2.LBPARTNO,T2.POSITION,T1.REGISTRATIONID AS REGISTRATIONID,T2.APPDATE,T2.APPNO,T2.INITIALQUOTA,T2.ACTUALQUOTA,T2.COMMENTS, T2.NOTES, T3.DEEDDATE

										FROM	REGISTRATIONXENTITY T1
														INNER JOIN GET_BUILDING T8
														ON T1.BUILDINGID=T8.BUILDINGID
															INNER JOIN GET_REGISTRATION T2
															ON T1.REGISTRATIONID=T2.REGISTRATIONID
																INNER JOIN DEED T3
																ON T2.DEEDID=T3.DEEDID
										WHERE	T1.ACTIVE='1'

								) AS T1
								INNER JOIN LANDGEOMETRY T2
								ON T1.LANDID=T2.LANDLOGID
				
				

					

					) AS T3
					INNER JOIN GET_LAND T4
					ON T3.LANDID=T4.LANDID
					--WHERE	T4.INTRAVILAN='0' 

					--aici se completeaza partea 
					AND T3.LBPARTNO=2

					-----------------------------------
					
ORDER BY	CONVERT(INT, PARSENAME(T3.LANDID, 1)),
			CONVERT(INT, PARSENAME(T3.LBPARTNO,1)), 
			CASE WHEN  T3.APPDATE='' THEN 2 ELSE 1 END, 
			--CONVERT(INT, PARSENAME(T3.APPDATE,1))
			CONVERT(INT, PARSENAME(T3.POSITION,1)),
			CONVERT(INT, PARSENAME(T3.DEEDDATE,1))


-----------------------------------------------------------------------------------------------------------------------------------------------------------------

-- inainte de a face renumerotarea pozitiilor este necesar sa se creeze un camp in care sa pastram numerotarea initiala
-- formula din excel =IF(A4>A3;1;D3+1)
-- ordonarea in excel se face dupa urmatoarea ordine: landid, appdate, position




--verificarea pozitiilor renumerotate

--SELECT  *
--FROM	REGISTRATION T1
--			INNER JOIN REGISTRATIONXENTITY T2
--			ON T1.REGISTRATIONID = T2.REGISTRATIONID


--WHERE	T2.LANDID = 14290
--WHERE	POSITION_OLD <> POSITION

-----------------------------------------------------

select  *
from	deed 
where	deednumber=''