---------------------------------------------------------------------------------------------------

--Autoritate emitenta mai mare de 50 de caractere
SELECT  DISTINCT *
FROM	(

					SELECT  T2.LANDID AS LANDID,t2.lbpartno as Partea,t2.position as Pozitia,t3.authority,t3.deedid
					FROM	LANDGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.LANDLOGID=T2.LANDID
									INNER JOIN DEED T3
									ON T2.DEEDID=T3.DEEDID
					WHERE	LEN(T3.authority)>50 

					UNION

					SELECT  T4.LANDID AS LANDID,t2.lbpartno as Partea,t2.position as Pozitia,T3.authority,t3.deedid
					FROM	BUILDINGGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.BUILDINGLOGID=T2.BUILDINGID
									INNER JOIN DEED T3
									ON T2.DEEDID=T3.DEEDID
										INNER JOIN GET_LAND T4
										ON T1.LANDLOGID=T4.LANDID

					WHERE	LEN(T3.authority)>50 

					


		) AS T1

------------------------------- LISTA DATA NUMBER, AUTORITATE EMITENTA LIPSA-----------------------------
SELECT  DISTINCT *
FROM	(

				SELECT  '' AS CONSTRUCTIE, T2.LANDID AS LANDID,t2.lbpartno as Partea,t2.position as Pozitia,T3.DEEDID,t3.deednumber,t3.deeddate, t3.authority,T3.NOTES
				FROM	LANDGEOMETRY T1
							INNER JOIN GET_REGXENTITY T2
							ON T1.LANDLOGID=T2.LANDID
								INNER JOIN DEED T3
								ON T2.DEEDID=T3.DEEDID
				WHERE	T3.DEEDNUMBER IS NULL OR 
						T3.DEEDDATE IS NULL OR 
						T3.AUTHORITY IS NULL OR
						T3.DEEDNUMBER='' OR
						T3.DEEDDATE='' OR
						T3.AUTHORITY=''

				UNION

				SELECT  T2.BUILDINGID AS CONSTRUCTIE,T4.LANDID AS LANDID,t2.lbpartno as Partea,t2.position as Pozitia,T3.DEEDID,t3.deednumber, t3.deeddate,T3.authority,T3.NOTES
				FROM	BUILDINGGEOMETRY T1
							INNER JOIN GET_REGXENTITY T2
							ON T1.BUILDINGLOGID=T2.BUILDINGID
								INNER JOIN DEED T3
								ON T2.DEEDID=T3.DEEDID
									INNER JOIN GET_LAND T4
									ON T1.LANDLOGID=T4.LANDID

				WHERE	T3.DEEDNUMBER IS NULL OR 
						T3.DEEDDATE IS NULL OR 
						T3.AUTHORITY IS NULL OR
						T3.DEEDNUMBER='' OR
						T3.DEEDDATE='' OR
						T3.AUTHORITY=''

	) AS T1


------------------------------- LISTA DATA NUMBER, AUTORITATE EMITENTA LIPSA-----------------------------

