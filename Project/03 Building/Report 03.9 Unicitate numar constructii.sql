--verificare unicitate numar constructii

select  concat(t2.landid,'-',t2.buildno) as 'landid constructie',count(*) as 'de cate ori'
from	buildinggeometry t1
			inner join get_building t2
			on t1.buildinglogid = t2.buildingid

group by concat(t2.landid,'-',t2.buildno)
having count (*) > 1



