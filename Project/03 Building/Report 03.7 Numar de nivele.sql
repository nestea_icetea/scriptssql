--script ce verifica numarul de nivele al constructiilor CORELATE. Acolo unde nu este completat, TREBUIE completat de mana sau default cu 1

SELECT  T2.BUILDINGID, T2.LEVELSNO, T1.REGIM_H
FROM	BUILDINGGEOMETRY T1
			INNER JOIN GET_BUILDING T2
			ON T1.BUILDINGLOGID = T2.BUILDINGID

WHERE	T2.LEVELSNO IS NULL OR
		T2.LEVELSNO = 0

