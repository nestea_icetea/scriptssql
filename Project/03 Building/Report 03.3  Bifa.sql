-- daca o constructie are inregistrari , bifa "are acte" trebuie sa fie pusa, daca nu , invers

----------------------------------------------------------------------------------------------------------------------------------------------------------------
		SELECT  T2.LANDID, T2.BUILDNO,T2.CONDOMINIU, 'trebuie debifat cu acte' as mentiune
		FROM	BUILDINGGEOMETRY T1
					INNER JOIN GET_BUILDING T2
					ON T1.BUILDINGLOGID = T2.BUILDINGID
						FULL OUTER JOIN REGISTRATIONXENTITY T3
						ON T2.BUILDINGID = T3.BUILDINGID
							
		WHERE	T3.ACTIVE=1 AND T2.AREACTE='DA' AND T2.CONDOMINIU='NU' AND T3.REGISTRATIONID IS NULL
		

				union

		SELECT  T2.LANDID, T2.BUILDNO,T2.CONDOMINIU, 'trebuie BIFAT cu acte' as mentiune
		FROM	BUILDINGGEOMETRY T1
					INNER JOIN GET_BUILDING T2
					ON T1.BUILDINGLOGID = T2.BUILDINGID
						FULL OUTER JOIN REGISTRATIONXENTITY T3
						ON T2.BUILDINGID = T3.BUILDINGID
							
		WHERE	T3.ACTIVE=1 AND T2.AREACTE='NU' AND T2.CONDOMINIU='NU' AND T3.REGISTRATIONID IS NOT NULL

				

------------------------------------------------------------------------------------------------------------------

