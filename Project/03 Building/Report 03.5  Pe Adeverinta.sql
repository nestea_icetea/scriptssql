---- lista cu imobilele ce au constructii inscrise pe adeverinta de insusire de la Primarie --
SELECT  T1.*
FROM	(
				SELECT  DISTINCT T6.LANDID as Landid, T6.E2Identifier AS IE, T8.AREA AS 'Suprafata masurata teren', t6.localitate as Localitate, T6.StreetName AS Strada, T6.PostalNumber as Postal, T7.persoana AS Proprietari, t2.buildingid, t4.*
				FROM	BUILDINGGEOMETRY T1
							INNER JOIN GET_BUILDING T2
							ON T1.BUILDINGLOGID = T2.BUILDINGID 
								INNER JOIN REGISTRATIONXENTITY T3
								ON T2.BUILDINGID = T3.BUILDINGID	
									INNER JOIN REGISTRATION T4
									ON T3.RegistrationID = T4.RegistrationID
										INNER JOIN DEED T5
										ON T4.Deedid = T5.DeedID
											INNER JOIN GET_LAND T6
											ON T2.LANDID = T6.LANDID
												INNER JOIN GET_UniquePersonByLand T7
												ON T6.LANDID = T7.LANDID
													INNER JOIN LANDGEOMETRY T8
													ON T6.LANDID = T8.LANDLOGID
							


				WHERE	T3.ACTIVE=1 
						AND
						T4.ACTIVE=1
						AND
						T5.ACTIVE=1
						AND
						T5.DEEDNUMBER like '%10882%' 
						--AND
						--T5.DEEDDATE='23.04.2021'

		) AS T1

		INNER JOIN LANDGEOMETRY T2
		ON T1.LandID = T2.LandLogID

		ORDER BY T1.LANDID ASC


