--verificare daca este preluat la constructii numarul de cf e2identifier

select  distinct t2.landid,t2.buildno,t2.e2identifier,t5.finalizat
from	buildinggeometry t1
			inner join get_building t2
			on t1.buildinglogid = t2.buildingid
				inner join registrationxentity t3
				on t2.buildingid = t3.buildingid
					inner join registration t4
					on t3.registrationid = t4.registrationid
						inner join landgeometry t5
						on t2.landid = t5.landlogid
						

where	(t2.e2identifier ='' or t2.e2identifier is null) and t4.appdate<>'' and t5.finalizat=1