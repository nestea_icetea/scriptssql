--Diferente suprafete imobile cu CF

select  *
from	(
			select  t2.landid,t2.e2identifier,t1.area,t3.parcellegalarea as s_act_log,t3.measuredarea as geometrie_log,t2.enclosed, iif(t3.measuredarea<>0,t1.area-t3.measuredarea,t1.area-t3.parcellegalarea) as verificare
			from	landgeometry t1
						inner join get_land t2
						on t1.landlogid = t2.landid
							full outer join get_land_log t3
							on t2.e2identifier = t3.e2identifier
					
			where	t1.landlogid is not null and (t2.enclosed=0 or t2.enclosed is null)
		) as t1

where	t1.verificare<>0 



--Diferente suprafete imobile fara CF


begin transaction update parcel set usecategoryid=6 where parcelid in (
																			--select  t1.landlogid, t2.parcelid, t2.codeusecategory
																			select  t2.parcelid
																			from	landgeometry t1
																						inner join get_parcel t2
																						on t1.landlogid = t2.landid
																			where	observatii='faneata'
																			)

