-- mutare nr top parcela la teren

BEGIN TRANSACTION
UPDATE LAND
SET LAND.TOPONO = T2_1.LISTA_TOPONO

FROM	GET_LAND T1
			INNER JOIN 
						--AICI SE LEAGA CU PARCELELE LISTA
						(
										SELECT  GET_LAND.LANDID,STUFF((

													SELECT  DISTINCT ', ' + TOPONO
													FROM	GET_PARCEL T1
													WHERE	T1.LANDID=GET_LAND.LANDID
													FOR     XML PATH('')),1,1,'') AS LISTA_TOPONO
									
										FROM								GET_LAND

										GROUP BY GET_LAND.LANDID
						) AS T2_1
						ON T1.LANDID=T2_1.LANDID

WHERE	T2_1.LISTA_TOPONO <>'' AND 
		LAND.LANDID=T2_1.LANDID


commit
------------------------------------------------------------------------------------------------------------------------------------




SELECT LANDID, TOPONO FROM LAND WHERE TOPONO<>''

------------------------------------------------------------------------------------------------------------------------------------

