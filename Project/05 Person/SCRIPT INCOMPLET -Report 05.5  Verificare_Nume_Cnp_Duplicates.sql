
------------------A se revedea scriptul deoarece acel get_personbyland nu scoate si proprietarii de la constructii
	
--############################################### FINAL ################################################################################				

SELECT  DISTINCT CONCAT(T1.LANDID,'-',T1.IDCODE) AS KEY1,CONCAT (T1.FIRSTNAME,' ',T1.LASTNAME) AS PERSOANA,T1.PERSONID
                        INTO #tabelpersonsduplicates
                        FROM	GET_PERSONBYLAND T1
			                        INNER JOIN LANDGEOMETRY T2
			                        ON T1.LANDID=T2.LANDLOGID
                        WHERE	T1.IDCODE<>'9999999999999' and T1.active='1'
                        ORDER BY CONCAT(T1.LANDID,'-',T1.IDCODE) ASC
------------------------------------------------------------------------------
go
------------------------------------------------------------------------------
SELECT  TT.*,T.FIRSTNAME,T.LASTNAME
FROM					(
                        SELECT  CONCAT('Mai exista o alta persoana din cadrul Landlogid: ',SUBSTRING(T1.KEY1, 1, CHARINDEX('-', T1.KEY1)-1),' care are CNP: ',
		                        SUBSTRING(T1.KEY1,CHARINDEX('-', T1.KEY1) +1,1000)) as Mentiune,
		                        SUBSTRING(T1.KEY1, 1, CHARINDEX('-', T1.KEY1)-1) AS LandLogID,
		                        SUBSTRING(T1.KEY1,CHARINDEX('-', T1.KEY1) +1,1000) AS CNP,
		                        REPLACE(T1.PERSOANA,' ','_') AS PERSOANA,T1.PERSONID
		
                        FROM	#tabelpersonsduplicates AS T1
			                        CROSS APPLY   ( SELECT  T2.KEY1,COUNT(KEY1) AS COUNT_KEY1, COUNT(DISTINCT PERSOANA) AS COUNT_DISTINCT_PERSOANA
							                        FROM	#tabelpersonsduplicates AS T2
							                        WHERE	T1.KEY1=T2.KEY1
							                        GROUP BY KEY1) AS CA
								

                        WHERE CA.COUNT_KEY1>1 AND COUNT_DISTINCT_PERSOANA >1) AS TT

						INNER JOIN GET_PERSON T
						ON TT.PERSONID=T.PERSONID
        
where CNP NOT LIKE '%99999%'	

-----------------------------------------------------------------------------
go
---------------------------------------------------------------------------
DROP TABLE #tabelpersonsduplicates
