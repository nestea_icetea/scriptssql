
-------------------------------------------------lista proprietari neidentificati EXTRAVILAN-----------------------------------------------------------------

---------------------------------------------------------------------------------
	SELECT  
					iif(T2.INTRAVILAN='1','Intravilan','Extravilan') as 'Dispunere',t2.localitate as Localitate,t2.Streetname as Strada, t2.postalnumber as 'Numar postal',T2.SECTORCADASTRAL as 'Sector cadastral',T2.CADGENNO as 'Identificator Cadastru General',
					T2_4.LISTA_CATEGORII as 'Categorie de folosinta',t2_2.LISTA_TARLALE as Tarla, t2_1.LISTA_PARCELE as 'Nr parcela',
					t3.nameregistrationtype as 'Tip inscriere actual',T4.DEEDNUMBER as 'Numar act', T4.DEEDDATE as 'Data act',T6.FIRSTNAME as 'Nume proprietar',t6.notes as 'Mentiune Proprietar',round(T1.AREA,3) as 'Suprafata masurata IMOBIL'
			FROM	LANDGEOMETRY T1
						INNER JOIN GET_LAND T2
						ON T1.LANDLOGID=T2.LANDID
			INNER JOIN 
										
						--AICI SE LEAGA CU PARCELELE LISTA
						(
										SELECT  GET_LAND.LANDID,STUFF((

													SELECT  DISTINCT ', ' + PARCELNO
													FROM	GET_PARCEL T1
													WHERE	T1.LANDID=GET_LAND.LANDID
													FOR     XML PATH('')),1,1,'') AS LISTA_PARCELE
									
										FROM								GET_LAND

										GROUP BY GET_LAND.LANDID
						) AS T2_1
			ON T2.LANDID=T2_1.LANDID

			INNER JOIN
						--AICI SE LEAGA CU TARLALELE
						(

									SELECT  GET_LAND.LANDID,STUFF((

										SELECT  DISTINCT ', ' + LANDPLOTNO
										FROM	GET_PARCEL T1
										WHERE	T1.LANDID=GET_LAND.LANDID
										FOR     XML PATH('')),1,1,'') AS LISTA_TARLALE
									
									FROM								GET_LAND

									GROUP BY GET_LAND.LANDID

						) AS T2_2
			ON T2.LANDID=T2_2.LANDID
					

			INNER JOIN
						--AICI SE LEAGA CU TP
						(
									SELECT  GET_LAND.LANDID,STUFF((

									SELECT  DISTINCT ', ' + TITLENO
									FROM	GET_PARCEL T1
									WHERE	T1.LANDID=GET_LAND.LANDID
									FOR     XML PATH('')),1,1,'') AS LISTA_TP
									
								FROM								GET_LAND

								GROUP BY GET_LAND.LANDID

						) AS T2_3
			ON T2.LANDID=T2_3.LANDID

			INNER JOIN
						--AICI SE LEAGA CU CATEGORII DE FOLOSINTA
						(
									SELECT  GET_LAND.LANDID,STUFF((

									SELECT  DISTINCT ', ' + CODEUSECATEGORY
									FROM	GET_PARCEL T1
									WHERE	T1.LANDID=GET_LAND.LANDID
									FOR     XML PATH('')),1,1,'') AS LISTA_CATEGORII
									
								FROM								GET_LAND

								GROUP BY GET_LAND.LANDID

						) AS T2_4
			ON T2.LANDID=T2_4.LANDID
								INNER JOIN GET_REGXENTITY T3
								ON T2.LANDID=T3.LANDID
									INNER JOIN GET_DEED T4
									ON T3.DEEDID=T4.DEEDID
										INNER JOIN REGISTRATIONXPERSON T5
										ON T3.REGISTRATIONID=T5.IDREGISTRATION
											INNER JOIN PERSON T6
											ON T5.IDPERSON=T6.PERSONID
							

			where   
					--t2.intravilan='0' and

					--and t6.firstname like '%COMUNA%' 
					--and 
					t4.deednumber like '%7%' and t4.deeddate LIKE '%13.03.1996%' 
					--t7.codeusecategory = 'DR' 
				    --and t2_4.LISTA_CATEGORII like '%DR%' 
					 --and t7.codeusecategory <> 'P' and t7.codeusecategory <> 'N'
					--AND T6.NOTES NOT LIKE '%REZERVA%'
					and t5.active='1'




			ORDER BY REPLACE(RIGHT(REPLICATE('0', 1000) + LTRIM(RTRIM(CAST(t2.sectorcadastral AS VARCHAR(MAX)))) + REPLICATE('0', 100 - CHARINDEX('.', REVERSE(LTRIM(RTRIM(CAST(t2.sectorcadastral AS VARCHAR(MAX))))), 1)), 1000), '.', '0'),
					REPLACE(RIGHT(REPLICATE('0', 1000) + LTRIM(RTRIM(CAST(t2.cadgenno AS VARCHAR(MAX)))) + REPLICATE('0', 100 - CHARINDEX('.', REVERSE(LTRIM(RTRIM(CAST(t2.cadgenno AS VARCHAR(MAX))))), 1)), 1000), '.', '0')
