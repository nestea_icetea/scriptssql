
------------------------------------------------------------------------------------------------- TABELA TEMPORARA CU PERSOANELE SI CNP-URILE ASOCIATE IN LUCRARE
SELECT	LANDID,PERSONID,FIRSTNAME,LASTNAME,CNP
INTO	#TEMP_CNP_PERSON
FROM	(
										SELECT  DISTINCT *
										FROM	(
												SELECT  T2.LANDID AS LANDID,T4.PERSONID,T4.FIRSTNAME,T4.LASTNAME,T4.IDCODE as CNP,T4.ISPHISICAL,T2.ACTIVE AS ACTIVE1,T3.ACTIVE AS ACTIVE2, T4.ACTIVE AS ACTIVE3
												FROM	LANDGEOMETRY T1
															INNER JOIN REGISTRATIONXENTITY T2
															ON T1.LANDLOGID=T2.LANDID
																INNER JOIN REGISTRATIONXPERSON T3
																ON T2.REGISTRATIONID=T3.IDREGISTRATION
																	INNER JOIN PERSON T4
																	ON T3.IDPERSON=T4.PERSONID

												UNION  

												SELECT  T6.LANDID AS LANDID,T8.PERSONID,T8.FIRSTNAME,T8.LASTNAME,T8.IDCODE AS CNP,T8.ISPHISICAL,T6.ACTIVE AS ACTIVE1,T7.ACTIVE AS ACTIVE2, T8.ACTIVE AS ACTIVE3
												FROM	BUILDINGGEOMETRY T5
															INNER JOIN REGISTRATIONXENTITY T6
															ON T5.BUILDINGLOGID=T6.BUILDINGID
																INNER JOIN REGISTRATIONXPERSON T7
																ON T6.REGISTRATIONID=T7.IDREGISTRATION
																	INNER JOIN PERSON T8
																	ON T7.IDPERSON=T8.PERSONID
			
												--lipseste union si cu proprietarii de la apartamente

											

												) AS T0
									WHERE						
										ISPHISICAL='1' AND 
										LANDID <>'0' AND 
										ACTIVE1='1' AND 
										ACTIVE2='1' AND 
										ACTIVE3='1'	AND
										CNP NOT LIKE '%9999%'
									
						
		) AS T1

-----------------------------------------------------------------
GO
----------------------------------------------------------------- VERIFICARE PROPRIU-ZISA

SELECT  LANDID,PERSONID,FIRSTNAME,LASTNAME,CNP,TEST AS 'Test CNP'
FROM	(

					SELECT  DISTINCT *,IIF(
															(
																(CAST(SUBSTRING(CNP,1,1) AS INT)*'2'+
																CAST(SUBSTRING(CNP,2,1) AS INT)*'7'+
																CAST(SUBSTRING(CNP,3,1) AS INT)*'9'+
																CAST(SUBSTRING(CNP,4,1) AS INT)*'1'+
																CAST(SUBSTRING(CNP,5,1) AS INT)*'4'+
																CAST(SUBSTRING(CNP,6,1) AS INT)*'6'+
																CAST(SUBSTRING(CNP,7,1) AS INT)*'3'+
																CAST(SUBSTRING(CNP,8,1) AS INT)*'5'+
																CAST(SUBSTRING(CNP,9,1) AS INT)*'8'+
																CAST(SUBSTRING(CNP,10,1) AS INT)*'2'+
																CAST(SUBSTRING(CNP,11,1) AS INT)*'7'+
																CAST(SUBSTRING(CNP,12,1) AS INT)*'9'	)

						
																% '11' 
						
																- CAST(SUBSTRING(CNP,13,1) AS INT)   =  '0'
															 )

											or

															(
																	(CAST(SUBSTRING(CNP,1,1) AS INT)*'2'+
																	CAST(SUBSTRING(CNP,2,1) AS INT)*'7'+
																	CAST(SUBSTRING(CNP,3,1) AS INT)*'9'+
																	CAST(SUBSTRING(CNP,4,1) AS INT)*'1'+
																	CAST(SUBSTRING(CNP,5,1) AS INT)*'4'+
																	CAST(SUBSTRING(CNP,6,1) AS INT)*'6'+
																	CAST(SUBSTRING(CNP,7,1) AS INT)*'3'+
																	CAST(SUBSTRING(CNP,8,1) AS INT)*'5'+
																	CAST(SUBSTRING(CNP,9,1) AS INT)*'8'+
																	CAST(SUBSTRING(CNP,10,1) AS INT)*'2'+
																	CAST(SUBSTRING(CNP,11,1) AS INT)*'7'+
																	CAST(SUBSTRING(CNP,12,1) AS INT)*'9'	)

						
																	% '11' -'10'  =  '0'
															)																		
																													,'OK','GRESIT') AS TEST
						FROM	#TEMP_CNP_PERSON
			) AS T1
					
					WHERE	T1.TEST='GRESIT'
----------------------------------------------------------------------------------
GO
---------------------------------------------------------------------------------
DROP TABLE #TEMP_CNP_PERSON



