

--------------  lista cu toate imobilele care sunt intabulate in baza TP si au mai multe de o persoana la teren ----------------------

SELECT  T4.cadgenno as CADGENNO,t4.e2identifier AS IE,T1.LANDID,T1.LBPARTNO AS PARTEA, T1.POSITION AS POZITIE, T2.DEEDNUMBER AS 'NR ACT', T2.DEEDDATE AS 'DATA ACT', T2.AUTHORITY AS 'AUTORITATE EMITENTA',T4.Intravilan AS INTRAVILAN,T5.PERSOANA AS PROPRIETARI
FROM	GET_REGxENTITY T1
			INNER JOIN GET_DEED T2
			ON T1.DEEDID=T2.DEEDID
					INNER JOIN GET_LAND T4
					ON T1.LANDID=T4.LANDID
						INNER JOIN GET_UNIQUEPERSONBYLAND T5
						ON T1.LANDID=T5.LANDID
							--INNER JOIN LANDGEOMETRY T3
							--ON T1.LANDID=T3.LANDLOGID
							

WHERE	T1.REGISTRATIONTYPEID=1 AND 
		T1.NameRegistrationType='Intabulare' AND 
		--T1.NameRightType='PROPRIETATE' AND 
		--T2.NAMEDEEDTYPE='act administrativ' AND 
		--T1.APPDATE='' AND
		T5.PERSOANA LIKE '%,%' AND
		T4.E2IDENTIFIER='' AND
		T2.AUTHORITY LIKE '%CJSDPAT%'
		
ORDER BY T2.DEEDNUMBER ASC

