SELECT	LANDID,PERSONID,FIRSTNAME,LASTNAME,CNP
INTO	#TEMP_CNP_PERSON
FROM	(
										SELECT  DISTINCT *
										FROM	(
												SELECT  T2.LANDID AS LANDID,T4.PERSONID,T4.FIRSTNAME,T4.LASTNAME,T4.IDCODE as CNP,T4.ISPHISICAL,T2.ACTIVE AS ACTIVE1,T3.ACTIVE AS ACTIVE2, T4.ACTIVE AS ACTIVE3
												FROM	LANDGEOMETRY T1
															INNER JOIN REGISTRATIONXENTITY T2
															ON T1.LANDLOGID=T2.LANDID
																INNER JOIN REGISTRATIONXPERSON T3
																ON T2.REGISTRATIONID=T3.IDREGISTRATION
																	INNER JOIN PERSON T4
																	ON T3.IDPERSON=T4.PERSONID

												UNION  

												SELECT  T6.LANDID AS LANDID,T8.PERSONID,T8.FIRSTNAME,T8.LASTNAME,T8.IDCODE AS CNP,T8.ISPHISICAL,T6.ACTIVE AS ACTIVE1,T7.ACTIVE AS ACTIVE2, T8.ACTIVE AS ACTIVE3
												FROM	BUILDINGGEOMETRY T5
															INNER JOIN REGISTRATIONXENTITY T6
															ON T5.BUILDINGLOGID=T6.BUILDINGID
																INNER JOIN REGISTRATIONXPERSON T7
																ON T6.REGISTRATIONID=T7.IDREGISTRATION
																	INNER JOIN PERSON T8
																	ON T7.IDPERSON=T8.PERSONID
			
												--lipseste union si cu proprietarii de la apartamente

											

												) AS T0
									WHERE						
										ISPHISICAL='1' AND 
										LANDID <>'0' AND 
										ACTIVE1='1' AND 
										ACTIVE2='1' AND 
										ACTIVE3='1'	AND
										CNP NOT LIKE '%9999%'
									
						
		) AS T1

-----------------------------------------------------------------
GO
----------------------------------------------------------------- VERIFICARE PROPRIU-ZISA
select * from #TEMP_CNP_PERSON

DECLARE @CNP NVARCHAR(13);
DECLARE @Validation VARCHAR(50);

DECLARE Cursor1 CURSOR FOR
SELECT CNP
FROM #TEMP_CNP_PERSON;

OPEN Cursor1

FETCH NEXT FROM Cursor1 INTO @CNP

WHILE @@FETCH_STATUS = 0
BEGIN
		IF LEN(@CNP) = 13
							BEGIN
							DECLARE @Suma INT;
							DECLARE @Control INT;

								SET @Suma = (SUBSTRING(@CNP, 1, 1) * 2 + SUBSTRING(@CNP, 2, 1) * 7 + 
											 SUBSTRING(@CNP, 3, 1) * 9 + SUBSTRING(@CNP, 4, 1) * 1 + 
											 SUBSTRING(@CNP, 5, 1) * 4 + SUBSTRING(@CNP, 6, 1) * 6 + 
											 SUBSTRING(@CNP, 7, 1) * 3 + SUBSTRING(@CNP, 8, 1) * 5 + 
											 SUBSTRING(@CNP, 9, 1) * 8 + SUBSTRING(@CNP, 10, 1) * 2 + 
											 SUBSTRING(@CNP, 11, 1) * 7 + SUBSTRING(@CNP, 12, 1) * 9) % 11;

								SET @Control = 11 - @Suma;

								IF       @Control = 10 	SET @Control = 1;
								ELSE IF	 @Control = 11 	SET @Control = 0;

								IF @Control = SUBSTRING(@CNP, 13, 1) 	SET @Validation = 'Valid';
	
								ELSE									SET @Validation = 'Invalid';
							END

		ELSE 	SET @Validation = 'Invalid';

PRINT 'CNP: ' + @CNP + ', Validation: ' + @Validation;

FETCH NEXT FROM Cursor1 INTO @CNP;
END

CLOSE Cursor1
DEALLOCATE Cursor1;