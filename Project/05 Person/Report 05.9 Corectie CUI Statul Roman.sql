--- corectie Statul Roman CUI

BEGIN TRANSACTION UPDATE PERSON SET IDCODE = '9999999999999' WHERE PERSONID IN (
	
																				SELECT  DISTINCT PERSONID
																												
																				FROM	(

																							SELECT  *
																															
																							FROM	(
																																			SELECT  T2.LANDID AS LANDID,T4.PERSONID,T4.FIRSTNAME,T4.LASTNAME,T4.IDCODE,T4.ISPHISICAL,T2.ACTIVE AS ACTIVE1,T3.ACTIVE AS ACTIVE2, T4.ACTIVE AS ACTIVE3
																																			FROM	LANDGEOMETRY T1
																																						INNER JOIN REGISTRATIONXENTITY T2
																																						ON T1.LANDLOGID=T2.LANDID
																																							INNER JOIN REGISTRATIONXPERSON T3
																																							ON T2.REGISTRATIONID=T3.IDREGISTRATION
																																								INNER JOIN PERSON T4
																																								ON T3.IDPERSON=T4.PERSONID

																																			UNION  

																																			SELECT  T6.LANDID AS LANDID,T8.PERSONID,T8.FIRSTNAME,T8.LASTNAME,T8.IDCODE,T8.ISPHISICAL,T6.ACTIVE AS ACTIVE1,T7.ACTIVE AS ACTIVE2, T8.ACTIVE AS ACTIVE3
																																			FROM	BUILDINGGEOMETRY T5
																																						INNER JOIN REGISTRATIONXENTITY T6
																																						ON T5.BUILDINGLOGID=T6.BUILDINGID
																																							INNER JOIN REGISTRATIONXPERSON T7
																																							ON T6.REGISTRATIONID=T7.IDREGISTRATION
																																								INNER JOIN PERSON T8
																																								ON T7.IDPERSON=T8.PERSONID
			
																									) AS T0
																																WHERE						
																																	ISPHISICAL='0' AND 
																																	LANDID <>'0' AND 
																																	ACTIVE1='1' AND 
																																	ACTIVE2='1' AND 
																																	ACTIVE3='1'	AND
																																	IDCODE <>'9999999999999' AND
																																	FIRSTNAME LIKE '%STATUL ROMAN%'
									
						
																				) AS T1

																				)

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
COMMIT



