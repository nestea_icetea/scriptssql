-- verificare CUI persoane juridice care contin si litere in CUI
--------------------------------------------------------------------------------------------
SELECT	LANDID,PERSONID,FIRSTNAME,LASTNAME,IDCODE

FROM	(
										SELECT  *
										FROM	(
												SELECT  T2.LANDID AS LANDID,T4.PERSONID,T4.FIRSTNAME,T4.LASTNAME,T4.IDCODE,T4.ISPHISICAL,T2.ACTIVE AS ACTIVE1,T3.ACTIVE AS ACTIVE2, T4.ACTIVE AS ACTIVE3
												FROM	LANDGEOMETRY T1
															INNER JOIN REGISTRATIONXENTITY T2
															ON T1.LANDLOGID=T2.LANDID
																INNER JOIN REGISTRATIONXPERSON T3
																ON T2.REGISTRATIONID=T3.IDREGISTRATION
																	INNER JOIN PERSON T4
																	ON T3.IDPERSON=T4.PERSONID

												UNION  

												SELECT  T6.LANDID AS LANDID,T8.PERSONID,T8.FIRSTNAME,T8.LASTNAME,T8.IDCODE,T8.ISPHISICAL,T6.ACTIVE AS ACTIVE1,T7.ACTIVE AS ACTIVE2, T8.ACTIVE AS ACTIVE3
												FROM	BUILDINGGEOMETRY T5
															INNER JOIN REGISTRATIONXENTITY T6
															ON T5.BUILDINGLOGID=T6.BUILDINGID
																INNER JOIN REGISTRATIONXPERSON T7
																ON T6.REGISTRATIONID=T7.IDREGISTRATION
																	INNER JOIN PERSON T8
																	ON T7.IDPERSON=T8.PERSONID

												union

												select  t2.landid,t5.personid,t5.firstname,t5.lastname,t5.idcode,T5.ISPHISICAL, '1' as ACTIVE1, '1' as ACTIVE2, '1' as ACTIVE3
											
												from	iu t1
															inner join get_building t2
															on t1.buildingid = t2.buildingid
																inner join registrationxentity t3
																on t1.iuid = t3.iuid
																	inner join registrationxperson t4
																	on t3.registrationid = t4.idregistration
																		inner join person t5
																		on t4.idperson=t5.personid

												where			t3.active=1 and t4.active=1 and t5.active=1
			
												) AS T0
									WHERE						
										ISPHISICAL='0' AND 
										LANDID <>'0' AND 
										ACTIVE1='1' AND 
										ACTIVE2='1' AND 
										ACTIVE3='1'	AND
										(IDCODE LIKE '[A-Za-z]%' OR 
																	(IDCODE LIKE '%9999%' AND LEN(IDCODE)<13)
										)
									
						
		) AS T1
-------------------------------------------------------------------------------------------------------------



