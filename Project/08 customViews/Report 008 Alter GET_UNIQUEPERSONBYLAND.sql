USE [SANCRAIU]
GO

/****** Object:  View [dbo].[GET_UniquePersonByLand]    Script Date: 07.05.2020 12:14:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[GET_UniquePersonByLand]
AS
SELECT  *
FROM	 (
										SELECT  GET_LAND.LANDID,STUFF((

													SELECT  DISTINCT ', ' + LTRIM(RTRIM(FIRSTNAME)) + ' ' +  LTRIM(RTRIM(LASTNAME))
													FROM	GET_PERSONBYLAND T1
													WHERE	T1.LANDID=GET_LAND.LANDID
													FOR     XML PATH('')),1,1,'') AS persoana
									
										FROM								GET_LAND

										GROUP BY GET_LAND.LANDID
						) AS T2_1



GO

USE [SANCRAIU]
GO

/****** Object:  View [dbo].[GET_UniquePersonByLand]    Script Date: 07.05.2020 12:14:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

