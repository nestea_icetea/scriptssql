
select t1.landid,sum(cast(t1.intravilan as int)) as sum1,sum(cast(t2.intravilan as int)) as sum2
into #TabelParcels1
from   get_land t1
			inner join get_parcel t2
			on t1.landid=t2.landid
				inner join landgeometry t3
				on t1.landid=t3.landlogid
group by t1.landid 

select landid,iif(sum1>=1 and sum2<1,'Imobilul este in intravilan => trebuie bifata o parcela de intravilan','ok') as Comentariu
from #TabelParcels1
where sum1>=1 and sum2<1
UNION
select landid,iif(sum1=0 and sum2>=1,'Imobilul este in extravilan => trebuie debifata o parcela/mai multe de intravilan','ok') as Comentariu
from #TabelParcels1
where sum1=0 and sum2>=1
Drop table #TabelParcels1


