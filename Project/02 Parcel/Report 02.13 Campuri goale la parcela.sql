-- verificare parcela cu spatii goale 


--mentiuni
begin transaction update parcel set notes='' where parcelid in (
																select  t3.parcelid
																from	landgeometry t1
																			inner join get_land t2
																			on t1.landlogid = t2.landid
																				inner join get_parcel t3
																				on t2.landid = t3.landid

																where	t3.notes='' and datalength (t3.notes) <>0
																)
commit

--tarla
select  t2.landid,t3.notes,t3.parcelid,datalength (t3.landplotno) 
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join get_parcel t3
				on t2.landid = t3.landid

where	t3.landplotno='' and datalength (t3.landplotno) <>0

--parcela
select  t2.landid,t3.notes,t3.parcelid,datalength (t3.parcelno) 
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join get_parcel t3
				on t2.landid = t3.landid

where	t3.parcelno='' and datalength (t3.parcelno) <>0
