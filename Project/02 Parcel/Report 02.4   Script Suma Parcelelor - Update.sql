USE MARACINENI

--################## SCRIPT DE COMPLETARE SUPRAFATA PARCELE PENTRU IMOBILE CARE AU O SINGURA PARCELA #############################
SELECT  DISTINCT t2.cadgenno, t2.landid,t2.parcellegalarea, round(t1.area,0) as Sup_Geomedia , t4.measuredarea, T4.PARCELID, IIF(ROUND(T1.AREA,0)<>T4.MEASUREDAREA,'DIFERIT','OK') AS TEST
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_PARCEL T4
				ON T2.LANDID=T4.LANDID
					--## unit cu toate imobilele care au doar o singura parcela##
						LEFT JOIN		(	SELECT  LANDID
											FROM	GET_PARCEL 
											GROUP BY LANDID
											HAVING COUNT(*)>1) T3
						ON T1.LANDLOGID=T3.LANDID

WHERE	T3.LANDID IS NULL
order by test asc
--###############################################################################################################################


