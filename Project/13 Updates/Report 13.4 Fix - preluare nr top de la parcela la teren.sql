-- script care preia numarul topografic de la parcela si il duce la teren
--pas 1 - preia nr.top de la parcela la teren


BEGIN TRANSACTION UPDATE LAND 
SET LAND.TOPONO = T2.LISTA_TOPO_PARCELA
FROM  LAND 
		INNER JOIN 
					(SELECT  T1.LANDID,REPLACE(T1.LISTA_TOPO,' ,','') AS LISTA_TOPO_PARCELA,T2.TOPONO AS LISTA_TOPO_TEREN
					FROM	(
							SELECT  GET_LAND.LANDID,STUFF((
							SELECT  DISTINCT ', ' + TOPONO
							FROM	GET_PARCEL T1
							WHERE	T1.LANDID=GET_LAND.LANDID
							FOR     XML PATH('')),1,1,'') AS LISTA_TOPO
									
							FROM								GET_LAND

							GROUP BY GET_LAND.LANDID
							) AS T1
								INNER JOIN GET_LAND T2
								ON T1.LANDID = T2.LANDID
					WHERE	LISTA_TOPO <>'' AND LTRIM(RTRIM(REPLACE(T1.LISTA_TOPO,' ,',''))) <> LTRIM(RTRIM(T2.TOPONO))
					) AS T2
			ON LAND.LANDID = T2.LANDID


commit







