-- SCRIPT CARE CREEAZA AUTOMAT ADRESA SI O ASOCIAZA ACELOR PROPRIETARI CARE NU AU NIMIC COMPLETAT (ADDRESSID=0)
begin transaction
-----------variabile adresa
---globale
DECLARE @sirsup nvarchar(6)
SET		@sirsup='20466' --margineni


--folosite in cursor
DECLARE @personid nvarchar(50)
DECLARE @firstname nvarchar(50)
DECLARE @lastname nvarchar(50)

--cauta si salveaza intr-o variabila identificatorul unic al ultimei adrese introduse
DECLARE @adressid nvarchar(50)
SELECT  TOP 1 @adressid = adressid
FROM	ADDRESS_LOG
ORDER BY CONVERT(INT, PARSENAME(adressid, 1)) DESC


--se declara cursorul
DECLARE cursor_adresa CURSOR FOR

--querry
SELECT	personid,firstname,lastname FROM PERSON_LOG WHERE ADDRESSID = 0


--se deschide cursorul
OPEN cursor_adresa

--se stabileste XX
FETCH NEXT FROM cursor_adresa into @personid,@firstname,@lastname
	

--conditii

WHILE @@FETCH_STATUS = 0
		BEGIN
			--creeaza o noua inregistrare in tabela ADDRESS
			INSERT INTO ADDRESS_LOG (SIRSUP,SIRUTA,INTRAVILAN) VALUES (@sirsup,@sirsup,1)		
			
			--se face update la persoana respectiva cu noua valoarea de adressid
			UPDATE PERSON_LOG SET ADDRESSID = @adressid+1 WHERE personid = @personid
		
			--se incrementeaza @adressid
			set @adressid = @adressid + 1

			--pentru a se trece la elementul urmator
			FETCH NEXT FROM cursor_adresa into @personid,@firstname,@lastname
		END;



--se inchide cursorul
CLOSE cursor_adresa
DEALLOCATE cursor_adresa
GO

commit
rollback






--select  addressid,count(*)
--from	person_LOG
--group by AddressID
--having count(*)>1


--SELECT  TOP 1 adressid
--FROM	ADDRESS_LOG
--ORDER BY CONVERT(INT, PARSENAME(adressid, 1)) DESC

