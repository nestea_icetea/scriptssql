

----------------------------------------------------------------------------------------------------------------------------------------------
											--BD (se poate face mereu dupa ce se incarca cgxml-uri)
----------------------------------------------------------------------------------------------------------------------------------------------
BEGIN TRANSACTION UPDATE PERSON SET FIRSTNAME=LASTNAME,LASTNAME='' WHERE ISPHISICAL=0 AND FIRSTNAME='' 
COMMIT


--corectare import persoane fizice bifate ca si juridice
BEGIN TRANSACTION UPDATE PERSON SET ISPHISICAL=1 WHERE ISPHISICAL=0 AND FIRSTNAME<>'' AND LASTNAME<>'' 
COMMIT



----------------------------------------------------------------------------------------------------------------------------------------------
											--LOG (se poate face mereu dupa ce se incarca cgxml-uri)
----------------------------------------------------------------------------------------------------------------------------------------------

--acest script rezolva problema importului gresit al proprietarilor (persoane juridice)
BEGIN TRANSACTION UPDATE PERSON_LOG SET FIRSTNAME=LASTNAME,LASTNAME='' WHERE ISPHISICAL=0 AND FIRSTNAME=''
COMMIT


--corectare import persoane fizice bifate ca si juridice
BEGIN TRANSACTION UPDATE PERSON_LOG SET ISPHISICAL=1 WHERE ISPHISICAL=0 AND FIRSTNAME<>'' AND LASTNAME<>'' 
COMMIT

-- Corectare imobile 
BEGIN TRANSACTION UPDATE PERSON SET ISPHISICAL=1 WHERE FIRSTNAME<>'' AND LASTNAME<>'' AND ISPHISICAL=0
COMMIT


----------------------------------------------------------------------------------------------------------------------------------------------
											--Verificare (nu trebuie sa returneze nimic daca e ok)
----------------------------------------------------------------------------------------------------------------------------------------------
SELECT * FROM PERSON WHERE ISPHISICAL=0 AND FIRSTNAME='' 
SELECT * FROM PERSON WHERE ISPHISICAL=0 AND FIRSTNAME<>'' AND LASTNAME<>'' 
SELECT * FROM PERSON WHERE	FIRSTNAME<>'' AND LASTNAME<>'' AND ISPHISICAL=0
SELECT * FROM PERSON WHERE ISPHISICAL=0 AND FIRSTNAME='' AND LASTNAME<>''
--------------------------------------------------------------------------------------------------
