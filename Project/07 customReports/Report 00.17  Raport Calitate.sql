
----------------------------------------------------------ANEXA 1A -------------------------RAPORT DE CALITATE 10% -PRIMA PREDARE--------------------------------------------------------------------------------
	SELECT	DISTINCT SECTORCADASTRAL as 'Sector cadastral' ,COUNT(T1.SECTORCADASTRAL) AS 'Numar total de imobile',ROUND(COUNT(T1.SECTORCADASTRAL)/10+1,0) AS 'Numar de imobile verificate (10%) ','Lista de imobile verificate' = STUFF ((
																SELECT	TOP (10) PERCENT
																',' + CADGENNO
																FROM	(SELECT  CAST(T1.SECTORCADASTRAL AS NVARCHAR) AS SECTORCADASTRAL,t1.Cadgenno
																		FROM	LAND T1
																				INNER JOIN LANDGEOMETRY T2
																				ON T1.LANDID=T2.LANDLOGID) AS T2
																WHERE	T2.SECTORCADASTRAL=T1.SECTORCADASTRAL 
																ORDER BY CADGENNO
																FOR		XML PATH ('')),1,1,'')
																		FROM	(SELECT  CAST(T1.SECTORCADASTRAL AS NVARCHAR) AS SECTORCADASTRAL,t1.Cadgenno
																				FROM	LAND T1
																						INNER JOIN LANDGEOMETRY T2
																						ON T1.LANDID=T2.LANDLOGID) AS T1
																		GROUP BY T1.SECTORCADASTRAL
																		ORDER BY T1.SECTORCADASTRAL


----------------------------------------------------------ANEXA 1B -------------------------RAPORT DE CALITATE 10% -RAPORT COMPLETARE--------------------------------------------------------------------------------

	SELECT	DISTINCT SECTORCADASTRAL as 'Sector cadastral' ,COUNT(T1.SECTORCADASTRAL) AS 'Numar total de imobile verificate',ROUND(COUNT(T1.SECTORCADASTRAL)/10+1,0) AS 'Numar de imobile verificate intern (10%) ','Lista de imobile verificate' = STUFF ((
																SELECT	TOP (10) PERCENT
																',' + CADGENNO
																FROM	(SELECT  CAST(T1.SECTORCADASTRAL AS NVARCHAR) AS SECTORCADASTRAL,t1.Cadgenno
																		FROM	LAND T1
																				INNER JOIN LANDGEOMETRY T2
																				ON T1.LANDID=T2.LANDLOGID
																					INNER JOIN ACTIVITY T3
																					ON T2.LANDLOGID = T3.LANDID
																				
																										) AS T2
																WHERE	T2.SECTORCADASTRAL=T1.SECTORCADASTRAL 
																ORDER BY CADGENNO
																FOR		XML PATH ('')),1,1,'')
																		FROM	(SELECT  CAST(T1.SECTORCADASTRAL AS NVARCHAR) AS SECTORCADASTRAL,t1.Cadgenno
																				FROM	LAND T1
																						INNER JOIN LANDGEOMETRY T2
																						ON T1.LANDID=T2.LANDLOGID
																							INNER JOIN  ACTIVITY T3
																							ON T2.LANDLOGID = T3.LANDID
																						
																									) AS T1
																		GROUP BY T1.SECTORCADASTRAL
																		ORDER BY T1.SECTORCADASTRAL




-------------------------------------------------------ANEXA 2--------------------------------Diferente eterre inscrieri partea a 3 a-----------------------------------------------------

SELECT  T1.CADGENNO AS 'Identificator Cadastru General - Prestator',T2.E2IDENTIFIER AS 'Identificator e-Terra (IE)',t2.*
FROM	GET_LAND T1
			INNER JOIN (

								SELECT    T1.E2IDENTIFIER,T1.TEST AS 'Are/Nu are inscrieri in partea a 3 a',t2.NUMAR_POZITII_BD AS 'Numarul pozitii fisierul CGXML Prestator - partea a 3 a',t2.NUMAR_POZITII_eterra AS 'Numarul pozitii e-Terra ANCPI - partea a 3 a',t2.Comentariu as 'Diferente intre datele preluate din sistemul integrat de cadastru si carte funciara si cele realizate de Prestator'
								FROM		(
			
											SELECT  DISTINCT T1.E2IDENTIFIER,IIF( MAX(T3.LBPARTNO)='2','Nu are inscrieri in partea a 3 a ','Are inscrieri in partea a 3 a') AS TEST

											FROM	GET_LAND T1 
														INNER JOIN LANDGEOMETRY T2
														ON T1.LANDID=T2.LANDLOGID
															INNER JOIN GET_DEEDBYLANDID T3
															ON T1.LANDID=T3.LANDID
											WHERE	 T1.E2IDENTIFIER>0 and T1.E2IDENTIFIER NOT LIKE '%TMP%'
											GROUP BY T1.E2IDENTIFIER

											) AS T1

																				LEFT OUTER JOIN  (																	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          

																										SELECT  DISTINCT T1.E2IDENTIFIER,t1.numar_pozitii as NUMAR_POZITII_BD, t2.NUMAR_POZITII_ETERRA, iif(t1.numar_pozitii <>t2.numar_pozitii_eterra,'Imobilul prezinta diferente in partea a 3 a','Imobilul nu prezinta diferente in partea a 3 a') as Comentariu

																										FROM	(SELECT  T2.E2IDENTIFIER,T1.*
																												 FROM		(
																																SELECT  DISTINCT T1.LANDID,COUNT(T1.POSITION) AS NUMAR_POZITII
						
																																FROM	GET_REGXENTITY T1
																																			INNER JOIN LANDGEOMETRY T2
																																			ON T1.LANDID=T2.LANDLOGID
																																				INNER JOIN GET_LAND T3
																																				ON T2.LANDLOGID=T3.LANDID
				
																																WHERE	T1.LBPARTNO=3 AND T3.E2IDENTIFIER>0 and T3.E2IDENTIFIER NOT LIKE '%TMP%'
																																GROUP BY T1.LANDID

																															) AS T1
																																				INNER JOIN GET_LAND T2
																																				ON T1.LANDID=T2.LANDID
									
																													) AS T1
																																													INNER JOIN 
																			
																																																										(
																																																										SELECT  T2.E2IDENTIFIER,T1.*
																																																										FROM	(
																																																													SELECT  T1.LANDID, COUNT(T1.POSITION) AS NUMAR_POZITII_ETERRA
																																			
																																																													FROM	GET_REGXENTITY_LOG T1	
																																																													WHERE	T1.LBPARTNO=3 AND T1.LANDID<>'0'
																																																													GROUP BY T1.LANDID

																																																												) AS T1

																																																															INNER JOIN GET_LAND_LOG T2
																																																															ON T1.LANDID=T2.LANDID

																																																										) AS T2
																			
																			
																																												ON T1.E2IDENTIFIER = T2.E2IDENTIFIER


																									) AS T2

																						ON T1.E2IDENTIFIER=T2.E2IDENTIFIER




				) AS T2

				ON T1.E2IDENTIFIER=T2.E2IDENTIFIER

WHERE T1.Cadgenno>0

ORDER BY REPLACE(RIGHT(REPLICATE('0', 1000) + LTRIM(RTRIM(CAST(t2.E2IDENTIFIER AS VARCHAR(MAX)))) + REPLICATE('0', 100 - CHARINDEX('.', REVERSE(LTRIM(RTRIM(CAST(t2.E2IDENTIFIER AS VARCHAR(MAX))))), 1)), 1000), '.', '0') 

-------------------------------------------------------ANEXA 2--------------------------------Diferente eterre inscrieri partea a 3 a-----------------------------------------------------
