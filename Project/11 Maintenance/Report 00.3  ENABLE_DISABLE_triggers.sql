/* Enable/Disable triggers script       */
/* Jason Buss - 2018-04-19              */
/* Enable line for action you want to perform (enable/disable) and run script in it's entirety */
 
DECLARE @action VARCHAR(8) = 'DISABLE';
--DECLARE @action varchar(8) = 'ENABLE';
 
DECLARE @catalog VARCHAR(10) = 'dbo';  --or dbo
DECLARE @disabled BIT= 0;
DECLARE @stmt VARCHAR(255);
DECLARE @loop INT= 0;
DECLARE @cnt INT;
DECLARE @totalcnt INT;
 
IF(@action = 'ENABLE')
    SET @disabled = 1;
 
SELECT @cnt = COUNT(*)
FROM sys.triggers
WHERE is_disabled = @disabled;
SELECT @totalcnt = COUNT(*)
FROM sys.triggers;
 
IF @cnt <> 0
    BEGIN
        WHILE @loop <> @cnt
            BEGIN
                SELECT TOP 1 
				
				@stmt = @action+' TRIGGER '+TRIG.name+' ON '+@catalog+'.'+TAB.name+';'
				FROM [sys].[triggers] AS TRIG
                     
					 INNER JOIN sys.tables AS TAB 
					 ON TRIG.parent_id = TAB.object_id
                
				WHERE trig.is_disabled = @disabled 			
				AND (TRIG.name LIKE '%log%' or TRIG.name LIKE '%Check%') ------------ CONDITIE SE PUNE PENTRU DEAZCTIVAREA ANUMITOR TRIGGERI
				
                ORDER BY TAB.name,TRIG.name;
 
                SET @loop = @loop + 1;
                PRINT @stmt;
                EXEC (@stmt); --disable to print statements only
            END;
        PRINT LOWER(@action)+'d '+CONVERT(VARCHAR(5), @cnt)+' of '+CONVERT(VARCHAR(5), @totalcnt)+' triggers';
    END;
ELSE
   PRINT 'Out of '+CONVERT(VARCHAR(5), @totalcnt)+' triggers, no triggers to '+LOWER(@action);



