
--- informatii cu privire la userul pnccf_intern pentru sesiunea respectiva
---kill connections
USE [master];


DECLARE		 @kill varchar(8000) = '';  
SELECT		@kill = @kill + 'kill ' + CONVERT(varchar(5), session_id) + ';'  
from
			(select  
				t1.session_id,
				t2.name as db,
				t1.login_name,
				t1.host_name,
				t3.client_net_address,
				--datediff(minute ,t1.login_time, t1.last_request_start_time) as minutes_worked,
				t1.login_time,
				t1.last_request_start_time,
				t1.program_name,
				t1.client_version,
				t1.status,
				t1.cpu_time,
				t1.text_size,
				t1.reads,
				t1.writes,
				t1.lock_timeout,
				t1.row_count,
				t3.num_reads,
				t3.num_writes,
				t3.client_tcp_port
	
		
		
		from sys.dm_exec_sessions t1
				inner join sys.databases t2
				on t1.database_id=t2.database_id
					inner join sys.dm_exec_connections t3
					on t1.session_id=t3.session_id
		where	t1.database_id>4 
				--and login_name not in ('sa')
				and t2.name='cristesti'
				--order by last_request_start_time desc				
				--and 
				--datediff(minute ,t1.login_time, t1.last_request_start_time>5 
				--and t2.name='Sancraiu'
				--and t1.host_name='RDK104914'
				--order by login_time asc
				) as t1
--where	t1.client_net_address='192.168.137.20'
EXEC(@kill);

