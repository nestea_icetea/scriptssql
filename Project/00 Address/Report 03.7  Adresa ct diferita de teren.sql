--- Aici se va verifica daca adresa constructiilor este identica cu cea a terenului


-------------------------------------------------------------------------------------------------------------------------------------------------------------------- hash adresa TEREN
												SELECT  T1.LANDID,T2.Buildno AS 'Numarul constructiei', 'adresa diferita fata de teren' as Mentiune
												FROM	(
															SELECT  T2.landid, HASHBYTES('MD5', 
																							CONCAT (
																										cast(T3.SIRSUP as varchar(max)),
																										cast(T3.SIRUTA as varchar(max)),
																										cast(T3.INTRAVILAN as varchar(max)) ,
																										cast(T3.DISTRICTTYPEID as varchar(max)) ,
																										cast(T3.DISTRICTNAME as varchar(max)) ,
																										cast(T3.STREETTYPEID as varchar(max)) ,
																										cast(T3.STREETNAME as varchar(max)) ,
																										cast(T3.POSTALNUMBER as varchar(max)) ,
																										cast(T3.BLOCK as varchar(max)) ,
																										cast(T3.ENTRY as varchar(max)) ,
																										cast(T3.FLOOR as varchar(max)) ,
																										cast(T3.ZIPCODE as varchar(max)) ,
																										cast(T3.DESCRIPTION as varchar(max)) ,
																										cast(T3.SECTION as varchar(max))
																										)
																			) as hashvalue
							
															FROM	LANDGEOMETRY T1
																		INNER JOIN LAND T2
																		ON T1.LANDLOGID = T2.LANDID
																			INNER JOIN ADDRESS T3
																			ON T2.ADDRESSID = T3.ADRESSID
															) AS T1

INNER JOIN 


--------------------------------------------------------------------------------------------------------------------------------------------------------------------- hash adresa CONSTRUCTII
														(SELECT  T2.landid, T2.BuildNo, HASHBYTES('MD5', 
																						CONCAT (
																										cast(T3.SIRSUP as varchar(max)),
																										cast(T3.SIRUTA as varchar(max)) ,
																										cast(T3.INTRAVILAN as varchar(max)) ,
																										cast(T3.DISTRICTTYPEID as varchar(max)) ,
																										cast(T3.DISTRICTNAME as varchar(max)) ,
																										cast(T3.STREETTYPEID as varchar(max)) ,
																										cast(T3.STREETNAME as varchar(max)) ,
																										cast(T3.POSTALNUMBER as varchar(max)) ,
																										cast(T3.BLOCK as varchar(max)) ,
																										cast(T3.ENTRY as varchar(max)) ,
																										cast(T3.FLOOR as varchar(max)) ,
																										cast(T3.ZIPCODE as varchar(max)) ,
																										cast(T3.DESCRIPTION as varchar(max)) ,
																										cast(T3.SECTION as varchar(max))
																										)
																		) as hashvalue
							
														FROM	BUILDINGGEOMETRY T1
																	INNER JOIN BUILDING T2
																	ON T1.BUILDINGLOGID = T2.BUILDINGID
																		INNER JOIN ADDRESS T3
																		ON T2.ADDRESSID = T3.ADRESSID

														) AS T2

ON T1.LANDID = T2.LANDID
WHERE T1.HASHVALUE<>T2.HASHVALUE

ORDER BY CONVERT(INT, PARSENAME(t1.landid, 1)) asc