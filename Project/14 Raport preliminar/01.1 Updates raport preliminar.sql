-- modificarea fisierelor cgxml aferente predarilor din raportul preliminar

--aici se completeaza cu valorile de SIRSUP
DECLARE @sirsup nvarchar(6)
SET		@sirsup='114453'
DECLARE @siruta nvarchar(6)
SET		@siruta='114462'            

begin transaction update address set sirsup=@sirsup where siruta=0
begin transaction update address set sirsup=@sirsup , siruta=@siruta where sirsup>0 and siruta>0

commit
commit
