
SELECT  T2.E2IDENTIFIER,T1.AREA, T2.LANDID,T2.PARCELLEGALAREA
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
WHERE	T1.LINIE='101'
ORDER BY GEOMETRY_YLO DESC


SELECT  ID,E2IDENTIFIER, AREA
FROM	LANDGEOMETRY_BKP
ORDER BY GEOMETRY_YlO DESC



SELECT   T1.ID,T1.IDENTIFIER,T2.LANDID
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.IDENTIFIER=T2.CADGENNO

WHERE	T1.LINIE='101'


SELECT  T1.ID, T1.LANDLOGID,T2.CADGENNO
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID

WHERE	T2.CADGENNO='' OR T2.CADGENNO IS NULL


SELECT  DISTINCT T1.E2IDENTIFIER
FROM	GET_LAND T1
			LEFT OUTER JOIN LANDGEOMETRY T2
			ON T1.LANDID = T2.LANDLOGID
WHERE   T2.LANDLOGID IS NULL AND
		T1.E2IDENTIFIER>0



SELECT  T2.CADGENNO, T1.AREA
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN GET_DEEDBYLANDID T3
				ON T2.LANDID = T3.LANDID

WHERE	T3.DEEDNUMBER = '214' AND T3.DEEDDATE='10.07.2000'


		
select * from land_log where e2identifier=32559


SELECT  t2.cadgenno,t2.landid,t2.e2identifier,t1.area, t2.parcellegalarea,t3.measuredarea 
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				FULL OUTER JOIN GET_LAND_LOG T3
				ON T2.E2IDENTIFIER=T3.e2IDENTIFIER

WHERE	T1.LINIE='a2'
order by geometry_ylo desc




SELECT  T2.CADGENNO,T2.LANDID,T2.E2IDENTIFIER,T2.INTRAVILAN,T2.ENCLOSED,T1.AREA,T2.PARCELLEGALAREA,T3.PARCELLEGALAREA AS SUP_ACT_CF_SPORADIC,T3.MEASUREDAREA AS SUP_CF_SPORADIC,T4.AREA AS SUP_MAS_SPORADIC
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				FULL OUTER JOIN GET_LAND_LOG T3
				ON T2.E2IDENTIFIER = T3.E2IDENTIFIER 
					FULL OUTER JOIN LANDGEOMETRY_LOG T4
					ON T3.E2IDENTIFIER= T4.IDENTIFIER

WHERE	T1.LANDLOGID IS NOT NULL



---CGXML


SELECT  T2.E2IDENTIFIER,
		T2.CADGENNO,T4.*, 
		T7.FIRSTNAME,
		T7.LASTNAME
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN REGISTRATIONXENTITY T3
				ON T2.LANDID = T3.LANDID
					INNER JOIN GET_REGISTRATION T4
					ON T3.REGISTRATIONID = T4.REGISTRATIONID
						INNER JOIN GET_DEED T5
						ON T4.DEEDID = T5.DEEDID
							INNER JOIN REGISTRATIONXPERSON T6
							ON T3.REGISTRATIONID = T6.IDREGISTRATION
								INNER JOIN PERSON T7
								ON T6.IDPERSON = T7.PERSONID


WHERE	T3.ACTIVE=1 AND T4.ACTIVE=1 AND T6.ACTIVE=1 AND T7.ACTIVE=1 AND
		T4.LBPARTNO=3 and T2.E2IDENTIFIER>0

ORDER BY T2.E2IDENTIFIER ASC





------IE

SELECT  T2.E2IDENTIFIER,
		T2.CADGENNO,T4.*, 
		T7.FIRSTNAME,
		T7.LASTNAME
FROM	GET_LAND_LOG T2
		INNER JOIN REGISTRATIONXENTITY_LOG T3
				ON T2.LANDID = T3.LANDID
					INNER JOIN GET_REGISTRATION_LOG T4
					ON T3.REGISTRATIONID = T4.REGISTRATIONID
						INNER JOIN GET_DEED_LOG T5
						ON T4.DEEDID = T5.DEEDID
							INNER JOIN REGISTRATIONXPERSON_LOG T6
							ON T3.REGISTRATIONID = T6.IDREGISTRATION
								INNER JOIN PERSON_LOG T7
								ON T6.IDPERSON = T7.PERSONID


WHERE	
		--T3.ACTIVE=1 AND T4.ACTIVE=1 AND T6.ACTIVE=1 AND T7.ACTIVE=1 AND
		T4.LBPARTNO=3 and T2.E2IDENTIFIER>0

ORDER BY T2.E2IDENTIFIER ASC



SELECT  ltrim(rtrim(t2.sectorcadastral)),ltrim(rtrim(t2.cadgenno))
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID


select * from landgeometry


select * from pv_contestatii



SELECT  T2.CADGENNO
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE T2.CADGENNO = 0 OR T2.CADGENNO IS NULL


SELECT  T2.cadgenno,T2.E2IDENTIFIER, T5.DEEDNUMBER,T5.DEEDDATE,T6.CODEUSECATEGORY,T6.LANDPLOTNO,T6.PARCELNO,T6.NOTES
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN REGISTRATIONXENTITY T3
				ON T2.LANDID = T3.LANDID
					INNER JOIN REGISTRATION T4
					ON T3.REGISTRATIONID = T4.REGISTRATIONID
						INNER JOIN GET_DEED T5
						ON T4.DEEDID = T5.DEEDID
							INNER JOIN GET_PARCEL T6
							ON T2.LANDID = T6.LANDID

WHERE	T6.CODEUSECATEGORY = 'DR' AND T5.DEEDNUMBER='6' AND T6.NOTES NOT LIKE '%trada%'

SELECT * FROM GET_PARCEL



SELECT t2.landid, t2.cadgenno
FROM   LANDGEOMETRY T1
				INNER JOIN GET_LAND T2
				ON T1.LANDLOGID = T2.LANDID

WHERE	T2.CADGENNO =''


SELECT  --DISTINCT T5.IDREGISTRATION
		T7.INTRAVILAN,T7.CADGENNO, T7.LANDID,T4.DEEDNUMBER,T4.DEEDDATE,T6.PERSONID,T6.FIRSTNAME,T6.LASTNAME,T6.NOTES
FROM	LANDGEOMETRY T1
			INNER JOIN REGISTRATIONXENTITY T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN REGISTRATION T3
				ON T2.REGISTRATIONID = T3.REGISTRATIONID
					INNER JOIN DEED T4
					ON T3.DEEDID = T4.DEEDID
						INNER JOIN REGISTRATIONXPERSON T5
						ON T2.REGISTRATIONID = T5.IDREGISTRATION
							INNER JOIN PERSON T6
							ON T5.IDPERSON = T6.PERSONID
								INNER JOIN GET_LAND T7
								ON T1.LANDLOGID = T7.LANDID

WHERE	T2.ACTIVE=1 AND T3.ACTIVE=1 AND T4.ACTIVE=1 AND T5.ACTIVE=1 AND T6.ACTIVE=1 AND
		T4.DEEDNUMBER LIKE '%7%' AND T4.DEEDDATE LIKE '%13.03.1996%'
		AND T7.INTRAVILAN=1

		)
		



select  'intravilan' as adresa, count(t2.landid) as 'nr imobile', sum(t1.area)/10000 as suprafata_ha
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid


where	t2.intravilan=1

union

select  'extravilan' as adresa, count(t2.landid) as 'nr imobile', sum(t1.area)/10000 as suprafata_ha
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join get_parcel t3
				on t


where	t2.intravilan=0
------------------------
SELECT  T3.CADGENNO,t2.landid,T2.NUMBER,T2.PARCELNO
                            FROM	LANDGEOMETRY T1
										INNER JOIN GET_PARCEL T2
										ON T1.LANDLOGID=T2.LANDID
											INNER JOIN GET_LAND T3
											ON T1.LANDLOGID=T3.LANDID

						WHERE	T2.PARCELNO LIKE ' ' or T2.PARCELNO LIKE '  '

--select * 
--from	(
--			SELECT CONCAT(T2.LANDID,'-',T2.BUILDNO) AS 'LANDID - NR CONSTRUCTIE',COUNT(CONCAT(T2.LANDID,'-',T2.BUILDNO)) AS DUBLURA
--									FROM	BUILDINGGEOMETRY T1
--												INNER JOIN GET_BUILDING T2
--												ON T1.BUILDINGLOGID=T2.BUILDINGID

--									GROUP BY CONCAT(T2.LANDID,'-',T2.BUILDNO)
--		) as t1
--where	  dublura>1


--select * 
--from	get_building t1
--			inner join buildinggeometry t2
--			on t1.buildingid=t2.buildinglogid
--where t1.landid = 4041



select  t2.cadgenno
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid=t2.landid

SELECT *
FROM	GET_UNIQUEDEEDBYLANDID


select distinct localitate from get_land


select * from land_log where e2identifier='76536'


select * from landgeometry where landlogid is null


--- NR DE IMOBILE CU CF NEASOCIATE

SELECT  T4.E2IDENTIFIER,T4.INTRAVILAN
FROM	(
		SELECT  T2.E2IDENTIFIER
		FROM	LANDGEOMETRY T1
					INNER JOIN GET_LAND T2
					ON T1.LANDLOGID = T2.LANDID) AS T3
							RIGHT OUTER JOIN GET_LAND_LOG T4
							ON T3.E2IDENTIFIER = T4.E2IDENTIFIER
								

WHERE	T3.E2IDENTIFIER IS NULL 
		AND T4.E2IDENTIFIER IS NOT NULL
	


SELECT  T2.E2IDENTIFIER, T2.CADGENNO
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
WHERE	T2.E2IDENTIFIER>0

SELECT  CADGENNO,E2IDENTIFIER
FROM	LAND 
WHERE	CADGENNO>0 AND E2IDENTIFIER>0


SELECT COUNT(*) FROM LANDGEOMETRY_BACKUP_10_08_2020

select sola from tarla


SELECT DISTINCT T3.LANDPLOTNO 
FROM	GET_LAND T1
			INNER JOIN LANDGEOMETRY T2
			ON T1.LANDID= T2.LANDLOGID
				INNER JOIN GET_PARCEL T3
				ON T1.LANDID= T3.LANDID

WHERE	T1.INTRAVILAN=0





SELECT *
FROM	LANDGEOMETRY
WHERE	(IDENTIFIER is null or identifier ='') and landlogid >0


select  *
from	landgeometry_backup_10_08_2020

select * from pv_contestatii


select * from deed_log
where	notes like '%deces%'



--BEGIN TRANSACTION UPDATE REGISTRATIONXENTITY
--SET		ACTIVE=0
--WHERE	REGISTRATIONXENTITY IN (

--									SELECT  --T2.CADGENNO,
--											T3.REGISTRATIONXENTITY
--											--,T3.REGISTRATIONID,T4.REGISTRATIONID,T4.NOTES,T5.DEEDNUMBER,T5.DEEDDATE
--									FROM	LANDGEOMETRY T1
--												INNER JOIN GET_LAND T2
--												ON T1.LANDLOGID = T2.LANDID
--													INNER JOIN REGISTRATIONXENTITY T3
--													ON T2.LANDID = T3.LANDID
--														INNER JOIN REGISTRATION T4
--														ON T3.REGISTRATIONID=T4.REGISTRATIONID
--															INNER JOIN DEED T5
--															ON T4.DEEDID = T5.DEEDID

--									WHERE	T5.DEEDNUMBER='17' AND T5.DEEDDATE='07.03.2014'
--											AND T3.ACTIVE='1' AND T4.ACTIVE='1' AND T5.ACTIVE='1'
--											AND T2.CADGENNO IN (
--											8631,1417,1341,2584,1558,2391,1802,2326,1934,741,3094,1022,1710,1963,1214,1313,2413,794,2052,1304,5612,3865,822,657,1945,601,1021,1415,1798,973,847,1312,611,2173,2573,1451,1213,704,2616,1442,2325,1881,293,1317,1502,3970,1167,1408,992,2690,2583,892,1303,1414,247,890,1460,8744,525,246,3004,359,5556,1164,3091,1825,245,186,477,1929,358,177,2409,645,985
--											)

--									)






select  t2.e2identifier 
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid

where	t2.e2identifier not like '%TMP%' and t2.e2identifier <>''




--BEGIN TRANSACTION UPDATE BUILDINGGEOMETRY SET BUILDINGLOGID=NULL



select * 
from pv_contestatii


select * 
from   registrationxentity where landid='14918'

select * from registration where notes like '%posesia%'

select * from pv_contestatii




--ADEVERINTA NUMAR 3928/17.05.2019


SELECT  T2.CADGENNO,t5.*
FROM	get_land t2
			
					INNER JOIN REGISTRATIONXENTITY T3
					ON T2.LANDID = T3.LANDID 
						INNER JOIN REGISTRATION T4
						ON T3.REGISTRATIONID = T4.REGISTRATIONID
							INNER JOIN DEED T5
							ON T4.DEEDID = T5.DEEDID
WHERE	t2.landid='3010'
		


update deed set deeddate='01.01.2000' where deedid=30820



SELECT  t2.sectorcadastral,t2.cadgenno as id,t1.nr as nr_cerere,t1.data_contest as  data_cerere 
FROM	PV_CONTESTATII T1
				INNER JOIN GET_LAND T2
				on t1.id_imobil = t2.cadgenno


where t1.nr='122'



--update buildinggeometry set buildinglogid=null


SELECT  T2.SECTORCADASTRAL,T1.ID_IMOBIL,'394' AS PV, '05.12.2019' AS DATA_PV, T1.NR AS NR_CERERE, T1.DATA_CONTEST 
FROM	PV_CONTESTATII T1
			INNER JOIN GET_LAND T2
			ON T1.ID_IMOBIL = T2.CADGENNO

WHERE	T1.NR=394





			--LISTA IMOBILE PE TP (EXTRAVILAN)
			SELECT  DISTINCT TOP 10 T2.CADGENNO
			FROM	LANDGEOMETRY T1
						INNER JOIN GET_LAND T2
						ON T1.LANDLOGID = T2.LANDID
							INNER JOIN REGISTRATIONXENTITY T3
							ON T2.LANDID = T3.LANDID
								INNER JOIN REGISTRATION T4
								ON T3.REGISTRATIONID = T4.REGISTRATIONID
									INNER JOIN DEED T5
									ON T4.DEEDID = T5.DEEDID
										LEFT OUTER JOIN PV_CONTESTATII T6
										ON T2.CADGENNO = T6.ID_IMOBIL
											--INNER JOIN GET_BUILDING T7
											--ON T2.LANDID = T7.LANDID
								
			WHERE	(T2.E2IDENTIFIER='' OR T2.E2IDENTIFIER IS NULL)
					--AND T5.AUTHORITY like '%NP%'
					--AND T5.AUTHORITY ='CJSDPT BUZAU'
					--AND T2.INTRAVILAN=1
					--AND T2.NOTES NOT LIKE '%Intravilan conform PUG (H.C.L 53/20.09.2018)%'
					AND T6.ID_IMOBIL IS NULL
					AND T3.ACTIVE=1
					--AND T1.AREA=T2.PARCELLEGALAREA
					--AND T4.RIGHTCOMMENT LIKE '%mostenire%'
					AND T5.DEEDTYPEID=5



select * from get_land



SELECT  T2.LANDID, T2.STREETNAME,T2.POSTALNUMBER ,T2.PAPERCADNO,T2.PAPERLBNO,T2.TOPONO,T2.LOCALITATE
FROM	LANDGEOMETRY T1
			RIGHT OUTER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T1.LANDLOGID IS NULL AND T2.E2IDENTIFIER>0  and t2.e2identifier not like '%-%'
ORDER BY T2.LOCALITATE,T2.POSTALNUMBER ASC



select t2.sectorcadastral,t2.cadgenno 
from landgeometry t1
					inner join get_land t2
					on t1.landlogid = t2.landid
select * from landgeometry



select * from land where cadgenno=10191

select  sum(area)/10000 as suprafata
from	landgeometry_log




select  t1.landid,t1.e2identifier,t1.papercadno
from	land t1
			
where	t1.landid>13108 and t1.active=1


select  t3.identifier
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				right outer join landgeometry_log  t3
				on t2.e2identifier = t3.identifier

where	t1.landlogid is null

SELECT  T2.LANDID,T2.E2IDENTIFIER
FROM	LANDGEOMETRY T1
			RIGHT OUTER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T1.LANDLOGID IS NULL
		AND T2.PAPERCADNO<>''



UPDATE LANDGEOMETRY SET PROPRIETARI = '%COMUNA CRISTESTI' WHERE PROPRIETARI IS NULL




select  t1.identifier,t2.topono, t3.topono
from	landgeometry_log t1
			inner join get_parcel_log t2
			on t1.landlogid = t2.landid
				inner join get_land_log t3
				on t1.landlogid = t3.landid

where	t2.topono <>'' or t3.topono<>'' 



select  t2.cadgenno, t4.*
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join registrationxentity t3
				on t2.landid = t3.landid
					inner join registration t4
					on t3.registrationid = t4.registrationid

where	t4.registrationtypeid=2 and t3.active =1 and t4.notes like '%poses%' and t2.e2identifier=''


SELECT  T2.CADGENNO
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2 
			ON T1.LANDLOGID = T2.LANDID


			select * from parcel


SELECT *
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN GET_PARCEL T3
				ON T2.LANDID =T3.LANDID

WHERE	T3.LANDPLOTNO = '6'



SELECT  T1.ID, T1.IDENTIFIER, T2.LANDID 
FROM	LANDGEOMETRY T1
			FULL OUTER JOIN GET_LAND T2
			ON T1.IDENTIFIER = T2.CADGENNO

WHERE T1.IDENTIFIER IS NOT NULL AND T1.IDENTIFIER <>'' AND T1.LANDLOGID  IS NULL



SELECT  T2.CADGENNO, T3.PERSOANA,T2.PARCELLEGALAREA
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN GET_UNIQUEPERSONBYLAND T3
				ON T2.LANDID = T3.LANDID

WHERE	T2.CADGENNO IN (2885,6171,2177,6544,8583,7479,3401,1085,8599,8598,7981,7357,5724,5322,5604,7447)



			SELECT  T1.IDENTIFIER, 
					T2.E2IDENTIFIER , 
					'' AS IE_NOU, 
					T2.PARCELLEGALAREA, 
					T1.AREA AS SUP_MAS_LAND, 
					T3.LANDPLOTNO,
					T3.PARCELNO,
					T4.PERSOANA,
					T5.LISTA_ACTE
			FROM	LANDGEOMETRY T1
						INNER JOIN GET_LAND T2
						ON T1.LANDLOGID = T2.LANDID
							INNER JOIN GET_PARCEL T3
							ON T2.LANDID = T3.LANDID
								INNER JOIN  GET_UNIQUEPERSONBYLAND T4
								ON T2.landid = T4.LANDID
									INNER JOIN GET_UNIQUEDEEDBYLANDID T5
									ON T1.LANDLOGID = T5.LANDID


			WHERE	T1.OBSERVATII ='linia2'

ORDER BY GEOMETRY_XLO DESC

SELECT * FROM GET_UNIQUEDEEDBYLANDID



--- script VALI
-- lista cu eterre care nu sunt integrate
-- lista cu eterre sistate

SELECT  *
FROM	BUILDINGGEOMETRY_LOG T1
			INNER JOIN GET_BUILDING_LOG T2
			ON T1.BUILDINGLOGID = T2.BUILDINGID
				INNER JOIN GET_LAND_LOG T3
				ON T2.LANDID = T3.LANDID
					LEFT OUTER JOIN GET_LAND T4
					ON T3.E2IDENTIFIER = T4.E2IDENTIFIER

WHERE	T4.LANDID IS NULL




--L2
SELECT  T1.IDENTIFIER AS ID, T3.LANDPLOTNO AS TARLA, T3.PARCELNO AS PARCELA, T5.LISTA_ACTE AS ACT, T1.AREA AS SUPRAFATA, T6.PERSOANA AS PROPRIETAR
FROM	LANDGEOMETRY_L2_BACKUP T1
			INNER JOIN GET_LAND T2
			ON T1.IDENTIFIER = T2.CADGENNO
				INNER JOIN GET_PARCEL T3
				ON T2.LANDID = T3.LANDID
					INNER JOIN GET_UNIQUEDEEDBYLANDID T4
					ON T2.LANDID = T4.LANDID
						INNER JOIN GET_UNIQUEDEEDBYLANDID T5
						ON T2.LANDID = T5.LANDID
							INNER JOIN GET_UNIQUEPERSONBYLAND T6
							ON T2.LANDID = T6.LANDID

WHERE	T2.CADGENNO IN (17901,12767,18353,18815,15843,12145,18335,16964,7967,4044,21559)
ORDER BY GEOMETRY_YLO DESC


--L3
SELECT  T1.IDENTIFIER AS ID, T3.LANDPLOTNO AS TARLA, T3.PARCELNO AS PARCELA, T5.LISTA_ACTE AS ACT, T1.AREA AS SUPRAFATA, T6.PERSOANA AS PROPRIETAR
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.IDENTIFIER = T2.CADGENNO
				INNER JOIN GET_PARCEL T3
				ON T2.LANDID = T3.LANDID
					INNER JOIN GET_UNIQUEDEEDBYLANDID T4
					ON T2.LANDID = T4.LANDID
						INNER JOIN GET_UNIQUEDEEDBYLANDID T5
						ON T2.LANDID = T5.LANDID
							INNER JOIN GET_UNIQUEPERSONBYLAND T6
							ON T2.LANDID = T6.LANDID

WHERE	T2.CADGENNO IN (26769,17901,12767,18353,18815,15843,12145,18335,16964,7967,4044,21559)
ORDER BY GEOMETRY_YLO DESC


select  t2.sectorcadastral, t2.cadgenno as id
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				
ORDER BY t2.sectorcadastral,CONVERT(INT, PARSENAME(t2.cadgenno, 1)) ASC



SELECT  T1.NRPOSTAL AS POSTAL_GEO, T2.POSTALNUMBER AS POSTAL_TXT, T2.LANDID, T2.E2IDENTIFIER, T2.ADDRESSID
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T1.NRPOSTAL <> T2.POSTALNUMBER AND T1.NRPOSTAL<>''


SELECT  ID,IDENTIFIER, LANDLOGID, AREA
FROM	LANDGEOMETRY T1
			LEFT OUTER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T2.LANDID IS NULL

ORDER BY T1.AREA DESC



SELECT *
FROM	LANDGEOMETRY T1
			INNER JOIN REGISTRATIONXENTITY T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T1.OBSERVATII='STALP'


BEGIN TRANSACTION UPDATE ADDRESS SET SIRSUP='114355', SIRUTA='114373' WHERE ADRESSID IN (

SELECT  T1.LANDLOGID, T1.INTRAVILAN AS INTRAVILAN_GEOMEDIA, T2.INTRAVILAN AS INTRAVILAN_TXT,T2.SIRSUP,T2.SIRUTA,T2.E2IDENTIFIER, 
		T1.LOCALITATE,
		T2.ADDRESSID 

FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				--FULL OUTER JOIN GET_PARCEL T3
				--ON T1.LANDLOGID = T3.LANDID

WHERE	T1.INTRAVILAN='0' AND T2.SIRUTA<>'0'


		)
select * from landgeometry

SELECT  ID as id_geomedia,identifier, sector,landlogid,(geometry_xlo + geometry_xhi)/2 as test
FROM	LANDGEOMETRY
ORDER BY CONVERT(INT, PARSENAME(sector, 1)), (geometry_xlo + geometry_xhi)/2 asc
--CONVERT(INT, PARSENAME(landlogid, 1)) ASC





--BEGIN TRANSACTION UPDATE LAND SET ENCLOSED=1 WHERE LANDID IN		(
--																	SELECT  T2.LANDID
--																			--T2.ADDRESSID,T2.ENCLOSED, T1.LANDLOGID,T1.IMPREJMUIRE
--																	FROM	LANDGEOMETRY T1
--																				INNER JOIN GET_LAND T2
--																				ON T1.LANDLOGID = T2.LANDID

--																	WHERE T1.IMPREJMUIRE = '1' 
--																	)



	
				


SELECT  T2.LANDID, T2.PAPERCADNO
from	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T2.PAPERCADNO<>''


SELECT  T2.LANDID,T2.BUILDNO,T2.NOTES
FROM	BUILDINGGEOMETRY T1
			INNER JOIN GET_BUILDING T2
			ON T1.BUILDINGLOGID = T2.BUILDINGID


			
SELECT  T2.LANDID,T6.IDREGISTRATION,T5.DEEDNUMBER,T5.DEEDDATE,T6.IDPERSON
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN REGISTRATIONXENTITY T3
				ON T2.LANDID =T3.LANDID
					INNER JOIN REGISTRATION T4
					ON T3.REGISTRATIONID = T4.REGISTRATIONID
						INNER JOIN DEED T5
						ON T4.DEEDID = T5.DEEDID
							FULL JOIN REGISTRATIONXPERSON T6
							ON T3.REGISTRATIONID = T6.IDREGISTRATION

WHERE T5.DEEDNUMBER='7' AND T5.DEEDDATE='13.03.1996' AND T3.ACTIVE=1 AND T6.ACTIVE=1 AND T5.ACTIVE=1 AND T4.ACTIVE=1


SELECT  T2.*
FROM	REGISTRATIONXENTITY T1
			INNER JOIN REGISTRATIONXPERSON T2
			ON T1.REGISTRATIONID = T2.IDREGISTRATION
				INNER JOIN REGISTRATION T3
				ON T1.REGISTRATIONID = T3.REGISTRATIONID
					INNER JOIN DEED T4
					ON T3.DEEDID = T4.DEEDID
			
			
WHERE 

----PERSOANA PENTRU INTRAVILAN --29973
----PERSOANA PENTRU EXTRAVILAN --27441

----INTRODUCERE PERSOANE LA INSCRIERE
--SELECT  T1.LANDLOGID, T2.REGISTRATIONID,T5.IDREGISTRATION,T5.IDPERSON,T4.DEEDNUMBER,T4.DEEDDATE 
--FROM	LANDGEOMETRY T1
--			INNER JOIN REGISTRATIONXENTITY T2
--			ON T1.LANDLOGID = T2.LANDID
--				INNER JOIN REGISTRATION T3
--				ON T2.REGISTRATIONID = T3.REGISTRATIONID
--					INNER JOIN DEED T4
--					ON T3.DEEDID = T4.DEEDID
--						FULL JOIN REGISTRATIONXPERSON T5
--						ON T3.REGISTRATIONID = T5.IDREGISTRATION
--							INNER JOIN GET_LAND T6
--							ON T1.LANDLOGID = T6.LANDID
				
				 
--WHERE T4.DEEDNUMBER='7' AND T4.DEEDDATE='13.03.1996' AND T5.IDREGISTRATION IS NULL AND T6.INTRAVILAN=0
--ORDER BY T1.LANDLOGID ASC


--SELECT * FROM REGISTRATIONXPERSON
--ORDER BY IDREGXPERSON ASC


--begin transaction update landgeometry set observatii='dublura' where landlogid in (							
--																				select  t1.landlogid 
--																				from	landgeometry t1
--																							inner join get_land t2
--																							on t1.landlogid = t2.landid

--																				group by t1.landlogid
--																				having count(*)>1
--																				)


select t2.landid
from  iu t1
		inner join get_building t2
		on t1.buildingid = t2.buildingid
group by t2.landid



select  *
from	landgeometry t1
			inner join registrationxentity t2
			on t1.landlogid = t2.landid
				inner join registration t3
				on t2.registrationid = t3.registrationid
						inner join get_deed t4
						on t3.deedid = t4.deedid

where	t4.deednumber='123'



--- lista cu prop. stersi din CGR si au inscrieri pe TP
SELECT * FROM GET_UNIQUEPERSONBYLAND


select  t2.cadgenno,t2.landid, t2.sectorcadastral
from 	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid

where t1.landlogid > 14319


SELECT  LANDID, PERSONID, FIRSTNAME, LASTNAME,SIRSUP,SIRUTA,DESCRIPTION,ADDRESSID
										FROM	(
												SELECT  T2.LANDID AS LANDID,T4.PERSONID,T4.FIRSTNAME,T4.LASTNAME,T2.ACTIVE AS ACTIVE1,T3.ACTIVE AS ACTIVE2, T4.ACTIVE AS ACTIVE3,T4.ADDRESSID,T5.SIRSUP,T5.SIRUTA,T5.DESCRIPTION
												FROM	LANDGEOMETRY T1
															INNER JOIN REGISTRATIONXENTITY T2
															ON T1.LANDLOGID=T2.LANDID
																INNER JOIN REGISTRATIONXPERSON T3
																ON T2.REGISTRATIONID=T3.IDREGISTRATION
																	INNER JOIN PERSON T4
																	ON T3.IDPERSON=T4.PERSONID
																		INNER JOIN ADDRESS T5
																		ON T4.ADDRESSID = T5.ADRESSID

												UNION  

												SELECT  T6.LANDID AS LANDID,T8.PERSONID,T8.FIRSTNAME,T8.LASTNAME,T6.ACTIVE AS ACTIVE1,T7.ACTIVE AS ACTIVE2, T8.ACTIVE AS ACTIVE3,T8.ADDRESSID,T9.SIRSUP,T9.SIRUTA,T9.DESCRIPTION
												FROM	BUILDINGGEOMETRY T5
															INNER JOIN REGISTRATIONXENTITY T6
															ON T5.BUILDINGLOGID=T6.BUILDINGID
																INNER JOIN REGISTRATIONXPERSON T7
																ON T6.REGISTRATIONID=T7.IDREGISTRATION
																	INNER JOIN PERSON T8
																	ON T7.IDPERSON=T8.PERSONID
																		INNER JOIN ADDRESS T9
																		ON T8.ADDRESSID = T9.ADRESSID
			
												) AS T0
									WHERE						
										--ISPHISICAL='1' AND 
										LANDID <>'0' AND 
										ACTIVE1='1' AND 
										ACTIVE2='1' AND 
										ACTIVE3='1'	AND
										SIRSUP=SIRUTA AND (DESCRIPTION IS NOT NULL OR DESCRIPTION<>'')
										

-------------------------------------------------------------------------------------------------------



SELECT  T1.LANDLOGID,T3.ENCLOSED AS IMPREJMUIT, T2.PARCELID, T2.NUMBER AS NUMAR_PARCELA,T2.LANDPLOTNO AS TARLA,T2.PARCELNO AS PARCELA,T2.NOTES AS MENTIUNE
FROM	LANDGEOMETRY T1
			INNER JOIN GET_PARCEL T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN GET_LAND T3
				ON T1.LANDLOGID = T3.LANDID
			
WHERE	T2.NOTES<>''
ORDER BY LANDLOGID ASC

-------------------------------------------------------------------------------------------------------


SELECT  T1.LANDLOGID,T1.IMPREJMUIT,T2.ENCLOSED
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T2.ENCLOSED <> T1.IMPREJMUIT


-------------------------------------------------------------

select distinct t2.landid
from	iu t1
			inner join get_building t2
			on t1.buildingid = t2.buildingid



-----------------------------------------------------------------------------------------------

SELECT  distinct T5.IDPERSON
		,T1.IDENTIFIER,T5.IDREGXPERSON,T6.*
	FROM	LANDGEOMETRY T1
			INNER JOIN REGISTRATIONXENTITY T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN REGISTRATION T3
				ON T2.REGISTRATIONID = T3.REGISTRATIONID
					INNER JOIN DEED T4
					ON T3.DEEDID = T4.DEEDID
						INNER JOIN REGISTRATIONXPERSON T5
						ON T3.REGISTRATIONID = T5.IDREGISTRATION
							INNER JOIN PERSON T6
							ON T5.IDPERSON = T6.PERSONID
								
					


	WHERE	T2.ACTIVE=1 AND 
			T3.ACTIVE=1 AND 
			T5.ACTIVE=1 AND 
			T6.ACTIVE=1 AND
			T6.LASTNAME=''

ORDER BY T6.FIRSTNAME ASC


-------------------------------------------------------------------------------------------------
select parcelid,t2.notes
from	landgeometry t1
			inner join get_parcel t2
			on t1.landlogid = t2.landid

where	t2.codeusecategory='A' and notes like '%vegetatie%'




-------------------------------------------------------------------------------------------------

select  t2.landid, t2.addressid,t2.streetname
from	landgeometry t1
				inner join get_land t2
				on t1.landlogid = t2.landid
where	streetname<>''




--------------------------------------------------------------------------------------------------

SELECT  T1.LANDLOGID,T1.OBSERVATII, T1.DENUMIRE,T2.NOTES,T2.CODEUSECATEGORY
FROM	LANDGEOMETRY T1
			INNER JOIN GET_PARCEL T2
			ON T1.LANDLOGID = T2.LANDID 

WHERE	T2.CODEUSECATEGORY = 'N' OR T2.CODEUSECATEGORY = 'HB' OR T2.CODEUSECATEGORY = 'HR' 

---------------------------------------------------------------------------------------------------


select e2identifier from land where active=1 and e2identifier>0


select * from land where e2identifier=60478



------------------------------------------------------------------------------



select distinct landlogid
from person t2 
	inner join landgeometry t1	
	on t1.landlogid = t2.fileid
where isphisical=0 and fatherinitial<>'' 

------------------------------------------------------------------------------



select  t2.e2identifier
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid

where	t2.e2identifier>0


select	* 
from	


---------------------------------------------------------------------------------------


select  t2.landid, t2.e2identifier ,t2.measuredarea, t2.parcellegalarea, t2.streetname,t2.postalnumber, t2.papercadno, t2.paperlbno, t2.topono, t2.localitate, t2.notes
from	landgeometry t1
			right join get_land t2
			on t1.landlogid = t2.landid

where	t2.e2identifier<>'' and t1.landlogid is null and t2.e2identifier not like '%-%' 

order by e2identifier asc

---------------------------------------------------------------------------------------

select  *
from	land t1
			inner join registrationxentity t2
			on t1.landid = t2.landid
				inner join registration t3
				on t2.registrationid = t3.registrationid
					inner join deed t4
					on t3.deedid = t4.deedid

where	t2.active=1 and t3.active=1 and t4.active=1 and 

-------------------------------------------------------------------------------------


select	distinct t2.landid
from	 iu t1
			inner join get_building t2
			on t1.buildingid = t2.buildingid
				inner join buildinggeometry t3
				on t2.buildingid = t3.buildinglogid


-------------------------------------------------------------------------------------

SELECT  *
FROM	GET_LAND_LOG T1
			INNER JOIN REGISTRATIONXENTI


	--------------------------------------------------

	select * from address


select  *
from	(
			select  t2.landid,t2.e2identifier,t1.area,t3.parcellegalarea as s_act_log,t3.measuredarea as geometrie_log, iif(t3.measuredarea<>0,t1.area-t3.measuredarea,t1.area-t3.parcellegalarea) as verificare
			from	landgeometry t1
						inner join get_land t2
						on t1.landlogid = t2.landid
							full outer join get_land_log t3
							on t2.e2identifier = t3.e2identifier
					
			where	t1.landlogid is not null and t2.enclosed=0
		) as t1

where	t1.verificare<>0
			

----------------------------------------------------------------------------------

SELECT  distinct  T4.E2IDENTIFIeR
FROM	IU T1
			INNER JOIN GET_BUILDING T2
			ON T1.BUILDINGID = T2.BUILDINGID
				INNER JOIN BUILDINGGEOMETRY T3
				ON T2.BUILDINGID = T3.BUILDINGLOGID
					INNER JOIN GET_LAND T4
					ON T2.LANDID = T4.LANDID



select distinct buildingid, count(*) as numar
from iu 
group by buildingid
having count(*)>2



select  distinct t3.e2identifier as IE, count(*) as numar_apartamente
from	iu t1
			inner join get_building t2
			on t1.buildingid = t2.buildingid 
				inner join get_land t3
				on t2.landid = t3.landid

group by t3.e2identifier


------------------------------------------------------------------


select  distinct t2.landlogid, t1.iuid,t1.apno,t1.entry, t5.firstname,t5.lastname
from	iu t1
			inner join buildinggeometry t2
			on t1.buildingid = t2.buildinglogid
				inner join registrationxentity t3
				on t1.iuid = t3.iuid
					inner join registrationxperson t4
					on t3.registrationid = t4.idregistration
						inner join person t5
						on t4.idperson = t5.personid


where	t5.firstname like '%atul%' and t3.active=1 and t4.active=1 and t5.active=1

---------------------------------------------------------------

select	id,observatie, posesie, teren, proprietar 
from	probleme

select * from land_log where e2identifier=50432

select * from parcel_log

select  *
from	building where landid=7519 and buildno=1


-------------------------------------------------------






select  *
from	get_building t1
			inner join registrationxentity t2
			on t1.buildingid = t2.buildingid
				inner join registration t3
				on t2.registrationid = t3.registrationid
where	t1.landid =1959 and t3.lbpartno=2 and t3.position=1
				
	
update registration set position_old='' where registrationid=29737
----------------------------------------------------------------------------


select * from get_building where landid=1857


select t3.e2identifier,t1.landid,t1.buildno,t1.levelsno, t1.totalarea, t2.area 
from get_building t1
		inner join buildinggeometry t2
		on t1.buildingid= t2.buildinglogid
			inner join get_land t3
			on t1.landid = t3.landid


--where landid =12796 
where	t1.totalarea<>t2.area and t1.totalarea>0
order by t3.e2identifier, t1.buildno asc

select t2.e2identifier,t1.landid,t1.buildno,t1.levelsno, t1.totalarea,t3.area,t1.notes
from get_building_log t1
			inner join get_land_log t2
			on t1.landid = t2.landid
				full outer join buildinggeometry_log t3
				on t1.buildingid= t3.buildinglogid




----------------------------------------------------------------------------

select  t2.landid,t2.parcellegalarea,t3.measuredarea,t3.parcellegalarea
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid 
				inner join get_land_log t3
				on t2.e2identifier=t3.e2identifier

where	t2.e2identifier>0 and t2.parcellegalarea<>t3.parcellegalarea

---------------------------------------------------------------------



select  count(distinct t2.e2identifier) as 'IE cu constructii' 
from	buildinggeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid


where t2.e2identifier>0


Select * from registration_log
------------------------------------------------------------------------

select  t3.id,t1.e2identifier,t1.landid as landid_old,t2.landid as landid_new
from	land t1
		 inner join land t2
		 on REPLACE(t1.e2identifier, '-', '') =t2.e2identifier
			inner join landgeometry t3
			on t1.landid = t3.landlogid

where t1.e2identifier<>'' and t1.e2identifier like '%-%'


select * from land


-----------------------------------------------------------------
select  t1.landlogid, t2.landlogid
from	parcelare t1
			left outer join landgeometry t2
			on t1.landlogid = t2.landlogid


where t2.landlogid is null
order by t1.landlogid asc


-----------------------------------------------------------------



select  distinct t3.e2identifier as IE,t3.streetname,t3.postalnumber,'' as 'Numar apartamente', t3.landid 
from	iu t1
			inner join get_building t2
			on t1.buildingid = t2.buildingid
				inner join get_land t3
				on t2.landid = t3.landids
					inner join landgeometry t4
					on t3.landid = t4.landlogid
	
	

-------------------------------------------------------------------------------
select  distinct t2.landid,t4.notes
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join registrationxentity t3
				on t2.landid = t3.landid
					inner join registration t4
					on t3.registrationid = t4.registrationid
						inner join deed t5
						on t4.deedid = t5.deedid


where	t3.active=1 and t4.active=1 and t5.active=1 and (t4.registrationtypeid=2 or t4.registrationtypeid=3) and t5.deednumber='8468'


----------------------------------------------------------------------------

--SELECT  T3.LANDID,T3.NOTES AS MENTIUNE_TEREN, T3.PARCELLEGALAREA AS S_ACT1, T3.MEASUREDAREA AS S_ACT2, T1.AREA AS S_MAS, T2.PARCELID, T2.NOTES
--FROM	LANDGEOMETRY T1
--			INNER JOIN GET_PARCEL T2
--			ON T1.LANDLOGID = T2.LANDID
--				INNER JOIN GET_LAND T3
--				ON T1.LANDLOGID = T3.LANDID
				

--WHERE	T2.NOTES LIKE '%diferenta%'



------------------------------------------------------- lista acte 
SELECT  DISTINCT *
FROM	(

					SELECT  T4.E2IDENTIFIER AS IE,t4.paperlbno,t4.papercadno,T2.LANDID AS LANDID,t2.lbpartno as Partea,t2.appdate,t2.appno,t3.deednumber,t3.deedid, '' AS deeddate
					FROM	LANDGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.LANDLOGID=T2.LANDID
									INNER JOIN DEED T3
									ON T2.DEEDID=T3.DEEDID
										INNER JOIN GET_LAND T4
										ON T1.LANDLOGID = T4.LANDID
					WHERE	T3.DEEDDATE LIKE '%01.01%' AND T3.ACTIVE='1'AND T2.ACTIVE='1'

					UNION

					SELECT  T4.E2IDENTIFIER AS IE,t4.paperlbno,t4.papercadno, T4.LANDID AS LANDID,t2.lbpartno as Partea,t2.appdate,t2.appno,t3.deednumber,t3.deedid,'' as deeddate
					FROM	BUILDINGGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.BUILDINGLOGID=T2.BUILDINGID
									INNER JOIN DEED T3
									ON T2.DEEDID=T3.DEEDID
										INNER JOIN GET_LAND T4
										ON T1.LANDLOGID=T4.LANDID

					WHERE	T3.DEEDDATE LIKE '%01.01%' AND T3.ACTIVE='1' AND T2.ACTIVE='1'


		) AS T1

WHERE IE<>''



-----------------------------------------------------------------------------------------------------

select  distinct t2.landid,t2.streetname,t2.postalnumber,t2.Localitate, t7.firstname,t7.lastname
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join registrationxentity t3
				on t2.landid = t3.landid	
					inner join registrationxperson t4
					on t3.registrationid = t4.idregistration
						inner join registration t5
						on t3.registrationid = t5.registrationid	
							inner join deed t6
							on t5.deedid = t6.deedid
								inner join person t7
								on t4.idperson = t7.personid
						



where	t6.deednumber='6352' and t3.active=1 and t4.active=1 and t5.active=1

----------------------------------------------------------------------------------------------------


SELECT  T2.CADGENNO, T2.LANDID, T3.LANDPLOTNO AS TARLA, T3.PARCELNO AS PARCELA,T3.CODEUSECATEGORY AS CATEGORIE_FOLOSINTA, T4.PERSOANA
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN GET_PARCEL T3
				ON T2.LANDID = T3.LANDID
					INNER JOIN GET_UNIQUEPERSONBYLAND T4
					ON T2.LANDID = T4.LANDID

WHERE	T3.CODEUSECATEGORY IN ('P','F')

-----------------------------------------------------------

SELECT  t2.Cadgenno as ID,t1.area as S_mas,t2.parcellegalarea as S_act1,t2.measuredarea as S_act2, t2.e2identifier as IE, T3.ParcelLegalArea, T3.MeasuredArea, T4.LANDPLOTNO AS Tarla, T4.PARCELNO as Parcela, T4.TITLENO as Titlu
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID
				INNER JOIN GET_LAND_LOG T3
				ON T2.E2IDENTIFIER = T3.E2Identifier
					INNER JOIN GET_PARCEL T4
					ON T2.LANDID = T4.LANDID

WHERE T1.OBSERVATII='exproprieri'

------------------------------------------------------------------------------------------------------------
select  t2.cadgenno, t2.notes
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid

where	t2.notes<>''







select t1.landlogid , count(*)
from	 landgeometry t1
		

group by t1.landlogid
having count(*)>1


--------------------------
select  t2.landid
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid


-------------------------------------
select  *
from	registration t1
			inner join registration_log t2
			on concat(t1.appno, '-', t1.appdate) = concat(t2.appno, '-'


------------------------------------------------

select * from registration where positioneterra is not null

-----------------------------------------------

select  identifier
from	landgeometry
group by identifier 
having count (*)>1



select  *
from	registrationxentity t1
			inner join registration t2
			on t1.registrationid = t2.registrationid

where t1.landid = 2026
and t1.active=1



SELECT  DISTINCT T3.LANDLOGID
FROM	GET_IU	T1 
		INNER JOIN GET_BUILDING T2
		ON T1.BUILDINGID = T2.BUILDINGID
			INNER JOIN LANDGEOMETRY T3
			ON T2.LandID = T3.LANDLOGID



-----

begin transaction update address set intravilan=0, Siruta=0 where adressid in (


select  t2.addressid
		--t1.landlogid, t1.localitate,t2.intravilan,
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				full join get_land_log t3
				on t2.e2identifier = t3.e2identifier
					

where	t1.localitate is null and t2.intravilan=1

)


-----------------------------


SELECT  T2.LANDID, T2.NOTES, T3.PARCELID, T3.NOTES
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID	
				INNER JOIN GET_PARCEL T3
				ON T2.LANDID = T3.LANDID

WHERE	T3.NOTES <>''




------ script pentru primaria corunca cu 

select  t3.landid,concat('C', t3.buildno, ' (', t3.codedestination, ')',' - ',iif(t6.deednumber like '%6352%','fara acte', 'cu acte')) as descriere_ct
into #tabel_corunca
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join get_building t3
				on t2.landid = t3.landid
					full join registrationxentity t4
					on t3.buildingid = t4.buildingid 
						inner join registration t5
						on t4.registrationid = t5.registrationid
							inner join deed t6
							on t5.deedid = t6.deedid	
	
where	t4.Active=1 and t3.landid is not null


select * 
from  #tabel_corunca 

order by landid, descriere_ct asc


SELECT T2.LANDID AS ID_CADASTRU_GENERAL,T2.E2IDENTIFIER AS IE,T2.LOCALITATE,T2.STREETNAME AS STRADA,T2.POSTALNUMBER AS NUMARPOSTAL,T1.LISTA_CONSTRUCTII 

FROM		(
				SELECT  GET_LAND.LANDID,rtrim(ltrim(STUFF((

																	SELECT  DISTINCT ', ' + descriere_ct
																	FROM	#tabel_corunca T1
																	WHERE	T1.LANDID=GET_LAND.LANDID 
																	FOR     XML PATH('')),1,1,''))) AS lista_constructii
									
														FROM								GET_LAND
										
														GROUP BY GET_LAND.LANDID

				) AS T1
				INNER JOIN GET_LAND T2
				ON T1.LANDID = T2.LANDID
WHERE LISTA_CONSTRUCTII IS NOT NULL AND LISTA_CONSTRUCTII LIKE'%fara%'
ORDER BY T1.LANDID ASC





---------------------------------------------------

SELECT  T1.LANDLOGID,t2.SectorCadastral
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID = T2.LANDID

WHERE	T2.SectorCadastral IS NULL OR T2.SectorCadastral='-1' 


-----------------------------------------------

SELECT	DISTINCT T3.Cadgenno 
FROM	GET_BUILDING T1
			INNER JOIN LANDGEOMETRY T2
			ON T1.LANDID = T2.LANDLOGID
				INNER JOIN GET_LAND T3
				ON T2.LANDLOGID = T3.LANDID





select  t2.E2Identifier,t1.* 
from	filelogdescription t1
			inner join land_log t2
			on t1.Fileid = t2.Fileid
order by importdate desc



select distinct uat, Implementat_i_p,Implementat_s_p,data 
from progres_log where data='2023-02-22'





