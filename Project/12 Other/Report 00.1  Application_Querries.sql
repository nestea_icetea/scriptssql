--Report1 --- dubluri landlogid

Select		Landlogid, count(landlogid) as NR_imobile
From		Landgeometry
WHERE		LANDLOGID IS NOT NULL
Group BY	LandLogID
HAVING		count(*)>1

Select  count(*) 
From	(Select		Landlogid, count(landlogid) as NR_imobile
		From		Landgeometry
		WHERE		LANDLOGID IS NOT NULL
		Group BY	LandLogID
		HAVING		count(*)>1) t1

--Report2 --- imobile necorelate sau cu landlogid gresit scris
Select		t1.id as ID_Geomedia, t1.Landlogid
from		Landgeometry t1
				left join get_land t2
				on t1.landlogid=t2.landid
where		t1.landlogid is null or t1.landlogid=0 or t1.landlogid='' or t2.landid is null

Select		count(*)
from		Landgeometry t1
				left join get_land t2
				on t1.landlogid=t2.landid
where		t1.landlogid is null or t1.landlogid=0 or t1.landlogid='' or t2.landid is null

--Report 3 -- Persoane fara adresa completata 
select  DISTINCT T1.LANDID,t1.PERSONID, t1.firstname, t1.lastname,t2.addressid
from get_personbyland t1
		inner join person t2
		on t1.personid=t2.personid
			inner join landgeometry t3
			on t1.landid=t3.landlogid
where	  t2.addressid =0 


select  count(*)
from get_personbyland t1
		inner join person t2
		on t1.personid=t2.personid
			inner join landgeometry t3
			on t1.landid=t3.landlogid
where	  t2.addressid =0 

--report 3.1 Persoane cu adresa gresita
SELECT  distinct T2.Landid,T1.FirstName,T1.LastName,T1.Sirsup,T1.Siruta,T1.Intravilan,T1.ADDRESSID
FROM	GET_PERSON T1
			INNER JOIN GET_PERSONBYLAND T2
			ON T1.PERSONID=T2.PERSONID
				INNER JOIN LANDGEOMETRY T3
				ON T2.LANDID=T3.LANDLOGID


WHERE	T1.SIRSUP='0' OR T1.SIRUTA='0' OR T1.INTRAVILAN='0'
ORDER BY T2.LANDID ASC

SELECT  distinct count(*)
FROM	GET_PERSON T1
			INNER JOIN GET_PERSONBYLAND T2
			ON T1.PERSONID=T2.PERSONID
				INNER JOIN LANDGEOMETRY T3
				ON T2.LANDID=T3.LANDLOGID


WHERE	T1.SIRSUP='0' OR T1.SIRUTA='0' OR T1.INTRAVILAN='0'


--Report 4 -- Inscrieri fara nr.act, data act, autoritate emitenta
SELECT  distinct T1.LANDLOGID,T3.DEEDID, t3.deednumber, t3.deeddate, t3.authority, t4.e2identifier
					FROM	LANDGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.LANDLOGID=T2.LANDID
									INNER JOIN GET_DEED T3
									ON T2.DEEDID=T3.DEEDID
										inner join get_land t4
										on t1.landlogid=t4.landid
					where	t3.deednumber='' or t3.deednumber is null or t3.DeedDate='' or t3.DeedDate is null 
					or t3.authority=''


SELECT  distinct count(*)
					FROM	LANDGEOMETRY T1
								INNER JOIN GET_REGXENTITY T2
								ON T1.LANDLOGID=T2.LANDID
									INNER JOIN GET_DEED T3
									ON T2.DEEDID=T3.DEEDID
										inner join get_land t4
										on t1.landlogid=t4.landid
					where	t3.deednumber='' or t3.deednumber is null or t3.DeedDate='' or t3.DeedDate is null 
					or t3.authority=''

--Report 5 -- Eterre fara tip act
SELECT DISTINCT 'Tipul actului nu este completat' as Mentiune,t2.landid,T1.LBPARTNO AS PARTEA, T1.POSITION AS POZITIA,T4.DeedNumber
		FROM GET_REGISTRATION T1
			INNER JOIN GET_REGXENTITY T2
			ON T1.RegistrationID=T2.REGISTRATIONID
				INNER JOIN GET_DEEDBYLANDID T4
					ON T1.RegistrationID=T4.RegistrationID
						FULL OUTER JOIN GET_BUILDING T5
						ON T2.BUILDINGID=T5.BUILDINGID
							FULL OUTER JOIN GET_LAND T6
							ON T2.LANDID=T6.LANDID
								INNER JOIN LANDGEOMETRY T7
								ON T6.LANDID=T7.LANDLOGID
--WHERE	(T3.DEEDNAME IS NULL OR T4.NAMEDEEDTYPE IS NULL) AND T2.ACTIVE=1
WHERE	T4.NAMEDEEDTYPE IS NULL AND T2.ACTIVE=1  

SELECT DISTINCT count(*)
		FROM GET_REGISTRATION T1
			INNER JOIN GET_REGXENTITY T2
			ON T1.RegistrationID=T2.REGISTRATIONID
				INNER JOIN GET_DEEDBYLANDID T4
					ON T1.RegistrationID=T4.RegistrationID
						FULL OUTER JOIN GET_BUILDING T5
						ON T2.BUILDINGID=T5.BUILDINGID
							FULL OUTER JOIN GET_LAND T6
							ON T2.LANDID=T6.LANDID
								INNER JOIN LANDGEOMETRY T7
								ON T6.LANDID=T7.LANDLOGID
--WHERE	(T3.DEEDNAME IS NULL OR T4.NAMEDEEDTYPE IS NULL) AND T2.ACTIVE=1
WHERE	T4.NAMEDEEDTYPE IS NULL AND T2.ACTIVE=1  



--Report 6 Parcele cu suprafata 0 mp

SELECT  T2.Landid, T3.NUMBER as ParcelNumber,T3.MeasuredArea
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_PARCEL T3
				ON T1.LANDLOGID=T3.LANDID

WHERE	T3.MEASUREDAREA=0

SELECT  count(*)
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_PARCEL T3
				ON T1.LANDLOGID=T3.LANDID

WHERE	T3.MEASUREDAREA=0

--Report 7 Mentiuni parcele loclaizare incerta
SELECT  T1.Landid, t3.number as ParcelNumber, T3.Notes as Notes
						FROM	GET_LAND T1
								INNER JOIN LANDGEOMETRY T2
								ON T1.LANDID=T2.LANDLOGID
									INNER JOIN GET_PARCEL T3
									ON T1.LANDID=T3.LANDID
					WHERE	T3.NOTES LIKE '%LOCALIZARE%' OR T3.NOTES LIKE '%localizare%' OR T3.NOTES LIKE '%certa%'

SELECT  count(*)
						FROM	GET_LAND T1
								INNER JOIN LANDGEOMETRY T2
								ON T1.LANDID=T2.LANDLOGID
									INNER JOIN GET_PARCEL T3
									ON T1.LANDID=T3.LANDID
					WHERE	T3.NOTES LIKE '%LOCALIZARE%' OR T3.NOTES LIKE '%localizare%' OR T3.NOTES LIKE '%certa%'

--Report 8  - Constructii fara acte
select T1.Landid,T1.Buildingid,T1.Buildno as NumarConstructie,T1.AreActe 
from get_building T1
	INNER JOIN BUILDINGGEOMETRY T2
	ON T1.BuildingID=T2.BUILDINGLOGID
where	T1.islegal=0 and T1.active=1
order by T1.landid,T1.buildno

select count(*)
from get_building T1
	INNER JOIN BUILDINGGEOMETRY T2
	ON T1.BuildingID=T2.BUILDINGLOGID
where	T1.islegal=0 and T1.active=1


--Report 9 Constructii aflate in Geomedia dar nu sunt legate de textuala
select  t1.id as ID_Geomedia_Constructie,t1.identifier as NumarConstructie,t1.Landlogid
from buildinggeometry t1
		left outer join get_building t2
		on t1.buildinglogid=t2.buildingid
where t2.buildingid is null

select  count(*)
from buildinggeometry t1
		left outer join get_building t2
		on t1.buildinglogid=t2.buildingid
where t2.buildingid is null

--Report 10 Constructii aflate in Geomedia dar nu sunt legate de textuala
SELECT	distinct T1.Landid,T1.BUILDNO as NumarConstructie
FROM	GET_BUILDING T1
			LEFT OUTER JOIN BUILDINGGEOMETRY T2
			ON T1.BUILDINGID=T2.BUILDINGLOGID
				INNER JOIN LANDGEOMETRY T3
				ON T1.LANDID=T3.LANDLOGID
WHERE	T2.BUILDINGLOGID IS NULL 

SELECT	 count(*)
FROM	GET_BUILDING T1
			LEFT OUTER JOIN BUILDINGGEOMETRY T2
			ON T1.BUILDINGID=T2.BUILDINGLOGID
				INNER JOIN LANDGEOMETRY T3
				ON T1.LANDID=T3.LANDLOGID
WHERE	T2.BUILDINGLOGID IS NULL 

--Report 11 ValueAmount cu virgula si trebuie cu punct
SELECT  'Inregistrarea nu trebuie sa contina ,(virgula) ci .(punct)' as Mentiune, t2.Landlogid,t1.Lbpartno as Partea, t1.position as Pozitie, T1.VALUEAMOUNT
FROM	GET_REGXENTITY T1
			INNER JOIN LANDGEOMETRY T2
			ON T1.LANDID=T2.LANDLOGID

WHERE	T1.VALUEAMOUNT LIKE '%,%'

SELECT  count(*)	
FROM	GET_REGXENTITY T1
			INNER JOIN LANDGEOMETRY T2
			ON T1.LANDID=T2.LANDLOGID

WHERE	T1.VALUEAMOUNT LIKE '%,%'

--Report 12 - Persoane care nu au completat CNP

Select  t1.Landid,t1.FirstName, t1.LastName, t1.IdCode as CNP
from	get_personbyland t1
			inner join landgeometry t2
			on t1.landid=t2.landlogid
where	t1.IDcode is null or t1.idcode=''

Select  count(*)
from	get_personbyland t1
			inner join landgeometry t2
			on t1.landid=t2.landlogid
where	t1.IDcode is null or t1.idcode=''

--Report 13 Proprietari neidentificati extravilan
SELECT  T2.CADGENNO,T2.INTRAVILAN, T2.SECTORCADASTRAL,
		T7.CODEUSECATEGORY,t7.number as ParcelNumber,
		T4.DEEDNUMBER, T4.DEEDDATE,T6.FIRSTNAME,T6.LASTNAME,T6.NOTES,T5.IDREGISTRATION,T6.PERSONID,round(T1.AREA,3) as Suprafata_mas
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_PARCEL T7
				ON T2.LANDID=T7.LANDID
					INNER JOIN GET_REGXENTITY T3
					ON T2.LANDID=T3.LANDID
						INNER JOIN GET_DEED T4
						ON T3.DEEDID=T4.DEEDID
							INNER JOIN REGISTRATIONXPERSON T5
							ON T3.REGISTRATIONID=T5.IDREGISTRATION
								INNER JOIN PERSON T6
								ON T5.IDPERSON=T6.PERSONID
							

where  t2.intravilan='0' and t4.deednumber='7' and t4.deeddate='13.03.1996' and t6.notes not like '%REZERVA%'

SELECT  count(distinct T2.landid)
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_PARCEL T7
				ON T2.LANDID=T7.LANDID
					INNER JOIN GET_REGXENTITY T3
					ON T2.LANDID=T3.LANDID
						INNER JOIN GET_DEED T4
						ON T3.DEEDID=T4.DEEDID
							INNER JOIN REGISTRATIONXPERSON T5
							ON T3.REGISTRATIONID=T5.IDREGISTRATION
								INNER JOIN PERSON T6
								ON T5.IDPERSON=T6.PERSONID
							

where  t2.intravilan='0' and t4.deednumber='7' and t4.deeddate='13.03.1996' and t6.notes not like '%REZERVA%'



--Report 13 Proprietari neidentificati intravilan

SELECT  distinct T2.Cadgenno,t2.Landid, T2.INTRAVILAN,T7.NUMBER as ParcelNumber, T7.CODEUSECATEGORY,
			T4.DEEDNUMBER, T4.DEEDDATE,T6.FIRSTNAME,T6.LASTNAME,T6.NOTES,round(T1.AREA,3) as Suprafata_mas,T6.PERSONID
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_PARCEL T7
				ON T2.LANDID=T7.LANDID
					INNER JOIN GET_REGXENTITY T3
					ON T2.LANDID=T3.LANDID
						INNER JOIN GET_DEED T4
						ON T3.DEEDID=T4.DEEDID
							INNER JOIN REGISTRATIONXPERSON T5
							ON T3.REGISTRATIONID=T5.IDREGISTRATION
								INNER JOIN PERSON T6
								ON T5.IDPERSON=T6.PERSONID
							

where  t4.deednumber='7' and t4.deeddate='13.03.1996' and t5.active='1' and t2.intravilan='1' 


SELECT  count(distinct t2.Landid)
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_PARCEL T7
				ON T2.LANDID=T7.LANDID
					INNER JOIN GET_REGXENTITY T3
					ON T2.LANDID=T3.LANDID
						INNER JOIN GET_DEED T4
						ON T3.DEEDID=T4.DEEDID
							INNER JOIN REGISTRATIONXPERSON T5
							ON T3.REGISTRATIONID=T5.IDREGISTRATION
								INNER JOIN PERSON T6
								ON T5.IDPERSON=T6.PERSONID
							

where  t4.deednumber='7' and t4.deeddate='13.03.1996' and t5.active='1' and t2.intravilan='1' 


--Report 14 imobile la care trbuie bifata o parcela de intravilan-- imobile la care trebuie debifata o percela de intravilan-- -------------DE RECTIFICAT SCRIPUTL PT CA NU 100% CORECT-----

select t1.landid,sum(cast(t1.intravilan as int)) as sum1,sum(cast(t2.intravilan as int)) as sum2
into #TabelParcels1
from   get_land t1
			inner join get_parcel t2
			on t1.landid=t2.landid
				inner join landgeometry t3
				on t1.landid=t3.landlogid
group by t1.landid 

select landid,iif(sum1>=1 and sum2<1,'Imobilul este in intravilan => trebuie bifata o parcela de intravilan','ok') as Comentariu
from #TabelParcels1
where sum1>=1 and sum2<1
UNION
select landid,iif(sum1=0 and sum2>=1,'Imobilul este in extravilan => trebuie debifata o parcela/mai multe de intravilan','ok') as Comentariu
from #TabelParcels1
where sum1=0 and sum2>=1
Drop table #TabelParcels1

---------------------------------------------------------------------------------------------------------------
select t1.landid,sum(cast(t1.intravilan as int)) as sum1,sum(cast(t2.intravilan as int)) as sum2
into #TabelParcels2
from   get_land t1
			inner join get_parcel t2
			on t1.landid=t2.landid
				inner join landgeometry t3
				on t1.landid=t3.landlogid
group by t1.landid 

select count(*)
from #TabelParcels2
where (sum1>=1 and sum2<1) or (sum1=0 and sum2>=1)

--Report 15 Persoane care au acelasi CNP dar nume diferite in cadrul aceluiasi ID
SELECT  DISTINCT CONCAT(T1.LANDID,'-',T1.IDCODE) AS KEY1,CONCAT (T1.FIRSTNAME,' ',T1.LASTNAME) AS PERSOANA,T1.PERSONID
INTO #tabelpersonsduplicates
FROM	GET_PERSONBYLAND T1
			INNER JOIN LANDGEOMETRY T2
			ON T1.LANDID=T2.LANDLOGID
WHERE	T1.IDCODE<>'9999999999999' and T1.active='1'
ORDER BY CONCAT(T1.LANDID,'-',T1.IDCODE) ASC

SELECT  CONCAT('Mai exista o alta persoana din cadrul Landlogid: ',SUBSTRING(T1.KEY1, 1, CHARINDEX('-', T1.KEY1)-1),' care are CNP: ',
		SUBSTRING(T1.KEY1,CHARINDEX('-', T1.KEY1) +1,1000)) as Mentiune,
		SUBSTRING(T1.KEY1, 1, CHARINDEX('-', T1.KEY1)-1) AS LandLogID,
		SUBSTRING(T1.KEY1,CHARINDEX('-', T1.KEY1) +1,1000) AS CNP,
		REPLACE(T1.PERSOANA,' ','_') AS PERSOANA,T1.PERSONID
		
FROM	#tabelpersonsduplicates AS T1
			CROSS APPLY   ( SELECT  T2.KEY1,COUNT(KEY1) AS COUNT_KEY1, COUNT(DISTINCT PERSOANA) AS COUNT_DISTINCT_PERSOANA
							FROM	#tabelpersonsduplicates AS T2
							WHERE	T1.KEY1=T2.KEY1
							GROUP BY KEY1) AS CA
								

WHERE CA.COUNT_KEY1>1 AND COUNT_DISTINCT_PERSOANA >1
DROP TABLE #tabelpersonsduplicates


SELECT  DISTINCT CONCAT(T1.LANDID,'-',T1.IDCODE) AS KEY1,CONCAT (T1.FIRSTNAME,' ',T1.LASTNAME) AS PERSOANA,T1.PERSONID
INTO #tabelpersonsduplicates1
FROM	GET_PERSONBYLAND T1
			INNER JOIN LANDGEOMETRY T2
			ON T1.LANDID=T2.LANDLOGID
WHERE	T1.IDCODE<>'9999999999999' and T1.active='1'
ORDER BY CONCAT(T1.LANDID,'-',T1.IDCODE) ASC

SELECT  count(T1.PERSONID)
		
FROM	#tabelpersonsduplicates1 AS T1
			CROSS APPLY   ( SELECT  T2.KEY1,COUNT(KEY1) AS COUNT_KEY1, COUNT(DISTINCT PERSOANA) AS COUNT_DISTINCT_PERSOANA
							FROM	#tabelpersonsduplicates1 AS T2
							WHERE	T1.KEY1=T2.KEY1
							GROUP BY KEY1) AS CA
								

WHERE CA.COUNT_KEY1>1 AND COUNT_DISTINCT_PERSOANA >1
DROP TABLE #tabelpersonsduplicates1

--Report 16
