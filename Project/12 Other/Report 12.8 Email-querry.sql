DECLARE @sub VARCHAR(100);
DECLARE @qry VARCHAR(1000);
DECLARE @msg VARCHAR(250);
DECLARE @query NVARCHAR(1000);
DECLARE @query_attachment_filename NVARCHAR(520);
SELECT @sub = 'TEST e-mail'; -- aici se va scrie titlul e-mailului

SELECT @msg = 'Please refer to the attached spread sheet for the report.'; -- un mesaj generic 
SELECT @query = '
SET NOCOUNT ON;



            Select top 10 * from master..sysobjects WITH(NOLOCK)';






SELECT @query_attachment_filename = 'test.csv'; --aici se va scrie denumirea fisierului in care se va exporta (doar csv)
EXEC msdb.dbo.sp_send_dbmail
     @profile_name = 'Notifications',
     @recipients = 'valerio@ramboll.com', --e-mail cui trebuie transmis
     @copy_recipients = '', -- in CC
     @body = @msg,
     @subject = @sub,
     @query = @query,
     @query_attachment_filename = @query_attachment_filename,
     @attach_query_result_as_file = 1,
     @query_result_header = 1,
     @query_result_width = 256,
     @query_result_separator = '   ',
     @query_result_no_padding = 1;