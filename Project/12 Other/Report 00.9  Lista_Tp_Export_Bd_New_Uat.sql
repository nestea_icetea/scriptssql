SELECT  distinct *
FROM	(
SELECT  DISTINCT t1.cadgenno as ID,
				t1.sectorcadastral as SECTOR,
				--T1_1.LINIE,
				T1_1.GEOMETRY_XLO, T1_1.GEOMETRY_YLO,
				T1.LANDID as LANDID,
				iif(T1_1.geometry is not null,'corelat','necorelat') as Corelat,
				T1.NOTES AS 'Mentiuni teren', T1.LOCALITATE,T1.E2IDENTIFIER as 'IE',
				T1.PAPERCADNO AS NRCADVECHI, 
				IIF(T1.INTRAVILAN='1','DA','NU') AS INTRAVILAN,
				IIF(T1.ENCLOSED='1','Imprejmuit','Neimprejmuit') as Imprejmuit, 
				T1.STREETNAME as 'Denumire strada',
				T1.POSTALNUMBER as 'Numar postal',
				round(T1_1.AREA,0) AS 'Suprafata teren masurata cadastru general',
				T1.PARCELLEGALAREA AS 'Suprafata act CGR',
				T5.PARCELLEGALAREA AS 'Suprafata act Eterra',
				round(T5.MEASUREDAREA,0) AS 'Suprafata din act CF -conversie',
				round(T9.AREA,0) AS 'Suprafata geometrie IE',
				
				ltrim(case 
					when	T2_2.LISTA_TARLALE like ', %' then substring(T2_2.LISTA_TARLALE,3,20)
					else	T2_2.LISTA_TARLALE
				end) as LISTA_TARLALE,
				T2_1.LISTA_PARCELE,
				T2_3.LISTA_TP as 'lista_tp', 
				T4.LISTA_ACTE as 'Lista acte de proprietate',
				T2_4.LISTA_CATEGORII_FOL AS 'Lista categorii de folosinta',
				T6.LISTA_PROPRIETARI AS Proprietari
				--t8.defunct
				

				
FROM	GET_LAND T1
		--LEFT OUTER JOIN LANDGEOMETRY T1_1
		FULL OUTER JOIN LANDGEOMETRY T1_1
		ON T1.LANDID=T1_1.LANDLOGID
			
			INNER JOIN 
						--AICI SE LEAGA CU PARCELELE LISTA
						(
										SELECT  GET_LAND.LANDID,rtrim(ltrim(STUFF((

													SELECT  DISTINCT ', ' + PARCELNO
													FROM	GET_PARCEL T1
													WHERE	T1.LANDID=GET_LAND.LANDID
													FOR     XML PATH('')),1,1,''))) AS LISTA_PARCELE
									
										FROM								GET_LAND
										
										GROUP BY GET_LAND.LANDID
						) AS T2_1
						ON T1.LANDID=T2_1.LANDID

			INNER JOIN
						--AICI SE LEAGA CU TARLALELE
						(

									SELECT  GET_LAND.LANDID,rtrim(ltrim(STUFF((

										SELECT  DISTINCT ', ' + LANDPLOTNO
										FROM	GET_PARCEL T1
										WHERE	T1.LANDID=GET_LAND.LANDID
										FOR     XML PATH('')),1,1,''))) AS LISTA_TARLALE
									
									FROM								GET_LAND
									
									GROUP BY GET_LAND.LANDID

						) AS T2_2
						ON T1.LANDID=T2_2.LANDID
					

			INNER JOIN
						--AICI SE LEAGA CU TP
						(
									SELECT  GET_LAND.LANDID,rtrim(ltrim(STUFF((

									SELECT  DISTINCT ', ' + TITLENO
									FROM	GET_PARCEL T1
									WHERE	T1.LANDID=GET_LAND.LANDID
									FOR     XML PATH('')),1,1,''))) AS LISTA_TP
									
								FROM								GET_LAND
								
								GROUP BY GET_LAND.LANDID

						) AS T2_3
						ON T1.LANDID=T2_3.LANDID

			INNER JOIN
						--AICI SE LEAGA CU CATEGORIILE DE FOLOSINTA CU VIRGULA
						(
									SELECT  GET_LAND.LANDID,rtrim(ltrim(STUFF((

									SELECT  DISTINCT ', ' + CODEUSECATEGORY
									FROM	GET_PARCEL T1
									WHERE	T1.LANDID=GET_LAND.LANDID
									FOR     XML PATH('')),1,1,''))) AS LISTA_CATEGORII_FOL
									
								FROM								GET_LAND
								
								GROUP BY GET_LAND.LANDID

						) AS T2_4
						ON T1.LANDID=T2_4.LANDID
			INNER JOIN 
						(
						--AICI SE LEGA TOATE ACTELE CU VIRGULA
						SELECT  GET_LAND.LANDID,rtrim(ltrim(STUFF((

															SELECT  DISTINCT ', ' + DEEDNUMBER
															FROM	GET_DEEDBYLANDID T1
															WHERE	T1.LANDID=GET_LAND.LANDID
															FOR     XML PATH('')),1,1,''))) AS LISTA_ACTE
									
						FROM								GET_LAND
						
						GROUP BY GET_LAND.LANDID

						) AS T4
						ON T1.LANDID=T4.LANDID

			INNER JOIN 
						(
						--AICI SE LEAGA PROPRIETARII CU VIRGULA
						SELECT  GET_LAND.LANDID,rtrim(ltrim(STUFF((

									SELECT  DISTINCT ', ' + FIRSTNAME + ' ' +  LASTNAME
									FROM	GET_PERSONBYLAND T1
									WHERE	T1.LANDID=GET_LAND.LANDID
									FOR     XML PATH('')),1,1,''))) AS LISTA_PROPRIETARI
									
						FROM								GET_LAND
						
						GROUP BY GET_LAND.LANDID
						) AS T6
						ON T6.LANDID=T1.LANDID

			--INNER JOIN 
			--			(
			--			--AICI SE LEAGA Defunctii cu virgula CU VIRGULA
			--			SELECT  GET_LAND.LANDID,STUFF((

			--							SELECT  DISTINCT ', ' + lastname + ' ' +  firstname
			--							FROM	GET_Defunct T1
			--							WHERE	T1.LANDID=GET_LAND.LANDID
			--							FOR     XML PATH('')),1,1,'') AS defunct
									
			--			FROM								GET_LAND

			--			GROUP BY GET_LAND.LANDID
			--			) AS T8
			--			ON T8.LANDID=T1.LANDID



			FULL OUTER JOIN LANDGEOMETRY_LOG T9 ON T1.E2IDENTIFIER = T9.IDENTIFIER


								--INNER JOIN GET_REGXENTITY T3
								--ON T1.LANDID=T3.LANDID
										
												FULL OUTER JOIN GET_LAND_LOG T5
												ON T1.E2IDENTIFIER=T5.E2IDENTIFIER
													FULL OUTER JOIN LANDGEOMETRY_LOG T7
													ON T5.landid=T7.LANDLOGID
													
--WHERE   
		
		
		--AND T1_1.nrprovizoriu='ref'
		--AND T2_1.LISTA_PARCELE LIKE '%112%'
		--AND T2_2.LISTA_tarlale like '%14%'
		--T1_1.observatii = '12345'
		--T1.landid =25963
			) AS T1
where t1.landid is not null 
	   --and t1.IE>0 and t1.NRCADVECHI<>''
	  --and t1.lista_tarlale like '%16%'


--ORDER BY GEOMETRY_XLO asc
--ORDER BY GEOMETRY_xLO asc


