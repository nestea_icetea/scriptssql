
---------------------------------LIPSA CADGENNO DIN IMOBILELE CE URMEAZA A FI PREDATE-----------------------------------
----MYTABLE primul numar
--SELECT  TOP 1 T2.CADGENNO
--FROM	LANDGEOMETRY T1
--			INNER JOIN GET_LAND T2
--			ON T1.LANDLOGID=T2.LANDID
--WHERE	T2.CADGENNO>0
--ORDER BY CONVERT(INT, PARSENAME(t2.cadgenno, 1)) ASC

-------------------------------------------------------
----MYTABLE ultimul numar
--SELECT  TOP 1 T2.CADGENNO
--FROM	LANDGEOMETRY T1
--			INNER JOIN GET_LAND T2
--			ON T1.LANDLOGID=T2.LANDID
--WHERE	T2.CADGENNO>0
--ORDER BY CONVERT(INT, PARSENAME(t2.cadgenno, 1)) DESC
 
----------------------------------------------PAS 1--------------------------------------------------------------
 --tabel cu ce compara
 create table #NumberGapsInSQL 
 (
  number varchar(20)
)
declare @LastNumberCorelated nvarchar(20)
declare @FirstNumberCorelated nvarchar(20)
declare @i int

SELECT @LastNumberCorelated = (SELECT  TOP 1 T2.CADGENNO
						FROM	LANDGEOMETRY T1
									INNER JOIN GET_LAND T2
									ON T1.LANDLOGID=T2.LANDID
						WHERE	T2.CADGENNO>0
						ORDER BY CONVERT(INT, PARSENAME(t2.cadgenno, 1)) DESC)
SELECT @FirstNumberCorelated = (SELECT  TOP 1 T2.CADGENNO
						FROM	LANDGEOMETRY T1
									INNER JOIN GET_LAND T2
									ON T1.LANDLOGID=T2.LANDID
						WHERE	T2.CADGENNO>0
						ORDER BY CONVERT(INT, PARSENAME(t2.cadgenno, 1)) ASC)


set @i = @FirstNumberCorelated
while @i <= @LastNumberCorelated
begin
 insert into #NumberGapsInSQL
   values (@i)
 set @i = @i + 1
end 
------------------------------------------------PAS 2------------------------------------------------------------
SELECT  NUMBER AS Cadgenno_LIPSA,'Imobil neasociat in lucrare' AS MENTIUNE
FROM	#NumberGapsInSQL
WHERE   NUMBER NOT IN ( SELECT  T2.CADGENNO
						FROM	LANDGEOMETRY T1
									INNER JOIN GET_LAND T2
									ON T1.LANDLOGID=T2.LANDID
						WHERE	T2.CADGENNO>0
						)

ORDER BY CONVERT(INT, PARSENAME(NUMBER, 1)) ASC
------------------------------------------------FINAL------------------------------------------------------------	
DROP TABLE 	#NumberGapsInSQL

