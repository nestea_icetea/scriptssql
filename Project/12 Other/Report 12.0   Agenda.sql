--Task-uri
-----------Reporting------------------------------------
--1. Eterre integrate/neintegrate

--2. Cati opratori lucreaza pe UAT

--3. Cate imobile avem incarcate in baza de date fata de cele estimate

--4.Cate imobile sunt lucrate din cele care sunt incarcate in BD

--5.Cate imobile sunt venite din Eterra si cate dintre acestea sunt integrate, sortate dupa intravilan si extravilan

--6.Cate constructii sunt incarcate in BD fata de cele estimate

--7.Pentru fiecare operator: (Raport Saptamanal)
							-- la ce ora a dat drumul la aplicatie si cand a inchis-o
							-- sa se calculeze timpii morti in care nu se intampla nimic@
							-- cate imobile a introdus sortate dupa ora (pivot)
							-- dintre cele de mai sus sa se numere cate sunt din eterra, cate sunt bagata manual
							-- dintre cele bagate manual sa se numere de cate tipuri sunt imobilele introduse ( TP, CVC, s.a.m.d)
							-- 

---------Verificari------------

--8. Cate erori de vertex avem (a se numara din tabela din geomedia)

--9. Cate imobile sunt cu proprietar neidentificat

--10. cate imobile au dublate LandLogID

--11. Cate imobile din intravilan nu au completata -- strada , nr.postal

--12. Cate imobile integrate nu au completata suprafata din acte si cele din Eterra

--13. Verificare suprafata acte fata de cea masurata atat pentru intravilan cat si pentru extravilan

--14. Imobile care nu au tarla/parcela completata--

--15. Imobile care se afla in alta tarla fata de cea delimitatata


-------------Updates----------------------

--[ ] Update din geomedia in CGR nr. sector
--[ ] Renumerotare identificator cadastru general
--[ ] Scriptul cu suma suprafetelor parcelelor diferita de suprafata terenului - diferenta lipsa la una din parcele
--[ ] Toate persoanale care nu au completata adresa - Comuna Luciu/CNP 9999999999999
--[ ] Completare codul postal al constructiei - Constructii intravilan= (1=127315;2=127316 si pentru cele din extravilan 999999)
--[ ] Constructii fara numar de nivele completat cu 1
--[ ] De sters mentiunile de la parcela pentru imobilele ce vin din Eterra;
--[ ] De sters valoarea "Numar de unitati individuale" de la constructii

