--raport cu imobile ce prezinta CF si au inscrieri intre anumite date

SELECT DISTINCT *
FROM	(
				SELECT  DISTINCT T2.E2IDENTIFIER, CONVERT(DATE,LEFT(T4.APPDATE,10),104) AS DATA_INSCRIERE
				FROM	GET_LAND_LOG T2
								INNER JOIN REGISTRATIONXENTITY_LOG T3
								ON T2.LANDID = T3.LANDID
									INNER JOIN REGISTRATION_LOG T4
									ON T3.REGISTRATIONID = T4.REGISTRATIONID

				UNION	

				SELECT  DISTINCT T4.E2IDENTIFIER,CONVERT(DATE,LEFT(T3.APPDATE,10),104) AS DATA_INSCRIERE
				FROM	GET_BUILDING_LOG T1
							INNER JOIN REGISTRATIONXENTITY_LOG T2
							ON T1.BUILDINGID = T2.BUILDINGID
								INNER JOIN REGISTRATION_LOG T3
								ON T2.REGISTRATIONID = T3.REGISTRATIONID
									INNER JOIN GET_LAND_LOG T4
									ON T1.LANDID = T4.LANDID

			) AS T1

WHERE	T1.DATA_INSCRIERE BETWEEN CAST('2019-08-02' AS DATE) AND CAST('2019-09-15' AS DATE)
ORDER	BY T1.DATA_INSCRIERE ASC
