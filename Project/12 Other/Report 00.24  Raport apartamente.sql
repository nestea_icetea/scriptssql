SELECT  T8.CADGENNO,T1.APNO AS 'Numar apartament', t1.floor as Etaj, t1.entry as Scara, t1.totalarea as 'Suprafata totala',t1.notes as Mentiuni,t4.nameregistrationtype as Intabulare,t5.deednumber as 'Numar act', t5.deeddate as 'Data act', 
		t7.firstname as Nume, t7.lastname as Prenume,t7. idcode as CNP
FROM	GET_IU T1
			INNER JOIN BUILDINGGEOMETRY T2
			ON T1.BUILDINGID=T2.BUILDINGLOGID
				INNER JOIN REGISTRATIONXENTITY T3
				ON T1.IUID=T3.IUID
					INNER JOIN GET_REGISTRATION T4
					ON T3.REGISTRATIONID=T4.REGISTRATIONID
						INNER JOIN GET_DEED T5
						ON T4.DEEDID=T5.DEEDID
							INNER JOIN REGISTRATIONXPERSON  T6
							ON T3.REGISTRATIONID=T6.IDREGISTRATION
								INNER JOIN GET_PERSON T7
								ON T6.IDPERSON=T7.PERSONID
									INNER JOIN GET_LAND T8
									ON T2.LANDLOGID=T8.LANDID