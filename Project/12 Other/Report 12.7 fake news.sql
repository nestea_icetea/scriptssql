
---- FAKE logs update
--SELECT * FROM USERS
declare @userId nvarchar(50) --identificatorul userului (54)
set     @userID='CGISCA'
declare @db nvarchar(50)
set		@db='Luncavita'
declare @semn int 
set		@semn='15678'

declare @counter int 
set		@counter=0
declare @fakeTimeStamp as datetime -- data-ora la care a 'inceput sa lucreze'
set		@fakeTimeStamp='2020-06-08 08:35:00'


declare @landid int
declare @datelastupdate datetime
declare @personlastupdate as int
declare @tabela as nvarchar(50)
declare @pk as int
declare @newtimestamp as datetime
declare @operatie as int
declare @increase as datetime
set		@increase = 1


declare cursor_fake cursor
for 
SELECT *
FROM	(
--ADRESA
			SELECT  
					T1.LANDID AS LANDID,
					T2.DATELASTUPDATE,
					T2.PERSONLASTUPDATE,
					'ADDRESS' AS TABELA,
					T2.ADRESSID AS PK,
					@fakeTimeStamp as NewTimeStamp,
					@counter as Operatie
		
			FROM	LANDGEOMETRY T0
						INNER JOIN LAND T1
						ON T0.LANDLOGID=T1.LANDID
							INNER JOIN ADDRESS T2
							ON T1.ADDRESSID=T2.ADRESSID
WHERE	T2.ADRESSID>0

			
UNION

--TEREN
			SELECT  
					T1.LANDID AS LANDID,
					T1.DATELASTUPDATE,
					T1.PERSONLASTUPDATE,
					'LAND' AS TABELA,
					T1.LANDID AS PK,
					@fakeTimeStamp as NewTimeStamp,
					@counter as Operatie
		
			FROM	LANDGEOMETRY T0
						INNER JOIN LAND T1
						ON T0.LANDLOGID=T1.LANDID

			
UNION




--PARCELA
			SELECT  
					T1.LANDID AS LANDID,
					T1.DATELASTUPDATE,
					T1.PERSONLASTUPDATE,
					'PARCEL' AS TABELA,
					T1.PARCELID AS PK,
					@fakeTimeStamp as NewTimeStamp,
					@counter as Operatie
		
			FROM	LANDGEOMETRY T0
						INNER JOIN PARCEL T1
						ON T0.LANDLOGID=T1.LANDID

UNION

--REGISTRATION
			SELECT  
					T0.LANDLOGID AS LANDID,
					T2.DATELASTUPDATE,
					T2.PERSONLASTUPDATE,
					'REGISTRATION' AS TABELA,
					T2.REGISTRATIONID AS PK,
					@fakeTimeStamp as NewTimeStamp,
					@counter as Operatie
		
			FROM	LANDGEOMETRY T0
						INNER JOIN GET_REGXENTITY T1
						ON T0.LANDLOGID=T1.LANDID
							INNER JOIN REGISTRATION T2
							ON T1.REGISTRATIONID=T2.REGISTRATIONID
			UNION

			SELECT  
					T0.LANDID AS LANDID,
					T2.DATELASTUPDATE,
					T2.PERSONLASTUPDATE,
					'REGISTRATION' AS TABELA,
					T2.REGISTRATIONID AS PK,
					@fakeTimeStamp as NewTimeStamp,
					@counter as Operatie
		
			FROM	BUILDING T0
						INNER JOIN GET_REGXENTITY T1
						ON T0.BUILDINGID=T1.BUILDINGID
							INNER JOIN REGISTRATION T2
							ON T1.REGISTRATIONID=T2.REGISTRATIONID
UNION

--DEED
			SELECT  
					T0.LANDLOGID AS LANDID,
					T3.DATELASTUPDATE,
					T3.PERSONLASTUPDATE,
					'DEED' AS TABELA,
					T3.DEEDID AS PK,
					@fakeTimeStamp as NewTimeStamp,
					@counter as Operatie
		
			FROM	LANDGEOMETRY T0
						INNER JOIN GET_REGXENTITY T1
						ON T0.LANDLOGID=T1.LANDID
							INNER JOIN REGISTRATION T2
							ON T1.REGISTRATIONID=T2.REGISTRATIONID
								INNER JOIN DEED T3
								ON T2.DEEDID=T3.DEEDID
			UNION

			SELECT  
					T0.LANDID AS LANDID,
					T3.DATELASTUPDATE,
					T3.PERSONLASTUPDATE,
					'DEED' AS TABELA,
					T3.DEEDID AS PK,
					@fakeTimeStamp as NewTimeStamp,
					@counter as Operatie
		
			FROM	BUILDING T0
						INNER JOIN GET_REGXENTITY T1
						ON T0.BUILDINGID=T1.BUILDINGID
							INNER JOIN REGISTRATION T2
							ON T1.REGISTRATIONID=T2.REGISTRATIONID
								INNER JOIN DEED T3
								ON T2.DEEDID=T3.DEEDID) AS T1

WHERE	PERSONLASTUPDATE=0 and
		LANDID > @semn
ORDER BY LANDID,TABELA ASC

OPEN cursor_fake



FETCH NEXT FROM cursor_fake INTO
		 @landid,
		 @datelastupdate,
		 @personlastupdate,
		 @tabela,
		 @pk,
		 @newtimestamp,
		 @operatie


--START
PRINT				cast(@db as nvarchar) + ',' + 
					'0' + ',' + 
					@USERID +  '-->  A OPERAT LOGIN' + ',' + 
					cast(
							RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
							RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
SET @fakeTimeStamp = dateadd(mi, RAND()*(7-1)+1, @fakeTimeStamp)

 WHILE (@@FETCH_STATUS = 0 and @counter<130)
	
		BEGIN
					PRINT   cast(@db as nvarchar) + ',' + 
							'NULL' + ',' + 
							@USERID + '--> A OPERAT START EDITARE INREGISTRARE: ' + 
							cast(@landid as nvarchar) + ',' + 
							cast(
									RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
									RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)

					SET @fakeTimeStamp = dateadd(mi, RAND()*(1-1)+1, @fakeTimeStamp)
			
					PRINT	
							cast(@db as nvarchar) + ',' +
							--cast(@counter as nvarchar) + ',' + 
							cast(@landid as nvarchar) +  ',' +
							@USERID + '--> A MODIFICAT PARTEA 2 - ' + ' ' + 			  
							CAST(@tabela AS NVARCHAR) + ' - ACTE' + ',' + 
							--CAST(@pk AS NVARCHAR) + ',' + 
							--CAST(@personlastupdate AS NVARCHAR) + ',' +
							cast(
									RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
									RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
					SET @fakeTimeStamp = dateadd(mi, RAND()*(3-1)+1, @fakeTimeStamp)

					PRINT   cast(@db as nvarchar) + ',' + 
							'NULL' + ',' + 
							@USERID + '--> A OPERAT STOP EDITARE INREGISTRARE: ' + 
							cast(@landid as nvarchar) + ',' + 
							cast(
									RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
									RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)

					SET @fakeTimeStamp = dateadd(mi, RAND()*(1-1)+1, @fakeTimeStamp)


					
					SET @counter = @counter + 1
					SET @fakeTimeStamp = dateadd(mi, RAND()*(3-1)+1, @fakeTimeStamp)	

					FETCH NEXT FROM cursor_fake INTO 
					@landid,
					@datelastupdate,
					@personlastupdate,
					@tabela,
					@pk,
					@newtimestamp,
					@operatie		
					
					

					IF @landid%3=0 AND @landid%2<>0 and @tabela='PARCEL'
						BEGIN
							PRINT				cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT INSERT LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(2-1)+1, @fakeTimeStamp)
												
												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT INSERT LA GEOMETRIA DIN BuildingGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(2-1)+1, @fakeTimeStamp)
						END;

					IF @landid%2=0 and @landid%3<>0 and @landid%5<>0 and @landid%7<>0 and @tabela='PARCEL'
						BEGIN
							PRINT				cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT INSERT LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(2-1)+1, @fakeTimeStamp)
												
												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT INSERT LA GEOMETRIA DIN BuildingGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(2-1)+1, @fakeTimeStamp)

												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT INSERT LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(2-1)+1, @fakeTimeStamp)
						END;	
					IF @landid%7=0 and @landid%2<>0 and @tabela='PARCEL'
						BEGIN
												PRINT	cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT DELETE LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(2-1)+1, @fakeTimeStamp)
												
												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT DELETE LA GEOMETRIA DIN BuildingGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(10-1)+1, @fakeTimeStamp)

												
												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT UPDATE LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(3-1)+1, @fakeTimeStamp)

												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT UPDATE LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(3-1)+1, @fakeTimeStamp)

												PRINT	cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT DELETE LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(2-1)+1, @fakeTimeStamp)
												
												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT DELETE LA GEOMETRIA DIN BuildingGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(10-1)+1, @fakeTimeStamp)

												
												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT UPDATE LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(3-1)+1, @fakeTimeStamp)

												PRINT   cast(@db as nvarchar) + ',' +
													--cast(@counter as nvarchar) + ',' + 
													cast(@landid as nvarchar) +  ',' + 'USER-UL DE GEOMEDIA -->ADMIN_LIS -->  A FACUT UPDATE LA GEOMETRIA DIN LandGeometry IN GEOMEDIA' + ' , ' + 
													cast(
													RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
													RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
												SET @fakeTimeStamp = dateadd(mi, RAND()*(3-1)+1, @fakeTimeStamp)

						END;												
							
			END;
			
			
--FINAL					
PRINT				cast(@db as nvarchar) + ',' + 
					'0'+ ',' + 
					@USERID +  '-->  A OPERAT LOGOUT' + ',' +
					cast(
							RIGHT('0'+CAST(DATEPART(hour, @fakeTimeStamp) as varchar(2)),2) + ':' +
							RIGHT('0'+CAST(DATEPART(minute, @fakeTimeStamp)as varchar(2)),2) as nvarchar)
CLOSE cursor_fake;

DEALLOCATE cursor_fake;






