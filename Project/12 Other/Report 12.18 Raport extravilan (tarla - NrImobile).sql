----- lista tarlele extravilan si numarul de imobile aferente (doar tp-uri)
------------------------------------------------------------------------------------------
use [Master]
DECLARE @command1 varchar(1000) 
SELECT @command1 = 
'IF ''?'' IN(
''LETEA_VECHE'', 
''MARGINENI'',
''NICOLAE_BALCESCU'') 

BEGIN USE ? 

SELECT  DISTINCT DB_NAME() AS UAT, t3.Landplotno AS Tarla, count(distinct t1.landid) as [Numar Imobile]
FROM            Land_log t1 INNER JOIN FileLogDescription t2
				ON t1.Fileid = t2.Fileid 
							INNER JOIN Parcel_log t3
							ON t1.LandID = t3.LandID 
								INNER JOIN Address_LOG T4
								ON T1.Addressid = T4.AdressId

where	t4.intravilan=0 AND T3.Intravilan=0 AND T2.SourceType=''DDAPT''
group by t3.landplotno
order by t3.landplotno asc

END'
EXEC sp_MSforeachdb @command1