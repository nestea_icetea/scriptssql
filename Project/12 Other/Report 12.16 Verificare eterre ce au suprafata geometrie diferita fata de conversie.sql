--- Verificare Bacau eterre care au suprafata geometrie diferita fata de coversie pad

SELECT  T1.E2IDENTIFIER as IE, 
		t1.intravilan,
		T1.Topono as NrTopo, 
		T1.PaperCadno as NrCAD, 
		t1.Paperlbno as NrCF, 
		t1.PARCELLEGALAREA as S_act,
		t1.MEASUREDAREA as S_mas_conversie, 
		
		t2.AREA as S_mas_geometrie, 

		CASE
				WHEN T2.AREA IS NOT NULL AND T1.PARCELLEGALAREA > 1 
							THEN  
									CASE
										WHEN T2.AREA-T1.MEASUREDAREA>1 OR T1.MEASUREDAREA-T2.AREA>1 THEN 'NO -             lamuriri OCPI'
										WHEN T2.AREA-T1.MEASUREDAREA=1 OR T1.MEASUREDAREA-T2.AREA=1 THEN 'OK -             e in toleranta de plus minus 1mp'
										ELSE 'OK -             suprafete identice'
									END
				
				ELSE 
									CASE
										WHEN T1.MEASUREDAREA = 0 AND T2.AREA>0 THEN 'OK -             se preia suprafata din geometrie' 
										WHEN T1.MEASUREDAREA = 0 AND (T2.AREA= 0 OR T2.AREA IS NULL) THEN 'OK -             se preia suprafata din ACT' 
										ELSE 'OK -             se preia suprafata din conversie'
									END
					

		END AS Verificare

FROM	GET_LAND_LOG T1
			FULL JOIN LANDGEOMETRY_LOG T2
			ON T1.E2IDENTIFIER = T2.IDENTIFIER


WHERE	T1.E2IDENTIFIER > 0


