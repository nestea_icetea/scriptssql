--Landgeometry=T1
--Get_Land=T2
-------------------Cate etere mai sunt de integrat pe extravilan?----------------
Declare @Integrate	int;
Declare	@Toate		int;

				-----eterre integrate---
				Select			@Integrate=Count(*)
				From			[dbo].[LandGeometry]	T1
				INNER JOIN		[dbo].[Get_Land]		T2
				ON				T1.[LandLogID]=T2.[LandID]
				Where			T2.[E2Identifier]<>'' AND [Intravilan]='0'
				---------------------------
				-------toate eterele-----
				Select			@Toate=count(*)
				From			[dbo].[Get_Land]
				Where			E2Identifier<>'' AND [Intravilan]='0'

Print	@Toate-@Integrate

----------------------Cate etere sunt integrate?--------------------
Declare			@Integrate	int;
Select			@Integrate=Count(*)
				From			[dbo].[LandGeometry]	T1
				INNER JOIN		[dbo].[Get_Land]		T2
				ON				T1.[LandLogID]=T2.[LandID]
				Where			T2.[E2Identifier]>0

Print			@Integrate

----------------------Cate etere avem in BD?----------------
Declare			@Toate		int;
Select			@Toate=count(*)
				From			[dbo].[Get_Land]
				Where			E2Identifier>0

Print			@Toate

---------------Cate eterre sunt integrate pe intravilan?---------------------
Declare			@Eintegrate1	INT
Select			@Eintegrate1=count(*)
				From			[dbo].[LandGeometry]	T1
				INNER JOIN		[dbo].[Get_Land]		T2
				ON				T1.[LandLogID]=T2.[LandID]
				Where			T2.[E2Identifier]>0 AND Intravilan=1

Print			@Eintegrate1

---------------Cate eterre sunt integrate pe extravilan?----------------------------
Declare			@EintegrateE	INT
Select			@EintegrateE=count(*)
				From			[dbo].[LandGeometry]	T1
				INNER JOIN		[dbo].[Get_Land]		T2
				ON				T1.[LandLogID]=T2.[LandID]
				Where			T2.[E2Identifier]>0 AND Intravilan=0

Print			@EintegrateE

----------------------------------------------------------------------










----------------------------------------------------Lista eterre CARE NU AU INSCRIERI IN CGR------------------------------------------------------------------------------------


SELECT		T1.CADGENNO,T1.LANDID,T1.E2IDENTIFIER AS NR_ETERRA,T1.INTRAVILAN,T1.PaperCadno AS NR_CADASTRAL_VECHI,t1.paperlbno as NR_CF, T1.PARCELLEGALAREA AS SUPRAFATA_ACTE, T3.PERSOANA
			--T3.NUMBER AS NUMAR_PARCELA,T3.LANDPLOTNO AS TARLA,T3.PARCELNO AS NUMAR_PARCELA_ACTE,T3.CODEINTRAVILAN AS ADRESA_PARCELA,T3.CodeUseCategory AS CATEGORIE_FOLOSINTA_PARCELA,T3.NOTES AS COMENTARII_PARCELA
FROM		GET_LAND T1
			LEFT OUTER JOIN	LANDGEOMETRY T2
			ON	T1.LANDID=T2.LANDLOGID
				FULL OUTER JOIN GET_UNIQUEPERSONBYLAND T3
				ON T1.LANDID=T3.LANDID
				
					 
WHERE		T2.LANDLOGID IS NULL AND T1.E2IDENTIFIER<>'' and t1.e2identifier not like '%TMP%' AND T3.PERSOANA IS NULL
ORDER BY	t1.papercadno asc,T1.INTRAVILAN ASC,T1.LANDID ASC
			--T3.NUMBER ASC

	




--update  land
--set		active='0'

--where	e2identifier in (
--							SELECT		T1.E2IDENTIFIER
--										--T3.NUMBER AS NUMAR_PARCELA,T3.LANDPLOTNO AS TARLA,T3.PARCELNO AS NUMAR_PARCELA_ACTE,T3.CODEINTRAVILAN AS ADRESA_PARCELA,T3.CodeUseCategory AS CATEGORIE_FOLOSINTA_PARCELA,T3.NOTES AS COMENTARII_PARCELA
--							FROM		GET_LAND T1
--										LEFT OUTER JOIN	LANDGEOMETRY T2
--										ON	T1.LANDID=T2.LANDLOGID
--											FULL OUTER JOIN GET_UNIQUEPERSONBYLAND T3
--											ON T1.LANDID=T3.LANDID
				
					 
--							WHERE		T2.LANDLOGID IS NULL AND T1.E2IDENTIFIER<>'' and t1.e2identifier not like '%TMP%' AND T3.PERSOANA IS NULL
						
--										--T3.NUMBER ASC

--							)

----------------------------------------------------Cate eterre nu sunt integrate ?------------------------------------------------------------------------------------------------------------

SELECT		count(*)
FROM		GET_LAND T1
			LEFT OUTER JOIN	LANDGEOMETRY T2
			ON	T1.LANDID=T2.LANDLOGID

					
WHERE		T2.LANDLOGID IS NULL AND T1.E2IDENTIFIER<>'' and T1.E2IDENTIFIER NOT LIKE '%TMP%'

----------------------------------------------------eTERRE DESFIINTATE?---------------




----------------------------Lista eterre corelate la mai multe imobile (la care se mentioneaza unde se afla restul de suprafata)---------------

SELECT  T1.E2IDENTIFIER
FROM	GET_LAND T1
			full outer JOIN LANDGEOMETRY T2
			ON T1.LANDID=T2.LANDLOGID
WHERE	T1.E2IDENTIFIER<>'' AND T1.E2IDENTIFIER IS NOT NULL
GROUP BY T1.E2IDENTIFIER
HAVING COUNT(*)>1
-----------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------eTERRE sistate ---------------------------------------------------------

SELECT  T1.LANDID, T1.E2IDENTIFIER, t1.active
FROM	GET_LAND T1
			left OUTER JOIN GET_LAND_LOG T2
			ON T1.E2IDENTIFIER=T2.E2IDENTIFIER
WHERE	T2.E2IDENTIFIER IS NULL AND T1.E2IDENTIFIER<>''

ORDER BY T1.e2identifier ASC



--------------------------------------------------------------------------------------------------------------
--######################### Lista eterre alocate la una sau mai multe imobile ##############################################

update  land
set		active='0'
where	e2identifier in (	'24250','24544','23918','23222','22154','24253','22859','24813','23692','21997','TMP21371','21298','24871','22954','22717','22486','24630','24870','22307','21517','24345','23156','24201','24003','24135','23504',
							'24460','24547','24182','24190','24053','21435','20079','23944','22441','24365','24427','24805','23691','22174','24538','22236','23214','24077','24414','24043','23696','24450','20256','20365','24614','20428','24890','23563','23715','22648',
							'21058','22650','22323','24186','20093','24181','24658','23651','24411','24187','24039','24497','24568','21945','24279','24005','23693','23287','23569','24286','24415','21310','24623','22857','24670','23453','24357','24047','23354','23503',
							'24278','24518','23925','24144','24629','24249','23161','21005','22045','21946','22464','24613','24429','21361','22765','24659','20091'


						)



						
						SELECT  T1.E2IDENTIFIER
						FROM	GET_LAND T1
									left OUTER JOIN GET_LAND_LOG T2
									ON T1.E2IDENTIFIER=T2.E2IDENTIFIER
						WHERE	T2.E2IDENTIFIER IS NULL AND T1.E2IDENTIFIER<>''
						)
select  distinct E2identifier
from	get_land
where	e2identifier is not null or e2identifier <>''

---------------------------------------------------------------------------------------------------------------

--lista eterre OG-35

SELECT  E2IDENTIFIER,PAPERCADNO AS NR_CAD, PAPERLBNO AS NR_CF, INTRAVILAN, localitate, UAT
FROM	GET_LAND
WHERE	(E2IDENTIFIER<>'' OR E2IDENTIFIER IS NOT NULL OR E2IDENTIFIER<>' ') AND INTRAVILAN='0'

--------------------------------------------------------------------------------------------------------------

select  * 
from	land_log












-----------------------------------------------------------------------------------------------------------------------------------------------
SELECT  T2.CADGENNO, T2.E2IDENTIFIER,T2.LANDID,T1.AREA AS SUP_GEOMEDIA,T1.E2identifier_Sact, t1.e2identifier_Smas,
		CASE 
			WHEN	T1.E2IDENTIFIER_SACT=0 THEN T1.E2IDENTIFIER_SMAS
			WHEN	T1.E2IDENTIFIER_SMAS=0 THEN T1.E2IDENTIFIER_SACT
		
			 WHEN T1.E2IDENTIFIER_SACT>T1.e2IDENTIFIER_SMAS THEN T1.E2IDENTIFIER_SMAS
			 WHEN T1.E2IDENTIFIER_SMAS>T1.E2IDENTIFIER_SACT THEN T1.E2IDENTIFIER_SACT

			 WHEN T1.E2IDENTIFIER_SACT=T1.E2IDENTIFIER_SMAS  THEN T1.E2IDENTIFIER_SACT
		END  AS TEST
INTO #TABEL1


		--IIF(t1.area<>TEST,'Gresit','ok') as Observatie
		----, T4.NOTA_CAD, T4.NOTA_CF
FROM	LANDGEOMETRY T1
			INNER JOIN GET_LAND T2
			ON T1.LANDLOGID=T2.LANDID
				INNER JOIN GET_LAND_LOG T3
				ON T2.E2IDENTIFIER=T3.E2IDENTIFIER
					--INNER JOIN Centralizator_Respinse T4
					--ON T2.CADGENNO=T4.IDENTIFICATORI

WHERE	t2.intravilan=0 
		--and T4.NOTA_CAD LIKE '%suprafata%' or t4.NOTA_CF LIKE '%suprafata%'

----------------------------------------
SELECT  *,IIF(SUP_GEOMEDIA<>TEST,'Gresit','ok') as Observatie
FROM	#TABEL1
WHERE	IIF(SUP_GEOMEDIA<>TEST,'Gresit','ok')='Gresit' and (sup_geomedia<>e2identifier_Sact and sup_geomedia<>e2identifier_Smas)