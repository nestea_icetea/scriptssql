--SELECT   distinct     parcel_log.Titleno, parcel_log.Landplotno, parcel_log.Parcelno

SELECT  DISTINCT *
FROM            Land_log INNER JOIN FileLogDescription 
				ON Land_log.Fileid = FileLogDescription.Fileid 
							INNER JOIN Parcel_log 
							ON Land_log.LandID = Parcel_log.LandID 
								FULL OUTER JOIN GET_PARCEL 
									INNER JOIN LandGeometry 
										ON GET_PARCEL.LandID = LandGeometry.LandLogID 
										ON Parcel_log.Parcelno = GET_PARCEL.Parcelno AND Parcel_log.Landplotno = GET_PARCEL.Landplotno AND Parcel_log.Titleno = GET_PARCEL.Titleno

 where SourceType = 'DDAPT' and landgeometry.geometry is null

