-- genereaza o lista de imobile ce au CF si inscrieri in partea a 3 a
select distinct t2.landid, t1.e2identifier
from	(
			select  distinct t1.e2identifier
			from	get_land_log t1
						inner join registrationxentity_log t2
						on t1.landid = t2.landid
							inner join registration_log t3
							on t2.registrationid = t3.registrationid 
					
			where   t3.lbpartno=3 and t2.active=1 and t3.active=1 
					and t3.righttypeid=8

			union

			select  distinct t4.e2identifier
			from	get_building_log t1
						inner join registrationxentity_log t2
						on t1.buildingid = t2.buildingid
							inner join registration_log t3
							on t2.registrationid = t3.registrationid
									inner join get_land_log t4
									on t1.landid = t4.landid

			where   t3.lbpartno=3 and t2.active=1 and t3.active=1 
					and t3.righttypeid=8

			
		) as t1

		inner join get_land t2
		on t1.e2identifier= t2.e2identifier
			inner join landgeometry t3
			on t2.landid = t3.landlogid
					
	

---------------------------------------------------------
