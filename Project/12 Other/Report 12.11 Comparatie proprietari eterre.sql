
--script ce verifica daca sunt preluati toti proprietarii din cgxml_log in viitoarele fisiere cgxml ce urmeaza sa se predea
-- de creeat view-uri get_personbyland2, get_uniquepersonbyland2, get_personbyland2_log, get_uniquepersonbyland2_log


SELECT  *
FROM	(
			SELECT  T1.E2IDENTIFIER,T1.PERSOANA AS PERS_BD,
					--LEN(t1.persoana) - LEN(REPLACE(t1.persoana, ',', '')) + 1 as nrProprietari1,
					--LEN(t2.persoana) - LEN(REPLACE(t2.persoana, ',', '')) + 1 as nrProprietari2,
					
					LEN(ltrim(rtrim(t1.persoana))) as nrProprietari1,
					LEN(ltrim(rtrim(t2.persoana))) as nrProprietari2,
					
					
					
					T2.PERSOANA AS PERS_LOG, 
					IIF(LTRIM(RTRIM(T1.PERSOANA))<>LTRIM(RTRIM(T2.PERSOANA)),'de verificat','ok') as test
					
			FROM	GET_UNIQUEPERSONBYLAND2 T1
						FULL JOIN GET_UNIQUEPERSONBYLAND2_LOG T2
						ON T1.E2IDENTIFIER = T2.E2IDENTIFIER

			WHERE T1.E2IDENTIFIER IS NOT NULL
		) AS T1

--WHERE	T1.test='de verificat'
WHERE	nrProprietari1-nrProprietari2>2 or nrProprietari2-nrProprietari1>2