--6.Cate constructii sunt incarcate in BD fata de cele estimate

--------------Diferenta imobile estimate - Imobile incarcate in BD ----------

Declare	@ConstructiiE	INT;
Set		@ConstructiiE=2000
Declare	@ConstructiiG	INT;

Select	@ConstructiiG=count(*)
From	[dbo].[BuildingGeometry]

Print	@ConstructiiE-@ConstructiiG

-----------------------------------------------------------------------------

--------------------------Constructii fara acte--------------------------------
select T1.cadgenno,T1.landid,T1.buildingid,T1.buildno,T1.active,T1.areacte 
from get_building T1
	INNER JOIN BUILDINGGEOMETRY T2
	ON T1.BuildingID=T2.BUILDINGLOGID
where	T1.islegal=0 and T1.active=1
order by T1.landid,T1.buildno


-------------------------Cate constructii nu sunt legate in Geomedia-------------
Select id,landlogid from Buildinggeometry
where	buildinglogid is null or buildinglogid=''
order by landlogid

-------------------------Cate constructii nu au identifier? -------
Select  id 
from	Buildinggeometry
where	identifier is null

-------------------------Cate constructii nu au landlogid? ------------
Select * from Buildinggeometry
where	landlogid is null


-------------------------Concatenari constructii ? -----------
Select concat(identifier,'-',landlogid) as Identifier_landid 
from Buildinggeometry
group by concat(identifier,'-',landlogid)
having count(*)>1


--------------------------Constructii (geometrii) care nu sunt legate de textuala ------
select  t1.id as ID_Geomedia,t1.identifier as NumarConstructie,t1.Landlogid
from buildinggeometry t1
		left outer join get_building t2
		on t1.buildinglogid=t2.buildingid
where t2.buildingid is null
--------------------------------------------------------------------------------------------

-------Constructii intravilan care nu au nr postal-----

Select  t1.localitate,t1.landid,t1.buildingid,t1.zipcode
From	get_building t1
			inner join buildinggeometry t2
			on t1.buildingid=t2.buildinglogid

where	t1.zipcode='' and t1.localitate='Caldaruseanca'

-----------------------------update-------------------------------------------------
--update address
--set zipcode='999999'
--where	adressid in (Select  addressid
--					   From	get_building t1
--									inner join buildinggeometry t2
--									on t1.buildingid=t2.buildinglogid

--					   where	t1.zipcode='' )



--------Constructii care nu au nr postal------
Select  t1.landid,t1.buildingid,t1.zipcode
From	get_building t1
			inner join buildinggeometry t2
			on t1.buildingid=t2.buildinglogid

where	t1.zipcode=''
---------------------------------------------------------
--------------------Constructii care au textuala in CGR dar nu au geometrie in geomedia-----

SELECT	T1.Landid,T1.BUILDNO as NumarConstructie
FROM	GET_BUILDING T1
			LEFT OUTER JOIN BUILDINGGEOMETRY T2
			ON T1.BUILDINGID=T2.BUILDINGLOGID
				INNER JOIN LANDGEOMETRY T3
				ON T1.LANDID=T3.LANDLOGID
WHERE	T2.BUILDINGLOGID IS NULL 
---------------------------------------------------------------------------------------------
-----------------------------constructii fara numar de nivele ------------
--UPDATE	BUILDING
--SET	LEVELSNO=1
--WHERE BUILDINGID IN (
--					SELECT  BUILDINGID
--					FROM	GET_BUILDING
--					WHERE	LEVELSNO=0)
--------------------------------------------------------------------------

----------------------------constructii care nu au destinatia constructiei----------------
SELECT	*
FROM	GET_BUILDING 
WHERE	IDDESTINATION=0
---------------------------------------------------------------------------------------
select * from buildinggeometry where buildinglogid=''

--########################### Constructii care nu au numar de nivele ################################################

SELECT  T2.LANDID,concat('C',T2.BUILDNO) AS 'Numar constructie', t2.LevelsNo as Nivele
FROM	BUILDINGGEOMETRY T1
			INNER JOIN GET_BUILDING T2
			ON T1.BUILDINGLOGID=T2.BUILDINGID

WHERE	T2.LEVELSNO =0 or T2.levelsno>4

SELECT * FROM GET_BUILDING

--##################################################################################################################

--############################ Constructii fara acte ###############################################################
SELECT  *
FROM	BUILDINGGEOMETRY T1
			INNER JOIN GET_BUILDING T2
			ON T1.BUILDINGLOGID=T2.BUILDINGID
WHERE	T2.ISLEGAL=0

select t2.Cadgenno
from	get_regxentity t1
			inner join get_land t2
			on t1.landid=t2.landid
				inner join get_deed t3
					on t1.deedid=t3.deedid
where	t3.deednumber='64026'

select * from process
select * from history


SELECT  *
FROM	BUILDINGGEOMETRY 
WHERE	BUILDINGLOGID = '' OR BUILDINGLOGID IS NULL


--------------------------------------

--update buildinggeometry
--set		buildinglogid=''		

---------------------------------------


SELECT * FROM GET_BUILDING	