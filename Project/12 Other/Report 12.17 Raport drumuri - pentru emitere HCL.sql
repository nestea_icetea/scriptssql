--- Raport HCL- Drumuri

Select  distinct t2.Cadgenno as 'ID cadastru general',t2.e2identifier as IE, iif(t2.Intravilan=1,'Intravilan','Extravilan') as Intravilan,t2.Localitate, 
		t1.area as S_mas, t2.ParcelLegalArea as S_act, t3.LISTA_CATEGORIIFOLOSINTA as 'Categorie de folosinta', t4.persoana as Proprietar,
		t5.notes as 'Denumire drum'
		
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
				inner join GET_UNIQUECODEUSECATEGORYBYLAND t3
				on t2.landid = t3.landid
					inner join GET_UniquePersonByLand t4
					on t1.landlogid = t4.landid
						inner join get_parcel t5
						on t1.landlogid = t5.landid
						
					

where t3.LISTA_CATEGORIIFOLOSINTA =' DR'


