-- verificare care sunt inscrierile nepreluate din eterra3 la actualizare

--lista inscrierilor aferente unui imobil
--se compara cu lista inscrierilor din log (cele nerumerotate) - Position_Old


-- lista IE-urilor integrate
select  t2.e2identifier
into	#lista_IE
from	landgeometry t1
			inner join get_land t2
			on t1.landlogid = t2.landid
where	t2.e2identifier<>''
GO
------------------------------------------

--lista inscrieri livrare
select  *
into	#inscrieri_livrare
from	(
			select	t2.e2identifier,t4.registrationid, t4.lbpartno, t4.position as position_new, t4.position_old
			from	landgeometry t1
						inner join get_land t2
						on t1.landlogid=t2.landid
							inner join registrationxentity t3
							on t2.landid = t3.landid
								inner join registration t4
								on t3.registrationid = t4.registrationid

			where  t3.active=1 and t4.active=1 and t2.e2identifier<>''

			union 

			select  t2.e2identifier, t4.registrationid, t4.lbpartno, t4.position as position_new, t4.position_old
			from	get_building t1
						inner join get_land t2							
						on t1.landid=t2.landid
							inner join registrationxentity t3
							on t1.buildingid = t3.buildingid
								inner join registration t4
								on t3.registrationid = t4.registrationid
									inner join deed t5
									on t4.deedid=t5.deedid

			where	t3.active=1 and t4.active=1 and t2.e2identifier<>'' and 
					t5.deednumber<>'8468' 
		) as t1

order by e2identifier, lbpartno, position_new
GO
---------------------------------------------------------------------------


--lista inscrieri eterra
select  *
into	#inscrieri_IE
from	(
			select  t1.e2identifier, t3.registrationid, t3.lbpartno, t3.position,t3.appno, t3.appdate, t4.nameregistrationtype, t3.notes
			from	get_land_log t1
						inner join registrationxentity_log t2
						on t1.landid = t2.landid
							inner join registration_log t3
							on t2.registrationid=t3.registrationid
								inner join registrationtype t4
								on t3.registrationtypeid=t4.registrationtypeid

			where	t1.e2identifier<>''
			union 

			select  t2.e2identifier, t4.registrationid, t4.lbpartno, t4.position,t4.appno, t4.appdate,t5.nameregistrationtype,t4.notes
			from	get_building_log t1
						inner join get_land_log t2
						on t1.landid = t2.landid
							inner join registrationxentity_log t3
							on t1.buildingid = t3.buildingid
								inner join registration_log t4
								on t3.registrationid = t4.registrationid
									inner join registrationtype t5
									on t4.registrationtypeid=t5.registrationtypeid
			where t2.e2identifier<>''
		) as t1

order by e2identifier, lbpartno, position
GO
---------------------------------------------------------------------------


--compararea celor doua tabele
select *
from	(
			select  
					case 
						when	t2.e2identifier is null then t1.e2identifier
						when	t1.e2identifier is null then t2.e2identifier
					end as e2identifier,

			t1.registrationid,t1.lbpartno,t1.position_old,t1.position_new,t2.lbpartno as lbpartno_ie,t2.position,t2.appno, t2.appdate, t2.nameregistrationtype,t2.notes
			
			from	#inscrieri_livrare t1
						full outer join #inscrieri_IE t2
						on concat(t1.e2identifier,'-',t1.lbpartno,'-', t1.position_old) = concat (t2.e2identifier,'-', t2.lbpartno,'-',t2.position)
							--inner join #lista_IE t3
							--on t2.e2identifier=t3.e2identifier
		) as t1

where t1.e2identifier in (select * from #lista_ie)
GO

drop table #inscrieri_livrare
go
drop table #inscrieri_ie
go	
drop table #lista_IE
go
---------------------------------------------------------------------------
