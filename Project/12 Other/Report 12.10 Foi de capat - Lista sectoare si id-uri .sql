--lista cu toate imobilele aferente fiecarui sector cu virgula intre ele

------------------------------ETAPA 1 - introducerea intr-un tabel temporar a listei cu sectoare si id - ordonata crescator
CREATE TABLE #TEMP (SECTORCADASTRAL INT, LISTA_IDURI NVARCHAR(MAX))
GO
INSERT INTO #TEMP (SECTORCADASTRAL, LISTA_IDURI) 
SELECT  t2.sectorcadastral, t2.cadgenno as id
		from	landgeometry t1
					inner join get_land t2
					on t1.landlogid = t2.landid				
ORDER BY t2.sectorcadastral,CONVERT(INT, PARSENAME(t2.cadgenno, 1)) ASC
GO
------------------------------- ETAPA 2 - generarea listesi efective
SELECT  SECTORCADASTRAL, 
		LISTA_IDURI = STUFF (( SELECT ', ' + LISTA_IDURI
							FROM	#TEMP AS B
							WHERE   B.SECTORCADASTRAL = A.SECTORCADASTRAL
						FOR XML PATH ('')), 1, 2, '')
FROM	
	#TEMP AS A
GROUP BY SECTORCADASTRAL
ORDER BY SECTORCADASTRAL ASC
----------------------------------------------------------------------





