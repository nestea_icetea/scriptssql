DECLARE @OldDate AS DATE
SET		@OldDate='2023-02-21'   --Day in past






DECLARE @NewDate AS DATE
SET		@NewDate=DATEADD(DAY,1,CONVERT(DATE ,GETDATE())) --			--Today
			

----------------------------------------------------------------------- AGREGATION 1------------------------------------------------------------------------------------------------------------------			
										
										SELECT  DISTINCT db_name() as UAT, DATEADD(DAY,-1,@NewDate) as Azi, @OldDate as DataOld, iif(t105.E2Identifier>1,1,0) AS IE, IIF(T106.IUNO>0,1,0) AS IU, T104.*
										INTO	#Temp
										FROM	(

--------------------------------------------------------------------------- AGREGATION -2------------------------------------------------------------------------------------------------------------------
																		SELECT  
																		SUBSTRING (Userid_Land,0,CHARINDEX('-',Userid_Land) ) AS UserId,
																		SUBSTRING (Userid_Land,CHARINDEX('-',Userid_Land)+1,len(Userid_Land))  AS Landid, 
																		sum(CreatedLand) as CreatedLand,
																		sum(CreatedBuilding) as CreatedBuilding, 												
																		sum(CreatedParcel) as CreatedParcel, 											
																		sum(CreatedDeed) as CreatedDeed, 
																		sum(CreatedDeedIU) as CreatedDeedIU, 
																		sum(CreatedPerson) as CreatedPerson,

																		sum(UpdatedLand) as UpdatedLand,
																		sum(UpdatedBuilding) as UpdatedBuilding, 												
																		sum(UpdatedParcel) as UpdatedParcel, 											
																		sum(UpdatedDeed) as UpdatedDeed,
																		sum(UpdateReg) as UpdatedReg,
																		sum(UpdatedDeedIU) as UpdatedDeedIU, 
																		sum(UpdatedPerson) as UpdatedPerson

																		FROM	(
----------------------------------------------------------------------- AGREGATION -3------------------------------------------------------------------------------------------------------------------	
																						--LAND CREATED
																						SELECT  DISTINCT 
																						concat(PERSONCREATED,'-',LANDID) as Userid_Land, 
																						count(*) as CreatedLand,
																						'' as CreatedBuilding, 
																	 
																						'' as CreatedParcel, 																
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,


																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	 
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson
			
																						FROM	LAND
																						where	cast(datecreated as date) >= @OldDate and 
																								cast(datecreated as date) <= @NewDate 
																								and (e2identifier is null or e2identifier =0)
																						group by concat(PERSONCREATED,'-',LANDID)
				

																					UNION 

																						--LAND UPDATED
																						SELECT  DISTINCT 
																						concat(PERSONLASTUPDATE,'-',LANDID) as Userid_Land, 

																						'' as  CreatedLand,
																						'' as CreatedBuilding, 																	
																						'' as CreatedParcel, 																
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						count(*) as UpdatedLand,
																						'' as UpdatedBuilding,																	 
																						'' as UpdatedParcel,
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

										
																			
																						FROM	LAND
																						WHERE PersonLastUpdate>0 and
																						cast(DateLastupdate as date) >= @OldDate and 
																						cast(DateLastupdate as date) <= @NewDate 
																						group by concat(PERSONLASTUPDATE,'-',LANDID)

		
				

																					UNION

				
																						--BUILDING CREATED
																						SELECT  DISTINCT 
																						concat(PERSONCREATED,'-',LANDID) as Userid_Land,
			
																						'' as CreatedLand, 
																						count(*) as CreatedBuilding,																	 
																						'' as CreatedParcel, 																	 
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding,																	 
																						'' as UpdatedParcel,
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	BUILDING																				
																						where	cast(datecreated as date) >= @OldDate and 
																								cast(datecreated as date) <= @NewDate  
																						--and len(e2identifier)>1
																						group by concat(PERSONCREATED,'-',LANDID)


																					UNION

				
																						--BUILDING UPDATED
																						SELECT  DISTINCT 
																						concat(PERSONLASTUPDATE,'-',LANDID) as Userid_Land,
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding,																	 
																						'' as CreatedParcel, 																	 
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						count(*) as UpdatedBuilding,																	 
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson


																						FROM	BUILDING
																						WHERE PersonLastUpdate>0 and 
																						cast(DateLastupdate as date) >= @OldDate and 
																						cast(DateLastupdate as date) <= @NewDate 
																						group by concat(PERSONLASTUPDATE,'-',LANDID)
					
																		

																					UNION

		
																						--PARCEL CREATED
																						SELECT  DISTINCT 
																						concat(t1.PERSONCREATED,'-',t1.LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding,																	 
																						count(*) as CreatedParcel,																
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding,																	
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	PARCEL t1
																									inner join land t2
																									on t1.landid = t2.landid
																						where	cast(t1.datecreated as date) >= @OldDate and 
																								cast(t1.datecreated as date) <= @NewDate 
																						group by concat(t1.PERSONCREATED,'-',t1.LANDID)


																					UNION

		
																						--PARCEL updated
																						SELECT  DISTINCT 
																						concat(PersonLastUpdate,'-',LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	
																						'' as CreatedParcel,																
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding,																	 
																						count(*) as UpdatedParcel, 
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	PARCEL 
																						WHERE PersonLastUpdate>0 and 
																						cast(DateLastUpdate as date) >= @OldDate and 
																						cast(DateLastUpdate as date) <= @NewDate 
																						group by concat(PersonLastUpdate,'-',LANDID)
																	
			
																						UNION 



																						--------------------------de pus si registrationxentity pentru inscrieri noi/sterse 
																						--regxentity TEREN updated
																						SELECT  DISTINCT  
																						concat(PersonLastUpdate,'-',LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	 
																						'' as CreatedParcel, 																
																						'' as CreatedDeed,
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	 
																						'' as UpdatedParcel, 
																						count(*) as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	REGISTRATIONXENTITY 																				
																						where	cast(datecreated as date) >= @OldDate and 
																								cast(datecreated as date) <= @NewDate 
																						--and (t4.E2Identifier is null or t4.E2Identifier=0)
																						group by concat(PersonLastUpdate,'-',LANDID)
																						-----------------------------------------------------------------------------------

																						UNION

																						--regxentity BUILDING updated
																						SELECT  DISTINCT  
																						concat(PersonLastUpdate,'-',LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	 
																						'' as CreatedParcel, 																
																						'' as CreatedDeed,
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	 
																						'' as UpdatedParcel, 
																						count(*) as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	REGISTRATIONXENTITY 																				
																						where	cast(DateLastUpdate as date) >= @OldDate and 
																								cast(DateLastUpdate as date) <= @NewDate
																						--and (t4.E2Identifier is null or t4.E2Identifier=0)
																						group by concat(PersonLastUpdate,'-',LANDID)
																						-----------------------------------------------------------------------------------

																						UNION


																						--DEED TEREN CREATED
																						SELECT  DISTINCT  
																						concat(t3.PERSONCREATED,'-',t1.LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	 
																						'' as CreatedParcel, 																
																						count(*) as CreatedDeed,
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	 
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	REGISTRATIONXENTITY T1
																									INNER JOIN REGISTRATION T2
																									ON T1.REGISTRATIONID = T2.REGISTRATIONID
																										INNER JOIN DEED T3
																										ON T2.DEEDID = T3.DEEDID
																											inner join land t4
																											on t1.LandID = t4.landid
																						where	cast(t3.datecreated as date) >= @OldDate and 
																								cast(t3.datecreated as date) <= @NewDate  
																						--and (t4.E2Identifier is null or t4.E2Identifier=0)
																						group by concat(t3.PERSONCREATED,'-',t1.LANDID)


																					UNION 

						
																						--DEED TEREN UPDATED
																						SELECT  DISTINCT  
																						concat(t3.PersonLastUpdate,'-',t1.LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding,																	
																						'' as CreatedParcel, 																
																						'' as CreatedDeed,
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	 
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						count(*) as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	REGISTRATIONXENTITY T1
																									INNER JOIN REGISTRATION T2
																									ON T1.REGISTRATIONID = T2.REGISTRATIONID
																										INNER JOIN DEED T3
																										ON T2.DEEDID = T3.DEEDID
																	
																						WHERE T3.PersonLastUpdate>0 and 
																						cast(t3.DateLastUpdate as date) >= @OldDate and 
																						cast(t3.DateLastUpdate as date) <= @NewDate 
																						group by concat(t3.PersonLastUpdate,'-',t1.LANDID)


																						UNION

																						--DEED BUILDING CREATED
																						SELECT  DISTINCT  
																						concat(t4.PERSONCREATED,'-',t1.LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	 
																						'' as CreatedParcel, 																
																						count(*) as CreatedDeed,
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding,																	 
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	BUILDING T1
																									INNER JOIN REGISTRATIONXENTITY T2
																									ON T1.BUILDINGID = T2.BUILDINGID
																										INNER JOIN REGISTRATION T3
																										ON T2.REGISTRATIONID = T3.REGISTRATIONID
																											INNER JOIN DEED T4
																											ON T3.DEEDID = T4.DEEDID
																												inner join land t5
																												on t1.landid = t5.landid
																						where	cast(t4.datecreated as date) >= @OldDate and 
																								cast(t4.datecreated as date) <= @NewDate  
																						--and (t5.E2Identifier is null or t5.E2Identifier=0)
																						group by concat(t4.PERSONCREATED,'-',t1.LANDID)


																
																						UNION

																						--DEED BUILDING UPDATED
																						SELECT  DISTINCT  
																						concat(t4.PersonLastUpdate,'-',t1.LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	
																						'' as CreatedParcel, 																
																						'' as CreatedDeed,
																						'' as CreatedDeedIU, 
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding,																	
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						count(*) as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson

																						FROM	BUILDING T1
																									INNER JOIN REGISTRATIONXENTITY T2
																									ON T1.BUILDINGID = T2.BUILDINGID
																										INNER JOIN REGISTRATION T3
																										ON T2.REGISTRATIONID = T3.REGISTRATIONID
																											INNER JOIN DEED T4
																											ON T3.DEEDID = T4.DEEDID
																	
																						WHERE T4.PersonLastUpdate>0 and 
																						cast(t4.DateLastUpdate as date) >= @OldDate and 
																						cast(t4.DateLastUpdate as date) <= @NewDate 
																						group by concat(t4.PersonLastUpdate,'-',t1.LANDID)
			
						
																						UNION

		
																						--DEED IU CREATED
																						SELECT  DISTINCT  
																						concat(t5.PERSONCREATED,'-',t1.LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding,																	 
																						'' as CreatedParcel, 																
																						'' as CreatedDeed, 
																						count(*) as CreatedDeedIU,
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	
																						'' as UpdatedParcel, 
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson


																						FROM	BUILDING T1
																									INNER JOIN REGISTRATIONXENTITY T2
																									ON T1.BUILDINGID = T2.BUILDINGID
																										INNER JOIN IU T3
																										ON T1.BUILDINGID = T3.BUILDINGID
																											INNER JOIN REGISTRATION T4
																											ON T2.REGISTRATIONID = T4.REGISTRATIONID
																												INNER JOIN DEED T5
																												ON T4.DEEDID = T5.DEEDID
																						where	cast(t5.datecreated as date) >= @OldDate and 
																								cast(t5.datecreated as date) <= @NewDate 
																						group by concat(t5.PERSONCREATED,'-',t1.LANDID)


																					UNION

		
																						--DEED IU UPDATED
																						SELECT  DISTINCT  
																						concat(t5.PersonLastUpdate,'-',t1.LANDID) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	 
																						'' as CreatedParcel, 																
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU,
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	 
																						'' as UpdatedParcel,
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						count(*) as UpdatedDeedIU, 
																						'' as UpdatedPerson


																						FROM	BUILDING T1
																									INNER JOIN REGISTRATIONXENTITY T2
																									ON T1.BUILDINGID = T2.BUILDINGID
																										INNER JOIN IU T3
																										ON T1.BUILDINGID = T3.BUILDINGID
																											INNER JOIN REGISTRATION T4
																											ON T2.REGISTRATIONID = T4.REGISTRATIONID
																												INNER JOIN DEED T5
																												ON T4.DEEDID = T5.DEEDID

																
																						WHERE T5.PersonLastUpdate>0 and 
																						cast(t5.DateLastupdate as date) >= @OldDate and 
																						cast(t5.DateLastUpdate as date) <= @NewDate 
																						group by concat(t5.PersonLastUpdate,'-',t1.LANDID)

			
																						UNION 

			
																						--PERSON  CREATED
																						SELECT  DISTINCT 
																						concat(t1.PERSONCREATED,'-',t1.fileid) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	 
																						'' as CreatedParcel, 															
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU,
																					count(*) as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	 
																						'' as UpdatedParcel,
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						'' as UpdatedPerson


																						FROM	PERSON t1
																									inner join land t2
																									on t1.FileId=t2.landid
																						where	cast(t1.datecreated as date) >= @OldDate and 
																								cast(t1.datecreated as date) <= @NewDate  
																						--and (t2.E2Identifier='' or t2.E2Identifier is null)
																						group by concat(t1.PERSONCREATED,'-',t1.fileid)


																					UNION 

			
																						--PERSON  UPDATED
																						SELECT  DISTINCT 
																						concat(PersonLastUpdate,'-',fileid) as Userid_Land, 
			
																						'' as CreatedLand, 
																						'' as CreatedBuilding, 																	 
																						'' as CreatedParcel, 															
																						'' as CreatedDeed, 
																						'' as CreatedDeedIU,
																						'' as CreatedPerson,

																						'' as UpdatedLand,
																						'' as UpdatedBuilding, 																	
																						'' as UpdatedParcel,
																						'' as UpdateReg,
																						'' as UpdatedDeed, 
																						'' as UpdatedDeedIU, 
																						count(*) as UpdatedPerson


																						FROM	PERSON
																						WHERE PersonLastUpdate>0 and 
																						cast(DateLastUpdate as date) >= @OldDate and 
																						cast(DateLastUpdate as date) <= @NewDate 
																						group by concat(PersonLastUpdate,'-',fileid)




-----------------------------------------------------------------------  FINAL AGREGATION -3------------------------------------------------------------------------------------------------------------------	
																			) AS T100										

																			WHERE 
																			SUBSTRING (Userid_Land,0,CHARINDEX('-',Userid_Land) ) >1 AND 
																			SUBSTRING (Userid_Land,CHARINDEX('-',Userid_Land)+1,len(Userid_Land)) >1
																			GROUP BY Userid_Land


-----------------------------------------------------------------------  FINAL AGREGATION -2------------------------------------------------------------------------------------------------------------------	
															
												) AS T104

												INNER JOIN LAND T105
												ON T104.LANDID = T105.LANDID
													INNER JOIN BUILDING T106
													ON T105.LANDID = T106.LANDID

-------------------------------------------------------------------------  FINAL AGREGATION 1------------------------------------------------------------------------------------------------------------------																

------------------------------------------------------------------------- BI------------------------------------------------------------------------------------------------------------------


					SELECT   db_name() as UAT, @oldDate as ZiuaStart, DATEADD(DAY,-1,@NewDate) as ZiuaFinal,
					
					 datediff(dd, @OldDate, @NewDate) +case when datepart(dw, @OldDate) = 7 then 1 else 0 end - (datediff(wk, @OldDate, @NewDate) * 2) -
					 case when datepart(dw, @OldDate) = 1 then 1 else 0 end +
					 case when datepart(dw, @NewDate) = 1 then 1 else 0 end as 'Zile lucratoare',
					
					
					
					t103.UserName,T104.LISTA_LANDID AS Lista, t102.*
					FROM	(SELECT  
							UserId,							
							COUNT(*) - sum(IE) as LandsProcessed_New, 
							sum(IE) AS LandsProcessed_with_IE,
							sum(IU) as LandsProcessed_with_UI,


							sum(CreatedLand) + sum(CreatedBuilding) + sum(CreatedParcel) as Created_Part1,
							sum(CreatedDeed) + sum(CreatedDeedIU) +	  sum(CreatedPerson) as Created_Part2_3,
							
							sum(UpdatedLand) + sum(UpdatedBuilding) + sum(UpdatedParcel) as Updated_Part1,
							sum(UpdatedDeed) + sum(UpdatedDeedIU) +   sum(UpdatedPerson) + sum(UpdatedReg) as Updated_Part2_3
							 

							FROM		#temp
							GROUP BY	USERID) AS T102
																INNER JOIN USERS T103
																ON T102.UserId = T103.UserId
																										INNER JOIN 
																												(SELECT  
																															T2.USERID,
																															STUFF(( SELECT ', ' + T1.LANDID
																															FROM #TEMP AS T1
																															WHERE	T1.UserId = T2.USERID
																															FOR XML PATH('')), 1, 1, '') as LISTA_LANDID
																													FROM	#TEMP AS T2
																													GROUP	BY T2.USERID
																												) AS T104
																												ON T102.UserId = T104.UserId
------------------------------------------------------------------------- BI------------------------------------------------------------------------------------------------------------------
DROP TABLE #TEMP


------------------------------------------------------------------------ GEOMEDIA-----------------------------------------------------------------------------------------------------------
select				db_name() as UAT, @oldDate as ZiuaStart, @NewDate as ZiuaFinal,
					
					 datediff(dd, @OldDate, @NewDate) +case when datepart(dw, @OldDate) = 7 then 1 else 0 end - (datediff(wk, @OldDate, @NewDate) * 2) -
					 case when datepart(dw, @OldDate) = 1 then 1 else 0 end +
					 case when datepart(dw, @NewDate) = 1 then 1 else 0 end as 'Zile lucratoare',
					 UserName, sum(Inserted) as InsertedGeometry, sum(Updated) as UpdatedGeometry, sum(Deleted) as DeletedGeometry
from	(
				select  username, 
								case when Operationtype like 'Insert' then count(*)
									 else 0
									 end as Inserted,
								case when Operationtype like 'Update' then count(*) 
									 else 0
									 end as Updated,
								case when Operationtype like 'Delete' then count(*) 
									 else 0
									 end as Deleted
				
				from	history 
				where	modificationDate >= @OldDate and 
						modificationDate <= @NewDate 
						and OperationType in ('Insert','Update','Delete') ----- daca se pune cadgenno>0 se poate vedea ca sunt acele geometrii care sunt deja corelate
				group	by username, OperationType 		
		) as t1
group by t1.username

