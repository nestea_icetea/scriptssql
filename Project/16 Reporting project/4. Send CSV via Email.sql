DECLARE @Column1Name1 VARCHAR(255)
DECLARE @Query1 VARCHAR(2048)
SET		@Column1Name1 = '[sep=*' + CHAR(13) + CHAR(10) + 'UAT]'
SET		@Query1 = 'SELECT UAT AS ' + @Column1Name1 + ', [Implementat UAT (imobile) %],[Implementat UAT (suprafete) %], Data FROM Overview'
EXEC msdb.dbo.sp_send_dbmail
    @profile_name = 'Notifications',  
    @recipients = 'cgisca@ramboll.com',  
    @body = 'Acesta este un overview pentru toate proiectele din cadrul PNCCF aflate in curs de implementare. Aceste procente sunt raportate la numarul de imobile din caietul de sarcini si la suprafata totala a UAT-ului',
    @query = @query1,
    @execute_query_database = 'Reporting',
    @attach_query_result_as_file = 1,
    @query_attachment_filename = 'Overview Projects PNCCF.csv',
    @query_result_header = 1,
    @subject =  'Overview Projects PNCCF',
    @query_result_separator = '*',
    @exclude_query_output = 1,
    @append_query_error = 1,
    @query_no_truncate = 0,
    @query_result_no_padding = 1,
    @query_result_width =32767
go



DECLARE @Column1Name2 VARCHAR(255)
DECLARE @Query2 VARCHAR(2048)
SET		@Column1Name2 = '[sep=*' + CHAR(13) + CHAR(10) + 'ID]'
SET		@Query2 = 'SELECT ID AS ' + @Column1Name2 + ',UAT, Yesterday, Today, [Zile lucratoare], UserName, 
				LandsProcessed_New, LandsProcessed_with_IE, LandsProcessed_with_UI,
				Created_Part1, Created_Part2_3, Updated_Part1, Updated_Part2_3 
				
				FROM UserActivity_Daily_CGR where Today = DATEADD(DAY,-1,CONVERT(DATE ,GETDATE() ))'
EXEC msdb.dbo.sp_send_dbmail
    @profile_name = 'Notifications',  
    @recipients = 'cgisca@ramboll.com',  
    @body = 'Acesta este un LOG cu privire la activitatea desfasurata in cadrul aplicatiei CGR',
    @query = @Query2,
    @execute_query_database = 'Reporting',
    @attach_query_result_as_file = 1,
    @query_attachment_filename = 'LOG_CGR.csv',
    @query_result_header = 1,
    @subject =  'LOG - Activity CGR Yesterday',
    @query_result_separator = '*',
    @exclude_query_output = 1,
    @append_query_error = 1,
    @query_no_truncate = 0,
    @query_result_no_padding = 1,
    @query_result_width =32767
go

DECLARE @Column1Name3 VARCHAR(255)
DECLARE @Query3 VARCHAR(2048)
SET		@Column1Name3 = '[sep=*' + CHAR(13) + CHAR(10) + 'UAT]'
SET		@Query3 = 'SELECT UAT AS ' + @Column1Name3 + ', Yesterday, Today, [Zile lucratoare], UserName, 
				InsertedGeometry, UpdatedGeometry, DeletedGeometry
				
				FROM UserActivity_Daily_Geomedia where Today=DATEADD(DAY,-1,CONVERT(DATE ,GETDATE()) )'
EXEC msdb.dbo.sp_send_dbmail
    @profile_name = 'Notifications',  
    @recipients = 'cgisca@ramboll.com',  
    @body = 'Acesta este un LOG cu privire la activitatea desfasurata in cadrul aplicatiei Geomedia',
    @query = @query3,
    @execute_query_database = 'Reporting',
    @attach_query_result_as_file = 1,
    @query_attachment_filename = 'LOG_Geomedia.csv',
    @query_result_header = 1,
    @subject =  'LOG - Activity Geomedia Yesterday',
    @query_result_separator = '*',
    @exclude_query_output = 1,
    @append_query_error = 1,
    @query_no_truncate = 0,
    @query_result_no_padding = 1,
    @query_result_width =32767
go

