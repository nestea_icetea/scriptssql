--- Populare date tabela DB_imobile---
use [Reporting]
DECLARE @command1 varchar(1000) 
SELECT @command1 = 
'IF ''?'' IN(
''CORUNCA'', 
''CRISTESTI'', 
''LETEA_VECHE'', 
''SANGEORGIU'',
''SAUCESTI'',
''MARGINENI'',
''SANCRAIU'', 
''NICOLAE_BALCESCU'') 

BEGIN USE ? 
				SELECT   DB_NAME() AS UAT, CONVERT(DATE ,GETDATE()) AS DATA, 
				t6.DB_ImobileCorelate_T, T5.DB_ImobileCorelate_I, DB_Sup_Corelate_T, DB_Sup_Corelate_I 
				from (
						(
						SELECT   DB_NAME() AS UAT, CONVERT(DATE ,GETDATE()) AS DATA, count(*) as DB_ImobileCorelate_T,sum(t1.area) as DB_Sup_Corelate_T
						FROM	LANDGEOMETRY T1
									INNER JOIN GET_LAND T2
									ON T1.LANDLOGID = T2.LANDID
						) as T6

						inner join 

						(
						SELECT   DB_NAME() AS UAT, CONVERT(DATE ,GETDATE()) AS DATA, count(*) as DB_ImobileCorelate_I ,sum(t3.area) as DB_Sup_Corelate_I
		
						FROM	LANDGEOMETRY T3
									INNER JOIN GET_LAND T4
									ON T3.LANDLOGID = T4.LANDID
						WHERE	T4.INTRAVILAN=1
						) as T5

						ON  t6.UAT = t5.UAT
				)
END'
INSERT  INTO DB_imobile (UAT, DATA, DB_ImobileCorelate_T, DB_ImobileCorelate_I, Db_Sup_corelate_T, DB_Sup_corelate_I)
EXEC sp_MSforeachdb @command1
