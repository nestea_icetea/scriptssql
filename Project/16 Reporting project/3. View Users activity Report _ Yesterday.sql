use [Reporting]
DECLARE @command1 varchar(1000) 
SELECT @command1 = 
'IF ''?'' IN(
''CORUNCA'', 
''CRISTESTI'', 
''LETEA_VECHE'', 
''SANGEORGIU'',
''SAUCESTI'',
''MARGINENI'',
''SANCRAIU'', 
''NICOLAE_BALCESCU'') 

BEGIN USE ? 
	

	SELECT  
	UAT, 
	Yesterday, 
	Today, 
	[Zile lucratoare], 
	UserName, 
	Lista, 
	UserId, 
	LandsProcessed_New, 
	LandsProcessed_With_IE, 
	LandsProcessed_With_UI, 
	Created_Part1, 
	Created_Part2_3, 
	Updated_Part1, 
	Updated_Part2_3
	FROM	GET_UserAvtivyYesterday_C_A


	

	
	
END'
INSERT INTO UserActivity_Daily_CGR 
	(UAT, 
	Yesterday, 
	Today, 
	[Zile lucratoare], 
	UserName, 
	Lista, 
	UserId, 
	LandsProcessed_New, 
	LandsProcessed_With_IE, 
	LandsProcessed_With_UI, 
	Created_Part1, 
	Created_Part2_3, 
	Updated_Part1, 
	Updated_Part2_3)
EXEC sp_MSforeachdb @command1
go

-------------------------------------------------------------------------------------Geomedia------
use [Reporting]
DECLARE @command1 varchar(1000) 
SELECT @command1 = 
'IF ''?'' IN(
''CORUNCA'', 
''CRISTESTI'', 
''LETEA_VECHE'', 
''SANGEORGIU'',
''SAUCESTI'',
''MARGINENI'',
''SANCRAIU'', 
''NICOLAE_BALCESCU'') 

BEGIN USE ? 
	SELECT 
	UAT,
	Yesterday,
	Today,
	[Zile lucratoare],
	UserName,
	InsertedGeometry,
	UpdatedGeometry,
	DeletedGeometry
	FROM	GET_UserActivityYesterday_G
END'

INSERT INTO UserActivity_Daily_Geomedia
	(UAT,
	Yesterday,
	Today,
	[Zile lucratoare],
	UserName,
	InsertedGeometry,
	UpdatedGeometry,
	DeletedGeometry)
EXEC sp_MSforeachdb @command1
go