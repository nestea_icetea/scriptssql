Select T1.UAT, T1.[Implementat UAT (imobile) %],T2.[Implementat UAT (suprafete) %],t1.Data
from  
(
Select  t1.UAT, 					
		round(cast(t2.Db_imobileCorelate_T  as float) /  cast(t1.EstimatImobileCS_T + t1.EstimatImobileCS_T /5 as float)*100,0) as 'Implementat UAT (imobile) %', t2.data
		--round(cast(t2.Db_imobileCorelate_I  as float) /  cast(t1.EstimatImobileCS_I + t1.EstimatImobileCS_I/5 as float)*100,2) as 'Implementat intravilan %',
		--round(cast(t2.Db_ImobileCorelate_T - t2.Db_ImobileCorelate_I as float) / 
		--cast(t1.EstimatImobileCS_T + t1.EstimatImobileCS_T/5 - t1.EstimatImobileCS_I +  t1.EstimatImobileCS_I/5 as float),2) * 100 as 'Implementat extravilan %'
from   RP_Estimari t1
			inner join db_imobile t2
			on t1.UAT = t2. UAT
) as T1

--la toate estimarile din CS cu privire la imobile a fost adaugat 20%

inner join 
(
Select  t1.UAT, 
		round(cast(t2.Db_Sup_corelate_T as float) /10000 / cast(t1.Sup_T as float)*100,0) as 'Implementat UAT (suprafete) %',t2.data
		--round(cast(t2.DB_Sup_corelate_I as float) /10000/ cast(t1.Sup_I as float)*100,0) as 'Implementat intravilan %',
		--round(cast(t2.DB_Sup_corelate_T- t2.DB_sup_corelate_I as float)/10000 / cast(t1.EstimatImobileCS_T-t1.EstimatImobileCS_I as float) *100,0) as 'Implementat extravilan %'
from   RP_Estimari t1
			inner join db_imobile t2
			on t1.UAT = t2. UAT
) as T2
on T1.UAT = T2.UAT
