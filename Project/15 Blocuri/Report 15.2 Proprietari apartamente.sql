--PROPRIETARI APARTAMENTE

select  t2.landid,t5.personid,t5.firstname,t5.lastname,t5.idcode
INTO	#TEMP_CNP_PERSON
from	iu t1
			inner join get_building t2
			on t1.buildingid = t2.buildingid
				inner join registrationxentity t3
				on t1.iuid = t3.iuid
					inner join registrationxperson t4
					on t3.registrationid = t4.idregistration
						inner join person t5
						on t4.idperson=t5.personid

where			t5.idcode NOT LIKE '%9999%' and t5.isphisical=1 and t3.active=1 and t4.active=1 and t5.active=1

-----------------------------------------------------------------
GO
----------------------------------------------------------------- VERIFICARE PROPRIU-ZISA

SELECT  LANDID,PERSONID,FIRSTNAME,LASTNAME,IDCODE AS CNP,TEST AS 'Test CNP'
FROM	(

					SELECT  DISTINCT *,IIF(
															(
																(CAST(SUBSTRING(IDCODE,1,1) AS INT)*'2'+
																CAST(SUBSTRING(IDCODE,2,1) AS INT)*'7'+
																CAST(SUBSTRING(IDCODE,3,1) AS INT)*'9'+
																CAST(SUBSTRING(IDCODE,4,1) AS INT)*'1'+
																CAST(SUBSTRING(IDCODE,5,1) AS INT)*'4'+
																CAST(SUBSTRING(IDCODE,6,1) AS INT)*'6'+
																CAST(SUBSTRING(IDCODE,7,1) AS INT)*'3'+
																CAST(SUBSTRING(IDCODE,8,1) AS INT)*'5'+
																CAST(SUBSTRING(IDCODE,9,1) AS INT)*'8'+
																CAST(SUBSTRING(IDCODE,10,1) AS INT)*'2'+
																CAST(SUBSTRING(IDCODE,11,1) AS INT)*'7'+
																CAST(SUBSTRING(IDCODE,12,1) AS INT)*'9'	)

						
																% '11' 
						
																- CAST(SUBSTRING(IDCODE,13,1) AS INT)   =  '0'
															 )

											OR

															(
																	(CAST(SUBSTRING(IDCODE,1,1) AS INT)*'2'+
																	CAST(SUBSTRING(IDCODE,2,1) AS INT)*'7'+
																	CAST(SUBSTRING(IDCODE,3,1) AS INT)*'9'+
																	CAST(SUBSTRING(IDCODE,4,1) AS INT)*'1'+
																	CAST(SUBSTRING(IDCODE,5,1) AS INT)*'4'+
																	CAST(SUBSTRING(IDCODE,6,1) AS INT)*'6'+
																	CAST(SUBSTRING(IDCODE,7,1) AS INT)*'3'+
																	CAST(SUBSTRING(IDCODE,8,1) AS INT)*'5'+
																	CAST(SUBSTRING(IDCODE,9,1) AS INT)*'8'+
																	CAST(SUBSTRING(IDCODE,10,1) AS INT)*'2'+
																	CAST(SUBSTRING(IDCODE,11,1) AS INT)*'7'+
																	CAST(SUBSTRING(IDCODE,12,1) AS INT)*'9'	)

						
																	% '11' -'10'  =  '0'
															)																		
																													,'OK','GRESIT') AS TEST
						FROM	#TEMP_CNP_PERSON
			) AS T1
					
					WHERE	T1.TEST='GRESIT'
----------------------------------------------------------------------------------
GO
---------------------------------------------------------------------------------
DROP TABLE #TEMP_CNP_PERSON